	.file	"kernel.c"
	.global	TD
	.bss
	.align	2
	.type	TD, %object
	.size	TD, 20
TD:
	.space	20
	.text
	.align	2
	.global	initialize
	.type	initialize, %function
initialize:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	mov	ip, sp
	stmfd	sp!, {fp, ip, lr, pc}
	sub	fp, ip, #4
	mov	r3, #0
	mov	r0, r3
	ldmfd	sp, {fp, sp, pc}
	.size	initialize, .-initialize
	.section	.rodata
	.align	2
.LC0:
	.ascii	"Hello world.\012\015\000"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 1, uses_anonymous_args = 0
	mov	ip, sp
	stmfd	sp!, {sl, fp, ip, lr, pc}
	sub	fp, ip, #4
	sub	sp, sp, #12
	ldr	sl, .L9
.L8:
	add	sl, pc, sl
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	mov	r0, #1
	mov	r1, #0
	bl	bwsetfifo(PLT)
	mov	r0, #1
	ldr	r3, .L9+4
	add	r3, sl, r3
	mov	r1, r3
	bl	bwprintf(PLT)
	bl	initialize(PLT)
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L4
.L5:
	mov	r0, #1
	mov	r1, #29
	bl	kerxit(PLT)
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L4:
	ldr	r3, [fp, #-20]
	cmp	r3, #3
	ble	.L5
	mov	r3, #0
	mov	r0, r3
	sub	sp, fp, #16
	ldmfd	sp, {sl, fp, sp, pc}
.L10:
	.align	2
.L9:
	.word	_GLOBAL_OFFSET_TABLE_-(.L8+8)
	.word	.LC0(GOTOFF)
	.size	main, .-main
	.section	.rodata
	.align	2
.LC1:
	.ascii	"lerxit.c: Hello.\012\015\000"
	.align	2
.LC2:
	.ascii	"kerxit.c: Activating.\012\015\000"
	.align	2
.LC3:
	.ascii	"kerxit.c: Good-bye.\012\015\000"
	.text
	.align	2
	.global	kerxit
	.type	kerxit, %function
kerxit:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	mov	ip, sp											;ip (r12) is a scratch register, not instruction pointer
	stmfd	sp!, {sl, fp, ip, lr, pc}					;sl is Stack limit (r10)
	sub	fp, ip, #4										;fp = sp - 4
	sub	sp, sp, #8
	ldr	sl, .L14
.L13:
	add	sl, pc, sl
	str	r0, [fp, #-20]									;Here is where TD* is stored onto the stack
	str	r1, [fp, #-24]									;Request* is stored in this -ve offset from fp
	mov	r0, #1
	ldr	r3, .L14+4
	add	r3, sl, r3
	mov	r1, r3
	bl	bwprintf(PLT)
	mov	r0, #1
	ldr	r3, .L14+8
	add	r3, sl, r3
	mov	r1, r3
	bl	bwprintf(PLT)
	push r0-r3									;push kernel registers to stack

	mrs r0, cpsr_all							;Copy CPSR to r0
	bic r0, r0, #&1F							;clear last 5 bits
	orr r0, r0, #31								;Set mode bits to 31 (All ones for last 5 bits)
	msr cpsr_all, r0							;write the new CPSR value

	ldr r1,  [fp, #-20]							;Load TD* to r1	
	ldr r2, [r0]								;Store sp into r2 (The square brackets indicate that it is a mem address
	ldr r3, [r0, #-4]							;Store cpsr into r3. Making the assumption that it is a negative offset
	ldr r0, [r0, #-8]							;Store return value into r0
												;pop registers of active task from its stack

	mrs r1, cpsr								;Switch back to svc mode
	bic r1, r1, #&1F							
	orr r1, r1, #&13							;Set mode bits to 16
	msr cpsr, r1								;Now back to svc mode

	msr spsr, r3								;install spsr of active task (earlier stored in r3)
	movs 										;install the pc of the active task

	bl	kerent(PLT)								;The context switch

												;acquire the arguments of the request
	

	mov	r0, #1
	ldr	r3, .L14+12
	add	r3, sl, r3
	mov	r1, r3
	bl	bwprintf(PLT)
	sub	sp, fp, #16
	ldmfd	sp, {sl, fp, sp, pc}
.L15:
	.align	2
.L14:
	.word	_GLOBAL_OFFSET_TABLE_-(.L13+8)
	.word	.LC1(GOTOFF)
	.word	.LC2(GOTOFF)
	.word	.LC3(GOTOFF)
	.size	kerxit, .-kerxit
	.align	2
	.global	kerent
	.type	kerent, %function
kerent:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	mov	ip, sp
	stmfd	sp!, {fp, ip, lr, pc}
	sub	fp, ip, #4
	ldmfd	sp, {fp, sp, pc}
	.size	kerent, .-kerent
	.ident	"GCC: (GNU) 4.0.2"
