#include "task_queue.h"

int taskQueuePush(Queue *queue, TaskId tid, TaskPriority priority, TaskDescriptor descriptors[MAX_TASK_COUNT])
{
    // Sanity Checks
    if(! queue)                                                         return -1;
    if(tid < MIN_TASK_ID || tid > MAX_TASK_ID)                          return -2; 
    if(priority < MIN_TASK_PRIORITY || priority > MAX_TASK_PRIORITY)    return -3;
    if(queue->len >= MAX_TASK_COUNT)                                    return -4;

    // Get position to add <tid> to queue
    int i = 0;
    int posn = tasks.queue->next;
    for(; i < queue->len; ++i) {
        int current_tid = queue->data[posn];
        int current_priority = descriptors[current_tid].priority;

        if(priority < current_priority) break; // Insert here 

        posn = (posn + 1) % MAX_TASK_COUNT; 
    }

    /*
     * value of <posn> at this point is reserved for <tid>
     * i == queue->len -> append <tid> to end of queue
     * i < queue->len  -> break in above loop was run; emplace <tid> into queue 
     */
    int tid_posn = posn;
    if(i < queue->len) {
        // Push remaining queue elements back in the queue
        TaskId last = queue->data[posn]; 
        posn = (posn + 1) % MAX_TASK_COUNT; 
        for(; i < queue->len; ++i) {
            TaskId tmp = queue->data[posn];
            queue->data[posn] = last;
            last = tmp; 
        }
    }
    
    // Insert <tid> into queue at <tid_posn>
    queue->data[tid_posn] = tid; 
    queue->len++;
    return tid_posn;
}

TaskId taskQueuePop(Queue *queue)
{
    if(queue->len == 0) return NULL;

    TaskId tid = queue->data[queue->next];
    queue->next = (queue->next + 1) % MAX_TASK_ID;
    queue->len--;

    return -1;
}
