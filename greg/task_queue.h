#pragma once

#include <task_descriptor.h>
#include <task_set.h>

struct TaskPriority {
    TaskId tid;
};

/*
 * Description: Priority queue of tasks.
 */
struct TaskQueue
{
    TaskId data[MAX_TASK_COUNT];
    int next; 
    int len;
}
typedef struct TaskQueue TaskQueue

/*
 * Description: Resets task <queue>
 */
void taskQueueReset(TaskQueue *queue)
{
    queue->next = 0;
    queue->len = 0;
}

/*
 * Description: Pushes <tid> into <queue> using <priority> 
 * Returns:
 *              Position in queue [1..MAX_TASK_ID]
 *              -1 if <queue> is invalid
 *              -2 if <tid> is not valid task id
 *              -3 if <priority> is not valid task priority 
 *              -4 if <queue> is full
 */
int taskQueuePush(TaskId tid, TaskPriority priority, TaskSet tasks);

/*
 * Description: Pops task queue
 * Returns:
 *              Next Task Id in <queue> (ie, task w highest priority)
 *              -1 if queue is empty
 */
TaskId taskQueuePop(Queue *queue);

