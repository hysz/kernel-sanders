#include "msg_queue.h"
#include "buffer.h"
#include "debug.h"
#include "common.h"

void queueReset_MSG(MsgQueue* q, int msg_len)
{
    if(msg_len > MAX_MSG_LEN) {
        ERROR("Cannot Reset MSG Queue; msg_len too big: %d > %d", msg_len, MAX_MSG_LEN);
    }
 
    q->next = 0;
    q->len = 0;
    q->msg_len = msg_len;
}

ErrorCode queuePush_MSG(MsgQueue* q, char* message, TaskId sender)
{
    if(q->len == MSG_QUEUE_SIZE) {
        return MSG_QUEUE_FULL; 
    }

    // Extend Queue
    cpyN(q->data[q->next].message, message, q->msg_len);
    q->data[q->next].sender = sender;

    q->next = (q->next + 1) % MSG_QUEUE_SIZE;
    if(q->len < MSG_QUEUE_SIZE) q->len++;

    return NO_ERROR;
}

int queueEmpty_MSG(MsgQueue* q)
{
    return (q->len == 0); 
}

char* queuePeek_MSG(MsgQueue* q)
{
    if( queueEmpty_MSG( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to peek empty MSG Queue");
    }

    int k = q->next - q->len;
    if(k < 0) k += MSG_QUEUE_SIZE;

    return q->data[k].message;
} 

// Popped value will not be available for long, so use it immediately!
char* queuePop_MSG(MsgQueue* q)
{
    if( queueEmpty_MSG( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop from empty MSG Queue");
    }

    int k = q->next - q->len;
    if(k < 0) k += MSG_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;

    return q->data[k].message;
} 
