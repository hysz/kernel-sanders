#pragma once

#include "errno.h"
#include "buffer.h"
#include "task.h"

#define MSG_QUEUE_SIZE 20
#define MAX_MSG_LEN 1024

struct MsgQueueItem {
    char message[MAX_MSG_LEN];
    TaskId sender;
};
typedef struct MsgQueueItem MsgQueueItem;

struct MsgQueue {
    MsgQueueItem data[MSG_QUEUE_SIZE];
    int next; 
    int len;
    int msg_len;
};
typedef struct MsgQueue MsgQueue;

void queueReset_MSG(MsgQueue *q, int msg_len);
ErrorCode queuePush_MSG(MsgQueue * q, char* message, TaskId sender);
int queueEmpty_MSG(MsgQueue* q);
char* queuePop_MSG(MsgQueue* q);
char* queuePeek_MSG(MsgQueue* q);
