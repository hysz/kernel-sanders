#include "courier.h"
#include "common.h"
#include "kapi.h"
#include "debug.h"
#include "errno.h"
#include "nameserver_api.h"
#include "msg_queue.h"

void Courier()
{
    // Initialization Message
    CourierInitMsg init;
    TaskId tid;
    ErrorCode init_error = Receive(&tid, &init, sizeof(init));
    if(init_error) {
        ERROR("Courier Failed to Init %d", init_error);
        Exit();
    }
    Reply(tid, 0, 0);

    TaskId forward = init.forward;
    int message_size = init.message_size;
    char buf[message_size];
    Bool forwarder_waiting = FALSE;
    volatile char** msg_peek = init.msg_peek;

    if(init.name) {
        RegisterAs(init.name);
    }

    MsgQueue msg_queue;
    queueReset_MSG(&msg_queue, message_size);

    FOREVER {
        TaskId sender = INVALID_TID;
        ErrorCode error = Receive(&sender, &buf, message_size);
        if(error) {
            ERROR("Courier Failed to receive %d", error);
            continue;
        }

        int clear = 0;
        cpyN(&clear, &buf, sizeof(clear));
        if(clear == CLEAR_COURIER) {
            ERROR("Clearing Courier");
            queueReset_MSG(&msg_queue, message_size);
            if(msg_peek) *msg_peek = 0;
            continue;
        }

        if(sender != forward) {
            Reply(sender, 0, 0);
            ErrorCode error = queuePush_MSG(&msg_queue, buf, sender);
            if(msg_peek && *msg_peek == 0) {
                *msg_peek = queuePeek_MSG(&msg_queue); 
            }
            if(error) {
                ERROR("Courier Msg Queue Push Failed: %d", error);
            }
        }
        
        if((sender == forward || forwarder_waiting) && ! queueEmpty_MSG(&msg_queue)) {
            char* msg = queuePop_MSG(&msg_queue);
            if(msg_peek) {
                if(queueEmpty_MSG(&msg_queue)) {            
                    *msg_peek = (char*)0;
                } else {
                    *msg_peek = queuePeek_MSG(&msg_queue);
                }
            }
            ErrorCode error = Reply(forward, msg, msg_queue.msg_len);
            if(error) {
                ERROR("Courier Failed to forward %d", error);
            }

            forwarder_waiting = FALSE;
        } else if(sender == forward) {
            forwarder_waiting = TRUE;
        }
    }
}
