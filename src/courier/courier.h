#pragma once

#include "task.h"
#include "names.h"
#include "common.h"

// This special integer
// when passed to the courier will
// force it to clear its message queue.
// The hex translates to 32 bits: 1010....1010
#define CLEAR_COURIER 0xAAAAAAAA

struct CourierInitMsg {
    TaskId forward;
    int message_size;
    Name name;
    int reply_size;
    volatile char** msg_peek;
};
typedef struct CourierInitMsg CourierInitMsg;

void Courier();
