#include "tests.h"
#include "debug.h"
#include "kapi.h"
#include "task.h"

#include "common.h"

#include "nameserver_api.h"

#include "errno.h"

void serverTask()
{
    DEBUG("Server Task Running %d", MyTid());
    
    DEBUG("Registering 69");
    ErrorCode err = RegisterAs(69);
    if(err) DEBUG("Error Registering Test Server: %d", err);

    // Register already registered - should cause error
    DEBUG("Registering 69");
    err = RegisterAs(69);
    if(err) DEBUG("Error Registering Test Server: %d", err);

    DEBUG("UnRegistering 69");
    err = unregisterName(69);
    if(err) DEBUG("Error UnRegistering Test Server: %d", err);

    Exit();
}

// This has MED_PRIORITY
void serverTest()
{
    TaskId tid;
    tid = Create(HI_PRIORITY, &serverTask);
    
    // If calling unregister above, this should cause an error
    // If comment out unregister above, should get valid TID
    DEBUG("Looking up 69");
    tid = WhoIs(69);
    if(! isValidTaskId(tid) ) DEBUG("Error Looking Up 69: %d", tid);
    else    DEBUG("TID of 69: %d", tid);
    
    DEBUG("First: Exiting");
    Exit(); 
}

