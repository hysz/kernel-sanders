#include "scissors.h"
#include "debug.h"
#include "task.h"

void rpsServerTask() {

    //Register with name server
    ErrorCode err = RegisterAs(RPS_SERVER_NAME);
    if(err) {
        DEBUG("\n\rRPS Server Error: %d", err);
        return;
    }

    Match matches[RPS_SERVER_MAX_CONCURRENT_PLAYERS];
    int i = 0;
    for (i = 0; i < RPS_SERVER_MAX_CONCURRENT_PLAYERS; i++) {
        matches[i].p1Tid = 0;
        matches[i].p1Choice = Undecided;
        matches[i].p2Tid = 0;
        matches[i].p2Choice = Undecided;
    }

    RPSMessage recvBuffer = {0};
    TaskId clientTid;

    TaskId waitingTaskTid = NO_PLAYER_WAITING; //A task that is waiting for another task to play with

    FOREVER {
        Receive( &clientTid, &recvBuffer, sizeof(RPSMessage));
        switch (recvBuffer.type) {
            case Signup:
                handleSignup(matches, clientTid, &waitingTaskTid);
                break;

            case Play:
                handlePlay(matches, clientTid, &recvBuffer);
                break;
    
            case Quit:
                handleQuit(matches, clientTid);
                break;

            case Start:
            case Result:
            case QuitAck:
            case Error:
            default:
                handleError(clientTid);
                break;
        }
    }
}

void handleError(TaskId clientTid) {
    RPSMessage errorReply = {0};
    errorReply.type = Error;
    Reply(clientTid, &errorReply, sizeof(RPSMessage));
}

void handleSignup(Match* matches, TaskId clientTid, TaskId* waitingTaskTid) {
    if (*waitingTaskTid == NO_PLAYER_WAITING) {
        *waitingTaskTid = clientTid;
        DEBUG("\n\rTask %d Signed Up!\n\r", clientTid);
        bwgetc(COM2);
    } else {
        //There are 2 tasks that can be paired
        RPSMessage signupReply = {0};
        signupReply.type = Start;
        Reply(*waitingTaskTid, &signupReply, sizeof(RPSMessage));
        Reply(clientTid, &signupReply, sizeof(RPSMessage)); 
        resetMatch(matches, *waitingTaskTid, clientTid);
        DEBUG("\n\rTask %d Signed Up!", clientTid);
        DEBUG("\n\rStarted Match [Task %d vs. Task %d]\n\r", clientTid, *waitingTaskTid);
        bwgetc(COM2);
        *waitingTaskTid = NO_PLAYER_WAITING;
    }
}

void handlePlay(Match* matches, TaskId clientTid, RPSMessage* recvBuffer) {
    Match m = matches[clientTid];
    if (m.p1Tid == clientTid) {
        m.p1Choice = recvBuffer->args[0];     
        matches[m.p2Tid].p1Choice = m.p1Choice;
    } else {
        m.p2Choice = recvBuffer->args[0];
        matches[m.p1Tid].p2Choice = m.p2Choice;
    }

    //Check if all players have decided
    if (m.p1Choice != Undecided && m.p2Choice != Undecided) {
        RPSMessage p1ResultReply = {0};
        p1ResultReply.type = Result;
        RPSMessage p2ResultReply = {0};
        p2ResultReply.type = Result;
        //Decide Outcome
        if (m.p1Choice == m.p2Choice) {
            p1ResultReply.args[0] = TIE; 
            p2ResultReply.args[0] = TIE;
        } else {
            switch (m.p1Choice) {
                case Rock:
                    if (m.p2Choice == Paper) {
                        p1ResultReply.args[0] = LOSE;
                        p2ResultReply.args[0] = WIN;
                    } else {
                        p1ResultReply.args[0] = WIN;
                        p2ResultReply.args[0] = LOSE;
                    }
                    break;

                case Paper:
                    if (m.p2Choice == Scissors) {
                        p1ResultReply.args[0] = LOSE;
                        p2ResultReply.args[0] = WIN;
                    } else {
                        p1ResultReply.args[0] = WIN;
                        p2ResultReply.args[0] = LOSE;
                    }
                    break;

                case Scissors:
                    if (m.p2Choice == Rock) {
                        p1ResultReply.args[0] = LOSE;
                        p2ResultReply.args[0] = WIN;
                    } else {
                        p1ResultReply.args[0] = WIN;
                        p2ResultReply.args[0] = LOSE;
                    }
                    break;
                case Undecided:
                //It should never go here... so this case isnt properly handled
                default:
                    break;

            }
        }

        //Have results. Send it to the players
        printMatch(&m);
        if (p1ResultReply.args[0] == TIE) {
            DEBUG("\n\r[Task %d vs. Task %d]: Tie Game!\n\r", m.p1Tid, m.p2Tid);
        } else if (p1ResultReply.args[0] == WIN) {
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d Wins\n\r", m.p1Tid, m.p2Tid, m.p1Tid);
        } else {
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d Wins\n\r", m.p1Tid, m.p2Tid, m.p2Tid);
        }
        bwgetc(COM2);

        Reply(m.p1Tid, &p1ResultReply, sizeof(RPSMessage));
        Reply(m.p2Tid, &p2ResultReply, sizeof(RPSMessage));

        //Reset the Match
        resetMatch(matches, m.p1Tid, m.p2Tid);
        
    } else if (m.p2Tid == 0) {
        //The other player has quit, return an error
        RPSMessage p1ErrorReply = {0};
        p1ErrorReply.type = Error;
        Reply(m.p1Tid, &p1ErrorReply, sizeof(RPSMessage));
    }
}

void handleQuit(Match* matches, TaskId clientTid) {
    matches[clientTid].p1Tid = 0;
    RPSMessage quitReply = {0};
    quitReply.type = QuitAck;
    Reply(clientTid, &quitReply, sizeof(RPSMessage));
    DEBUG("\n\rTask %d quits\n\r", clientTid);
    bwgetc(COM2);
}

void resetMatch(Match* matches, int p1, int p2) {
    matches[p1].p1Tid = p1;
    matches[p1].p1Choice = Undecided;
    matches[p1].p2Tid = p2;
    matches[p1].p2Choice = Undecided;

    matches[p2] = matches[p1];
}

void printMatch(Match *match) {
    switch (match->p1Choice) {
        case Rock:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays Rock", match->p1Tid, match->p2Tid, match->p1Tid);
            break;
        case Paper:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays Paper", match->p1Tid, match->p2Tid, match->p1Tid);
            break;
        case Scissors:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays Scissors", match->p1Tid, match->p2Tid, match->p1Tid);
            break;
        default:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays undefined", match->p1Tid, match->p2Tid, match->p1Tid);
    } 

    switch (match->p2Choice) {
        case Rock:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays Rock", match->p1Tid, match->p2Tid, match->p2Tid);
            break;
        case Paper:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays Paper", match->p1Tid, match->p2Tid, match->p2Tid);
            break;
        case Scissors:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays Scissors", match->p1Tid, match->p2Tid, match->p2Tid);
            break;
        default:
            DEBUG("\n\r[Task %d vs. Task %d]: Task %d plays undefined", match->p1Tid, match->p2Tid, match->p2Tid);
    } 
}

void rpsClientTask1() {
    TaskId serverTid = WhoIs(RPS_SERVER_NAME);
    if (! isValidTaskId( serverTid ) ) {
        DEBUG("\n\r[Task%d]Error looking up RPS Server tid: %d", MyTid(), serverTid);
        Exit();
    }

    RPSMessage msg;
    RPSMessage reply;
    msg.type = Signup;
    int err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    msg.type = Play;
    msg.args[0] = getRandomInt(3) + 1;
    err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    //Play annother game
    msg.type = Play;
    msg.args[0] = getRandomInt(3) + 1;
    err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    //Send a Quit
    msg.type = Quit;
    err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    //Send another signup
    msg.type = Signup;
    err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    //Play annother game
    msg.type = Play;
    msg.args[0] = getRandomInt(3) + 1;
    err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    //Send a Quit
    msg.type = Quit;
    err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    Exit();
}

void rpsClientTask4Ever () {
    TaskId serverTid = WhoIs(RPS_SERVER_NAME);
    if (! isValidTaskId(serverTid) ) {
        DEBUG("Task %d Error looking up RPS Server tid: %d", MyTid(), serverTid);
        Exit();
    }

    RPSMessage msg;
    RPSMessage reply;
    msg.type = Signup;
    ErrorCode err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
    if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);

    FOREVER {
        msg.type = Play;
        msg.args[0] = getRandomInt(3) + 1;
        err = Send(serverTid, &msg, sizeof(RPSMessage), &reply, sizeof(RPSMessage));
        if (err) DEBUG("Task %d: Error from Send: ", MyTid(), err);
    }
}

void scissorsTestTask()
{
    Create(MED_PRIORITY, &rpsServerTask);

    Create(LOW_PRIORITY, &rpsClientTask1);
    Create(LOW_PRIORITY, &rpsClientTask1);
    Create(LOW_PRIORITY, &rpsClientTask1);
    Create(LOW_PRIORITY, &rpsClientTask1);

    /*int i = 0;
    for (i = 0; i < 90; i++) {
        Create(MED_PRIORITY, &rpsClientTask4Ever); 
    }*/

    Exit();
}

//Random gen functions

int getRandomInt(int n) {
    unsigned int* timer3val = (unsigned int*) (TIMER3_BASE + VAL_OFFSET);

    return *timer3val % n; 
}
