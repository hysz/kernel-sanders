    .text
    .align  2
    .global HwiTest
    .type   HwiTest, %function
HwiTest: 
    mov r0, #1
    mov r1, #65
    mov r2, #66
    mov r3, #4
    mov r4, #68
    mov r5, #69
    mov r6, #70
    mov r7, #71
    mov r8, #72
    mov r9, #73
    #mov r10, #74
    #mov r11, #75
    #mov r12, #76
    #mov r14, #78

    # Trigger Interrupt
    mov r0, #0x80
    mov r0, r0, LSL #24
    mov r1, #0x0B
    mov r1, r1, LSL #16
    add r0, r0, r1
    str r3, [r0, #0x18]

    mov r0, #1

    # Print Stuff
    # #bl bwputc(PLT)    

    ##bl bwputc(PLT)    

    mov r0, #1
    mov r1, r2
    ##bl bwputc(PLT)    

    mov r0, #1
    mov r1, r3
    ##bl bwputc(PLT)    

    mov r0, #1
    mov r1, r4
    ##bl bwputc(PLT)    
    
    mov r0, #1
    mov r1, r5
    ##bl bwputc(PLT)    

    mov r0, #1
    mov r1, r6
    ##bl bwputc(PLT)    

    mov r0, #1
    mov r1, r7
    ##bl bwputc(PLT)    

    mov r0, #1
    mov r1, r8
    #bl bwputc(PLT)    

    mov r0, #1
    mov r1, r9
    #bl bwputc(PLT)    

    mov r0, #1
    mov r1, r10
    #bl bwputc(PLT)    

    mov r0, #1
    mov r1, r11
    #bl bwputc(PLT)    

    mov r0, #1
    mov r1, r12
    #bl bwputc(PLT)    

    mov r0, #1
    mov r1, r13
    #bl bwputc(PLT)    

    mov r0, #1
    mov r1, r14
    #bl bwputc(PLT)    

    .size   HwiTest, .-HwiTest
