#include "tests.h"
#include "debug.h"
#include "kapi.h"
#include "task.h"
#include "scissors.h"

#include "common.h"

#include "nameserver_api.h"

#include "errno.h"

void otherTask()
{
    DEBUG("Task ID: %d, Parent Task ID: %d", MyTid(), MyParentTid());
    Pass();
    DEBUG("Task ID: %d, Parent Task ID: %d", MyTid(), MyParentTid());
    Exit();
}

void otherTask2()
{
    DEBUG("Task ID: %d, Parent Task ID: %d", MyTid(), MyParentTid());
    
    char replyMsg[4];
    replyMsg[0] = 'K';
    replyMsg[1] = 'K';
    replyMsg[2] = 'K';
    replyMsg[3] = '\0';

    char msg[5];
    msg[0] = 'z';
    msg[1] = 'z';
    msg[2] = 'z';
    msg[3] = 'z';
    msg[4] = '\0';
    TaskId tid = 0;
    int i = 0;
    for(i = 0; i < 7; ++i) {
        Receive(&tid,msg,5);
        DEBUG("Task %d Got Message from %d: '%s'", MyTid(), tid, msg);
        replyMsg[1] = '0' + i;
        Reply(tid, replyMsg, 4); 
        
    }

    Exit();
}

void serverTask()
{
    DEBUG("Server Task Running %d", MyTid());
    
    DEBUG("Registering 69");
    ErrorCode err = RegisterAs(69);
    if(err) DEBUG("Error Registering Test Server: %d", err);

    // Register already registered - should cause error
    DEBUG("Registering 69");
    err = RegisterAs(69);
    if(err) DEBUG("Error Registering Test Server: %d", err);

    DEBUG("UnRegistering 69");
    err = unregisterName(69);
    if(err) DEBUG("Error UnRegistering Test Server: %d", err);

    Exit();
}

// This has MED_PRIORITY
void firstTask()
{
    TaskId tid;
    tid = Create(HI_PRIORITY, &serverTask);
    

    DEBUG("Looking up 69");
    tid = WhoIs(69);
    if(tid) DEBUG("Error Looking Up 69: %d", tid);
    else    DEBUG("TID of 69: %d", tid);
    
    DEBUG("First: Exiting");
    Exit(); 
}

