#include "timedreceive_test.h"
#include "debug.h"
#include "message.h"
#include "await_sensor_api.h"
#include "kapi.h"
#include "clockserver_api.h"

void timedReceiveTest() {

    TaskId tid = INVALID_TID;
    int asd = 1;
    ErrorCode err = TimedReceive(&tid, &asd, sizeof(int), 3 * SECOND_DELAY);
    DEBUG("TimedReceive returned: %d", err); 

    Exit();
}
