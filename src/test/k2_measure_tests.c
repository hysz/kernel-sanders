#include "task.h"
#include "kapi.h" 
#include "k2_measure_tests.h"
#include "debug.h"
#include "ts7200.h"
#include "common.h"
#include "kapi.h"

void recvTask()
{
    /* 04 byte message */ char msg[4];
    /* 64 byte message char msg[64]; */

    /* Send Before Recv */ TaskId tid = 1;
    /* Recv Before Send  TaskId tid = 2; */

    DEBUG("Recv Task Starting (%d). Expecting %d bytes from Task %d", MyTid(), sizeof(msg), tid);
    Pass();
    int i = 0;
    for(i = 0; i < 1000; ++i) {
        //DEBUG("Receiving...");
        Receive(&tid, msg, sizeof(msg));
        PCC();

        Reply(tid, msg, sizeof(msg)); 
    }

    Exit();
}

void sendTask()
{
    /* 04 byte message */ char msg[4];
    /* 64 byte message char msg[64]; */

    /* Send Before Recv */ TaskId tid = 2;
    /* Recv Before Send  TaskId tid = 1; */

    DEBUG("Send Task Starting (%d). Sending %d bytes to Task %d", MyTid(), sizeof(msg), tid);
    Pass();

    int i = 0;
    unsigned int tdiff = 0;
    for(i = 0; i < 1000; ++i) {
        /* To enable recv before send:*/ Pass();
        //DEBUG("Sending...");
        unsigned int start = *(int*)(TIMER3_BASE + VAL_OFFSET);
        Send(tid, msg, sizeof(msg), msg, sizeof(msg)); 
        unsigned int end = *(int*)(TIMER3_BASE + VAL_OFFSET);

        /*
            We're using the 508 KHz clock.
            So, it will oscillate 508000 times in a second.
            Convert this to microseconds by doing:
            = (# seconds) * 1000000
            = ( (end - start) / 508000) * 1000000
            = (end - start) * 1000000 / 508000 
            = (end - start) * ( 1000 / 508 ) 
         
         */
        // DEBUG("Start: %u, End: %u", start, end);
        tdiff += (end > start) ? end - start : start - end;
    }
    float factor = (float)1/ (float)508;
     DEBUG("tdiff=%d, Time: %d", tdiff, (int)((float)tdiff * factor));
}
