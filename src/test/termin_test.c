#include "termout_test.h"
#include "io.h"
#include "nameserver_api.h"
#include "names.h"
#include "debug.h"

void terminTest() {
    TaskId channel = WhoIs(TERM_GET_SERVER);

    DEBUG("startin termin test querying channel %d", channel);
    while (1) {
        int c = getc(channel);
        DEBUG("terminTest got char %d", c);
    }
}
