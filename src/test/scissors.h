#pragma once
#include "kapi.h"
#include "common.h"
#include "errno.h"
#include "nameserver_api.h"
#include "ts7200.h"

#define RPS_SERVER_MAX_CONCURRENT_PLAYERS 100
#define RPS_SERVER_RECEIVE_BUFFER_LENGTH 10
#define RPS_MESSAGE_ARGS_SIZE 1
#define NO_PLAYER_WAITING (RPS_SERVER_MAX_CONCURRENT_PLAYERS + 1)

#define RPS_SERVER_NAME 29

typedef enum {Signup, Start, Play, Result, Quit, QuitAck, Error} MessageType;
typedef enum {Undecided, Rock, Paper, Scissors} PlayChoice;
typedef enum {WIN, LOSE, TIE} RoundResult;

struct RPSMessage {
    MessageType type;
    int args[RPS_MESSAGE_ARGS_SIZE];
};
typedef struct RPSMessage RPSMessage;

struct Match {
    TaskId p1Tid;
    PlayChoice p1Choice;
    TaskId p2Tid;
    PlayChoice p2Choice;
};
typedef struct Match Match;

void rpsServerTask();

void rpsClientTask1();

void rpsClientTask2();

void scissorsTestTask();

void printMatch(Match* match);

void resetMatch(Match* matches, int p1, int p2);

void handlePlay(Match* matches, TaskId clientTid, RPSMessage* recvBuffer);

void handleQuit(Match* matches, TaskId clientTid);

void handleError(TaskId clientTid);

void handleSignup(Match* matches, TaskId clientTid, TaskId* waitingTaskTid);

int getRandomInt();
