#include "clockserver_test.h"
#include "clockserver_api.h"
#include "debug.h"
#include "kapi.h"

void basicClockTest()
{
    int i = 0;
    for(; i < 60 * 20; ++i ) {
        Delay(SECOND_DELAY);
        DEBUG("%d", i + 1);
    }

    Exit();
}

struct DelayInfo {
    int delay;
    int num_delays;
};
typedef struct DelayInfo DelayInfo;

static void k3DelayTest()
{
    DelayInfo delay_info;
    int dummy = 69;
    Send(MyParentTid(), &dummy, sizeof(dummy), &delay_info, sizeof(delay_info)); 
    
    TaskId tid = MyTid();
    int i;
    for(i = 1; i <= delay_info.num_delays; ++i ) {
        Delay(delay_info.delay);
        DEBUG("Task %d: Interval %d, Delayed %d Times", tid, delay_info.delay, i); 
    }

    Exit();
}

void k3ClockTest()
{
    TaskId tid;

    tid = Create(3, &k3DelayTest);
    if( ! isValidTaskId(tid) ) {
        ERROR("Failed to create Delay Test with Priority 3");
        Exit();
    }
    DEBUG("Created Priority 3 Delay Test (tid=%d)", tid);

    tid = Create(4, &k3DelayTest);
    if( ! isValidTaskId(tid) ) {
        ERROR("Failed to create Delay Test with Priority 4");
        Exit();
    }
    DEBUG("Created Priority 4 Delay Test (tid=%d)", tid);

    tid = Create(5, &k3DelayTest);
    if( ! isValidTaskId(tid) ) {
        ERROR("Failed to create Delay Test with Priority 5");
        Exit();
    }
    DEBUG("Created Priority 5 Delay Test (tid=%d)", tid);

    tid = Create(6, &k3DelayTest);
    if( ! isValidTaskId(tid) ) {
        ERROR("Failed to create Delay Test with Priority 6");
        Exit();
    }
    DEBUG("Created Priority 6 Delay Test (tid=%d)", tid);

    DelayInfo delays[4];
    delays[0].delay      = 10;
    delays[0].num_delays = 20;
    delays[1].delay      = 23;
    delays[1].num_delays = 9;
    delays[2].delay      = 33;
    delays[2].num_delays = 6;
    delays[3].delay      = 71;
    delays[3].num_delays = 3;
    
    int i = 0;
    for(; i < 4; ++i ) {
        TaskId recv_tid;
        int msg;
        Receive(&recv_tid, &msg, sizeof(msg));

        bwprintf(COM2, "First User Task: Got message from Task %d", recv_tid); 
        Reply(recv_tid, &delays[i], sizeof(delays[i]));
    }
    
    Exit();
}
