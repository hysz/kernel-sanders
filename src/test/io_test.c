#include "io_test.h"
#include "io.h"
#include "nameserver_api.h"
#include "names.h"
#include "debug.h"
#include "kapi.h"
#include "clockserver_api.h"

void fifoTermTest()
{
    TaskId channel = WhoIs(TERM_PUT_SERVER);

    Delay(200);

    int i = 0;
    for(; i < 100; ++i) {
        while(putD(channel, i)) {
            Delay(1);
        }
    }

        while(putS(channel, "DONE\r\n")) {
            Delay(1);
        }

    Exit();
}

void fifoTermTestEsc()
{
    TaskId channel = WhoIs(TERM_PUT_SERVER);

    Delay(200);

    putS(channel, "\033[2J\033[?25l");

    int i = 0;
    for(; i < 100; ++i) {
        while(putS(channel, "\033[HGREG HYSEN\r\n")) {
            Delay(1);
        }
    }

        while(putS(channel, "DONE\r\n")) {
            Delay(1);
        }

    Exit();
}

void fifoTimeoutTermTest()
{
    // This won't fill entire FIFO, but should be printed due to timeout
    TaskId channel = WhoIs(TERM_PUT_SERVER);
    putc(channel, 'G');
    putc(channel, 'R');
    putc(channel, 'E');
    putc(channel, 'G');

    Exit();
}

void simpleTrainPutTest()
{
    TaskId channel = WhoIs(TRAIN_PUT_SERVER);
    if(! isValidTaskId(channel) ) {
        DEBUG("FAIL");
    }
    
    TaskId term_channel = WhoIs(TERM_PUT_SERVER);

    putS(term_channel, "\r\nSIR GREGORY\r\n");
    putc(term_channel, 'G');
    if(putc(channel, 0x60)) {DEBUG("FAIL");}
    putc(channel, 5);
    putc(channel, 58);
    putc(channel, 0x60);
    putc(term_channel, 'S');
}

void simpleTermPutTest()
{
    TaskId channel = WhoIs(TERM_PUT_SERVER);

    putc(channel, 'G');
    putc(channel, 'R');
    putc(channel, 'E');
    putc(channel, 'G');
    putc(channel, ' ');
    putc(channel, 'H');
    putc(channel, 'Y');
    putc(channel, 'S');
    putc(channel, 'E');
    putc(channel, 'N');
}
