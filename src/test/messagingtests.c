#include "tests.h"
#include "debug.h"
#include "kapi.h"
#include "task.h"
#include "scissors.h"

#include "common.h"

void otherTask()
{
    DEBUG("Task ID: %d, Parent Task ID: %d\r\n", MyTid(), MyParentTid());
    Pass();
    DEBUG("Task ID: %d, Parent Task ID: %d\r\n", MyTid(), MyParentTid());
    Exit();
}

void otherTask2()
{
    DEBUG("Task ID: %d, Parent Task ID: %d\r\n", MyTid(), MyParentTid());
    
    char replyMsg[4];
    replyMsg[0] = 'K';
    replyMsg[1] = 'K';
    replyMsg[2] = 'K';
    replyMsg[3] = '\0';

    char msg[5];
    msg[0] = 'z';
    msg[1] = 'z';
    msg[2] = 'z';
    msg[3] = 'z';
    msg[4] = '\0';
    TaskId tid = 0;
    int i = 0;
    for(i = 0; i < 7; ++i) {
        Receive(&tid,msg,5);
        DEBUG("Task %d Got Message from %d: '%s'\r\n", MyTid(), tid, msg);
        replyMsg[1] = '0' + i;
        Reply(tid, replyMsg, 4); 
        
    }

    Exit();
}

// This has MED_PRIORITY
void firstTask()
{
    TaskId tid;
    
/*
    tid = Create(LOW_PRIORITY, &otherTask); 
    DEBUG("Created: %d\r\n", tid);

    tid = Create(LOW_PRIORITY, &otherTask); 
    DEBUG("Created: %d\r\n", tid);

    tid = Create(HI_PRIORITY, &otherTask); 
    DEBUG("Created: %d\r\n", tid);

    tid = Create(HI_PRIORITY, &otherTask); 
    DEBUG("Created: %d\r\n", tid);
*/

    tid = Create(LOW_PRIORITY, &otherTask2);
    DEBUG("Created: %d\r\n", tid);

    char msg[5];
    msg[0] = 'a';
    msg[1] = 'b';
    msg[2] = 'c';
    msg[3] = 'd';
    msg[4] = '\0';
    char reply[5];
    DEBUG("\n\rSent First Task");
    Send(tid, msg, 5, reply, 4);
    DEBUG("\n\r Got reply %s]", reply);

    msg[0] = 'a';
    msg[1] = 'a';
    msg[2] = 'c';
    msg[3] = 'd';
    msg[4] = '\0';
    DEBUG("\n\rSent 2nd Task");
    Send(tid, msg, 5, reply, 4);
    DEBUG("\n\r Got 2nd reply %s]", reply);

    msg[0] = 'a';
    msg[1] = 'a';
    msg[2] = 'a';
    msg[3] = 'd';
    msg[4] = '\0';
    Send(tid, msg, 5, reply, 4);
    DEBUG("\n\r Got reply %s]", reply);

    msg[0] = 'a';
    msg[1] = 'a';
    msg[2] = 'a';
    msg[3] = 'a';
    msg[4] = '\0';
    Send(tid, msg, 5, reply, 4);
    DEBUG("\n\r Got reply %s]", reply);

    msg[0] = 'a';
    msg[1] = 'a';
    msg[2] = 'a';
    msg[3] = 'd';
    msg[4] = '\0';
    Send(tid, msg, 5, reply, 4);
    DEBUG("\n\r Got reply %s]", reply);

    msg[0] = 'a';
    msg[1] = 'a';
    msg[2] = 'c';
    msg[3] = 'd';
    msg[4] = '\0';
    Send(tid, msg, 5, reply, 4);
    DEBUG("\n\r Got reply %s]", reply);

    msg[0] = 'a';
    msg[1] = 'b';
    msg[2] = 'c';
    msg[3] = 'd';
    msg[4] = '\0';
    Send(tid, msg, 5, reply, 4);
    DEBUG("\n\r Got reply %s]", reply);

    DEBUG("First: Exiting\r\n");
    Exit(); 
}

