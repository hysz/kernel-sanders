#include "termout_test.h"
#include "io.h"
#include "nameserver_api.h"
#include "names.h"
#include "debug.h"

void simpleTest()
{
    TaskId channel = WhoIs(TERM_PUT_SERVER);

    bwprintf(COM2, "12");

    putc(channel, 'G');
    putc(channel, 'R');
    putc(channel, 'E');
    putc(channel, 'G');
    putc(channel, ' ');
    putc(channel, 'H');
    putc(channel, 'Y');
    putc(channel, 'S');
    putc(channel, 'E');
    putc(channel, 'N');
}

void complexTest() {
    TaskId getServer = WhoIs(TERM_GET_SERVER);
    TaskId putServer = WhoIs(TERM_PUT_SERVER);
    while(1) {
        char c = getc(getServer);
        putc(putServer, c);
    }
}

