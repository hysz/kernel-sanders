    .text
    .align  2
    .global kerinit 
    .type   kerinit, %function
kerinit: 
    mov ip, sp
    stmfd   sp!, {fp, ip, lr, pc}    
    sub     fp, ip, #4

    # Enter Kernel Mode
    msr cpsr_c, #0x93

    # Load CP15 into R0
    mrc P15, 0, R0, C1, C0, 0

    # Enable I-Cache, D-Cache, and MMU
    orr R0, R0,  #0
    orr R0, R0,  #0x4
    orr R0, R0,  #0x1000
    
    # Writeback to CP15
    mcr P15, 0, R0, C1, C0, 0

    # Return to caller
    ldmfd sp, {fp, sp, pc}
