#include "common.h"
#include "context_switch.h"
#include "task_set.h"
#include "debug.h"
#include "message.h"
#include "kapi.h"
#include "ts7200.h"

void newHandle(int *user_args, Task* active_task, TaskSet* tasks, Message *messages, EventWaitInfo* waitInfoTable, int* eventIdTable, int track_ready[2])
{
    enum {OPCODE, SP, PSR, PC, P1, P2, P3, P4, P5, HWR};

    active_task->kapi_args = user_args;
    active_task->sp = user_args[SP];
    active_task->cpsr = user_args[PSR];
    active_task->pc = user_args[PC] + PROGRAM_OFFSET;

    switch(user_args[OPCODE]) {
    case OPCODE_CREATE:
        // KDEBUG("CREATE");
        active_task->retval = createTask((TaskId) (user_args[P1]), (void*) (user_args[P2]), tasks, active_task->id);
        break;

    case OPCODE_MYTID:
        // KDEBUG("MyTid");
        active_task->retval = active_task->id;
        break;

    case OPCODE_MYPARENTTID:
        // KDEBUG("MyParentTid");
        active_task->retval = active_task->parent;
        break;

    case OPCODE_MYPRIORITY:
        active_task->retval = active_task->priority;
        break;

    case OPCODE_PASS:
        // KDEBUG("Pass");
        active_task->retval = 0;
        break;

    case OPCODE_EXIT:
        // KDEBUG("Exit");
        active_task->retval = 0;
        active_task->state = ZOMBIE;
        break;

    case OPCODE_SEND:
        // KDEBUG("Send");
        active_task->retval = sendMessage( (TaskId) user_args[P1]     /* TID to Send to */
                                         , (void*)  user_args[P2]     /* Message */
                                         , (int)    user_args[P3]     /* Message Len */ 
                                         , (void*)  user_args[P4]     /* Reply */
                                         , (int)    user_args[P5]     /* Reply Len */
                                         , active_task
                                         , messages
                                         , tasks
                                         );
        break;

    case OPCODE_RECEIVE:
        //KDEBUG("Recv");
        active_task->retval = receiveMessage(active_task, (TaskId*) user_args[P1], (void*) user_args[P2], (int) user_args[P3], messages, tasks, 0);
        break;
    case OPCODE_PEEK:
        //KDEBUG("Peek");
        active_task->retval = receiveMessage(active_task, (TaskId*) user_args[P1], (void*) user_args[P2], (int) user_args[P3], messages, tasks, 1);
        break;

    case OPCODE_REPLY:
        //KDEBUG("Reply");
        active_task->retval = replyMessage(user_args[P1], (void*) user_args[P2], user_args[P3], messages, tasks);
        break;

    case OPCODE_AWAIT_EVENT:
        //KDEBUG("AwaitEvent");
        active_task->retval = awaitEvent(active_task, user_args[P1], (void*) user_args[P2], user_args[P3], waitInfoTable, eventIdTable);
        break;

    case OPCODE_HWI:
        //KDEBUG("\n\rHardware Interrupt");
        active_task->retval = user_args[P1];// Value in r0
        handleHWI(tasks, waitInfoTable, eventIdTable, track_ready);
        break;

    default:
        KDEBUG("UNKNOWN OPCODE=%d", user_args[OPCODE]);
        active_task->retval = user_args[P1];// Value in r0
        break; 
    }
}

void handleHWI(TaskSet* tasks, EventWaitInfo* waitInfoTable, int* eventBitTable, int track_ready[2]) {
    //KDEBUG("HWI");
    //Determine what triggered the interrupt
    int event = 69;

    ErrorCode err = getInterruptId(&event, eventBitTable);
    if (err) {
        KDEBUG("\n\rGOT error from getInterruptids: %d", err);
    }

    //Clear the interrupt
    switch (event) {
        case TIMER_EVENT:
            *(int*)(TIMER3_CLEAR_ADDR) = 69;
            err = wakeUpTask(event, waitInfoTable, tasks);
            if (err) {
                KERROR("\n\rGOT error from Wake Up Task for timer event: %d", err);
            }
            break;
        
        case UART1_OR_EVENT:
            handleTrainEvent(waitInfoTable, eventBitTable, tasks, track_ready);
            break;

        case UART2_OR_EVENT:
            handleTermEvent(waitInfoTable, eventBitTable, tasks);
            break;

        default:
            KERROR("\n\rGOT AN UNEXPECTED EVENT: %d", event);
            break;
    }

    //Wake up task
}

int handleTermEvent(EventWaitInfo* waitInfoTable, int* eventBitTable, TaskSet* tasks) {
    //Determine what kind of event this is
    int* interrupt_flags = (int*)(UART2_BASE + UART_INTR_OFFSET);

    if (*interrupt_flags & RIS_MASK || *interrupt_flags & RTIS_MASK) {
        int* errReg = (int*)(UART2_BASE + UART_RSR_OFFSET);
        checkRxError(errReg);

        EventWaitInfo* ewi = &(waitInfoTable[TERM_GET_READY]);
        int* data = (int*)(UART2_BASE + UART_DATA_OFFSET);
        int* flags = (int*)(UART2_BASE + UART_FLAG_OFFSET);

        int index = 0;
        char buf[20];

        //Receive till RTIS is GONE
        while(!(*flags & RXFE_MASK)) {
            if (index > 15) {
                //We shall get the rest of the bytes at a better time
                break;
            }
            buf[index] = *data;
            index++;
        }


        buf[16] = (char) index;

        //Now that the interrupt is cleared, check if anyone is waiting for it
        if (ewi->waiter == UNOCCUPIED_WAIT_TID) {
            KERROR("Handle Term RX waiter is UNNOCUPIED_WAIT_TID");
        }

        ErrorCode err = transferEventData(ewi, buf, 20);
        if (err) {
            KDEBUG("Error transferring UART2 RCV Data of size:  %d", index);
            return err;
        }

        err = wakeUpTask(TERM_GET_READY, waitInfoTable, tasks);
        if (err) {
            KDEBUG("Error Waking up task waiting on TERM_GET_READY:");
            return err;
        }
        index = 0;
    }

    if (*interrupt_flags & TIS_MASK) {
        //There is atleast one char that can be put onto TX q
        *(int*)(UART2_TX_CLEAR_ADDR) &= ~TIS_MASK; // must be 0
        ErrorCode err = wakeUpTask(TERM_PUT_READY, waitInfoTable, tasks);
        if (err) {
            KDEBUG("Error Waking up task waiting on TERM_GET_READY:");
            return err;
        }
    }
    return NO_ERROR;
}

int handleTrainEvent(EventWaitInfo* waitInfoTable, int* eventBitTable, TaskSet* tasks, int track_ready[2]) {
    //Determine what kind of event this is
    int* interrupt_flags = (int*)(UART1_BASE + UART_INTR_OFFSET);

    if ((*interrupt_flags & RIS_MASK) || (*interrupt_flags & RTIS_MASK)) {

        //Check error register
        int* errReg = (int*)(UART1_BASE + UART_RSR_OFFSET);
        checkRxError(errReg);

        //There is atleast one char to receive
        EventWaitInfo* ewi = &(waitInfoTable[TRAIN_GET_READY]);
        int* data = (int*)(UART1_BASE + UART_DATA_OFFSET);
        int* flags = (int*)(UART1_BASE + UART_FLAG_OFFSET);

        int index = 0;
        char buf[20];

        //Receive till RTIS is GONE
        while(!(*flags & RXFE_MASK)) {
            if (index > 15) {
                //We shall get the rest at a better time
                break;
            }

            buf[index] = *data;
            index++;
        }

        buf[16] = (char) index;

        //Now that the interrupt is cleared, check if anyone is waiting for it
        if (ewi->waiter == UNOCCUPIED_WAIT_TID) {
            KERROR("Handle TRAIN RX waiter is UNNOCUPIED_WAIT_TID");
        }

        ErrorCode err = transferEventData(ewi, buf, 20);
        if (err) {
            KDEBUG("Error transferring UART1 RCV Data of size:  %d", index);
            return err;
        }

        err = wakeUpTask(TRAIN_GET_READY, waitInfoTable, tasks);
        if (err) {
            KDEBUG("Error Waking up task waiting on TRAIN_GET_READY:");
            return err;
        }
        index = 0;
    }

    // Wait for TX and Modem Interrupts
    // When we get a modem interrupt, ensure that CTS is set 
    // See page 14-26
    if(*interrupt_flags & TIS_MASK) {
        *(int *)(UART1_BASE + UART_CTLR_OFFSET) &= ~TIEN_MASK;
        *interrupt_flags &= ~TIS_MASK;
        //KERROR("TIS MASK!");

        track_ready[0] = 1;
    }

    if(*interrupt_flags & MIS_MASK) {
        //KERROR("MSIEN MASK!");
        *interrupt_flags &= ~MIS_MASK;

        if(*(int*)(UART1_BASE + UART_MDMSTS_OFFSET) & 0x1 && *(int*)(UART1_BASE + UART_MDMSTS_OFFSET) & 0x10) {
            //KERROR("ALL READY!");
            *(int *)(UART1_BASE + UART_CTLR_OFFSET) &= ~MSIEN_MASK;
            track_ready[1] = 1;
        }
    }
/*
    if( (*interrupt_flags & MIS_MASK)) {
        KERROR("MIS MASK!");
        KERROR("Delta: %d, CTS: %d", *(const int*)(UART1_BASE + UART_MDMSTS_OFFSET) & 0x1, *(const int*)(UART1_BASE + UART_MDMSTS_OFFSET) & (0x10));



        if(*(const int*)(UART1_BASE + UART_MDMSTS_OFFSET) & (0x1 | 0x10)) {
            *(int *)(UART1_BASE + UART_CTLR_OFFSET) &= ~MSIEN_MASK;
            txready_two = 1;
        }
    }
*/
        
    if ( track_ready[0] && track_ready[1] ) {
        track_ready[0] = 0;
        track_ready[1] = 0;
        ErrorCode err = wakeUpTask(TRAIN_PUT_READY, waitInfoTable, tasks);
        if (err) {
            KDEBUG("\n\rError Waking up task waiting on TERM_GET_READY:");
            return err;
        }
    }
    return NO_ERROR;
}

void checkRxError(int* errReg) {
        //Check error register
        if (*errReg != 0) {
            if (*errReg & OE_MASK) {
                KERROR("Rx Overrun: %x", errReg);
            } else if (*errReg & BE_MASK) { 
                KERROR("Rx BREAK bit error %x", errReg);
            } else if (*errReg & PE_MASK) {
                KERROR("Rx Parity bit error %x", errReg);
            } else if (*errReg & FE_MASK) {
                KERROR("Rx Framing error %x", errReg);
            }
            *errReg = 0;
        }
}
