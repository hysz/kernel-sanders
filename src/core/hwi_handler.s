    .text
    .align  2
    .global HwiHandler
    .type   HwiHandler, %function
HwiHandler: 
    # Switch to system mode
    msr CPSR_c, #0x9F

    # Backup Tasks stuff
    # r0-10
    # fp
    # ip
    # sp
    # lr
    # pc (lr of IRQ)

    # Backup Registers
    sub sp, sp, #12
    stmfd   sp, {r0-r10, fp, ip}
    sub sp, sp, #52    

    # store original stack pointer
    add ip, sp, #64
    add sp, sp, #60
    stmfd sp, {ip, lr}
    sub sp, sp, #60

    # Store PC
    add sp, sp, #64
     
    # 
    msr CPSR_c, #0x92
    add r3, lr, #-4
    msr CPSR_c, #0x9F
    stmfd   sp, {r3}

   #  ldr r3, =ReturnFromHWI
   #  add r3, r3, #0x218000
    # stmfd   sp, {r3}
    sub sp, sp, #64

    #
    sub     fp, ip, #4

    # operation args
    ldr r4, [fp, #4] 
    mov r5, sp
    stmfd r5!, {r0-r4}
    # 5 args
    mov r4, #5

    # Keep r0 (50 is the HWI OPCODE defined in kapi.h)
    mov r0, #50

    mov r1, sp

    msr CPSR_c, #0x92
    mrs r2, spsr
    # mov r2, #16
    msr CPSR_c, #0x9F
    
    ldr r3, =ReturnFromHWI

    stmfd r5!, {r0-r3}

    # Enter Kernel Mode
    msr CPSR_c, #0x93

    # This will add return value to stack so when we pop kernel will get it
    stmfd sp!, {r5}

    # Restore Kernel
    ldmia sp, {r0-r10, fp, sp, pc}

    ReturnFromHWI: 
    # mov r0, #1
    # mov r1, #65
    # bl bwputc(PLT)
    ldmfd   sp, {r0-r10, fp, ip, sp, lr, pc}

    
    .size   HwiHandler, .-HwiHandler
