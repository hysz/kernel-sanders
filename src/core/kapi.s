    .text
    .align  2
    .global Create 
    .type   Create, %function
Create: 
    swi #1
    .size   Create, .-Create

####################################################
####################################################

    .text
    .align  2
    .global MyTid 
    .type   MyTid, %function
MyTid: 
    swi #2
    .size   MyTid, .-MyTid

####################################################
####################################################

    .text
    .align  2
    .global MyParentTid 
    .type   MyParentTid, %function
MyParentTid: 
    swi #3
    .size   MyParentTid, .-MyParentTid

####################################################
####################################################

    .text
    .align  2
    .global Pass 
    .type   Pass, %function
Pass: 
    swi #4
    .size   Pass, .-Pass

####################################################
####################################################

    .text
    .align  2
    .global Exit 
    .type   Exit, %function
Exit: 
    swi #5
    .size   Exit, .-Exit

####################################################
####################################################

    .text
    .align  2
    .global Send 
    .type   Send, %function
Send: 
    swi #6
    .size   Send, .-Send

####################################################
####################################################

    .text
    .align  2
    .global Receive 
    .type   Receive, %function
Receive: 
    swi #7
    .size   Receive, .-Receive

####################################################
####################################################

    .text
    .align  2
    .global Reply 
    .type   Reply, %function
Reply: 
    swi #8
    .size   Reply, .-Reply

####################################################
####################################################

    .text
    .align  2
    .global AwaitEvent 
    .type   AwaitEvent, %function
AwaitEvent: 
    swi #9
    .size   AwaitEvent, .-AwaitEvent

####################################################
####################################################

    .text
    .align  2
    .global Peek 
    .type   Peek, %function
Peek: 
    swi #10
    .size   Peek, .-Peek

####################################################
####################################################

    .text
    .align  2
    .global MyPriority
    .type   MyPriority, %function
MyPriority: 
    swi #11
    .size   MyPriority, .-MyPriority

####################################################
####################################################
