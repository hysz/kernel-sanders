#pragma once

#include "task.h"


typedef enum {OPCODE_CREATE = 1, OPCODE_MYTID, OPCODE_MYPARENTTID, OPCODE_PASS, OPCODE_EXIT, OPCODE_SEND, OPCODE_RECEIVE, OPCODE_REPLY, OPCODE_AWAIT_EVENT, OPCODE_PEEK, OPCODE_MYPRIORITY, OPCODE_HWI = 50}OpCode; 

int Create(int priority, void (*code) ());
int MyTid();
int MyPriority();
int MyParentTid();
void Pass();
void Exit();

int Send(TaskId tid, void *msg, int msg_len, void *reply, int replylen);
int Receive(TaskId *tid, void *msg, int msglen);
int Peek(TaskId* tid, void* msg, int msglen);
int Reply(TaskId tid, void *reply, int replylen);

int AwaitEvent(int eventid, void* event, int eventlen);

