    .text
    .align  2
    .global SwiHandler
    .type   SwiHandler, %function
SwiHandler: 
    # Switch to system mode
    msr CPSR_c, #0x9F

    stmfd   sp, {r1-r10, fp, ip, sp, lr, pc}    
    mov ip, sp
    sub sp, sp, #60 
    sub     fp, ip, #4

    # operation args
    ldr r4, [fp, #4] 
    mov r5, sp
    stmfd r5!, {r0-r4}
    # 5 args
    mov r4, #5

    # kerhandle parameters *do not change order* 
     msr CPSR_c, #0x93
     LDR r0,[lr,#-4]
     BIC r0,r0,#0xFF000000
     msr CPSR_c, #0x9F

    # mov r0, #6
    mov r1, sp

    msr CPSR_c, #0x93
    mrs r2, spsr
    msr CPSR_c, #0x9F

    ldr r3, =ReturnFromSWI
    stmfd r5!, {r0-r3}

    # Go into kernel
    msr CPSR_c, #0x93
    stmfd sp!, {r5}
    ldmia sp, {r0-r10, fp, sp, pc}

    ReturnFromSWI: ldmfd   sp, {r1-r10, fp, ip, sp, pc}

    .size   SwiHandler, .-SwiHandler
