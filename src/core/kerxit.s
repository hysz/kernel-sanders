    .text
    .align  2
    .global kerxit 
    .type   kerxit, %function
kerxit: 
    # Push kernel registers onto stack    
    mov ip, sp
    stmfd sp!, {r1-r10, fp,ip,lr}

    # Go into user mode to set stack for user
    mov ip, sp
    msr CPSR_c, #0x9F
    mov sp, r0
    msr CPSR_c, #0x93

    # Set spsr_svc to cpsr_usr 
    msr spsr, r1

    # Get PC of Task
    mov lr, r2

    #set r0 to return value of task
    mov r0, r3

    # Run task
    movs pc, lr

    # Return to kernel 
    # We should not reach this point in execution 
    # This cmd will be run within the swi and hwi handlers
    stmfd sp, {r1-r10, fp, sp, pc}
    .size   kerxit, .-kerxit
