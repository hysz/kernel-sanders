#pragma once
#include "task.h"
#include "message.h"
#include "event.h"
#include "buffer.h"

void newHandle(int *user_args, Task* active_task, TaskSet* tasks, Message *messages, EventWaitInfo* waitInfoTable, int* eventIdTable, int track_ready[2]);

void handleHWI(TaskSet* tasks, EventWaitInfo* waitInfoTable, int* eventIdTable, int track_ready[2]);

int handleTermEvent(EventWaitInfo* waitInfoTable, int* eventBitTable, TaskSet* tasks);

int handleTrainEvent(EventWaitInfo* waitInfoTable, int* eventBitTable, TaskSet* tasks, int track_ready[2]);

void checkRxError(int* errReg);

int* kerxit(int sp, int psr, int pc, int retval);

// See assembly files for these
void SwiHandler();
void HwiHandler();
void kerinit();
