#include "await_sensor_queue.h"
#include "buffer.h"
#include "debug.h"
#include "common.h"

void queueReset_ASQ(ASQueue* q)
{
    q->next = 0;
    q->len = 0;
}

ErrorCode queuePush_ASQ(ASQueue* q, int sensorNum, float distance, int trainNum)
{
    if(q->len == ASQ_QUEUE_SIZE) {
        return MSG_QUEUE_FULL; 
    }

    // Extend Queue
    q->data[q->next].sensorNum = sensorNum;
    q->data[q->next].distance = distance;
    q->data[q->next].trainNum = trainNum;

    q->next = (q->next + 1) % ASQ_QUEUE_SIZE;
    if(q->len < ASQ_QUEUE_SIZE) q->len++;

    return NO_ERROR;
}

int queueEmpty_ASQ(ASQueue* q)
{
    return (q->len == 0); 
}

ASQueueItem* queuePeek_ASQ(ASQueue* q)
{
    if( queueEmpty_ASQ( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to peek empty ASQ Queue");

    }

    int k = q->next - q->len;
    if(k < 0) k += ASQ_QUEUE_SIZE;

    return &(q->data[k]);
} 

// Popped value will not be available for long, so use it immediately!
ASQueueItem* queuePop_ASQ(ASQueue* q)
{
    if( queueEmpty_ASQ( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop from empty ASQ Queue");
    }

    int k = q->next - q->len;
    if(k < 0) k += ASQ_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;

    return &(q->data[k]);
} 

//Finds a value in a queue
