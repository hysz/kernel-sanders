#pragma once
#include "common.h"
#include "sensor_queue.h"
#include "errno.h"
#include "names.h"
#include "task.h"
#include "sensorserver.h"
#include "node_queue.h"
#include "path_finder.h"

#define SENSOR_SERVER_TID 17
#define MAX_SENSORS 80
#define NUM_TRAINS 6
#define AWAIT_SENSOR_TIMEOUT 69
#define INVALID_SENSOR 2929
#define MAX_LOOK_AHEAD 8
#define MAX_BROKEN_SENSORS 30
#define NO_ALTERNATE_PATH 0

#define SENSOR_INACTIVE (char) 0
#define SENSOR_ACTIVE (char) 1
#define SENSOR_BROKEN (char) 2

typedef enum {DATA, WANT_DATA, AWAIT, AWAIT_ALL, SENSOR_TIMEOUT, BROKEN_SENSOR, WAKEUP_TRAIN, ATTRIBUTE, TRAIN_INIT, TRAIN_RESET, ATTRIBUTE_ALT_TURNOUT, ATTRIBUTE_PATH} SensorProviderMsg_Type;

struct SensorData {
    int type;

    union {
        int* activeSensors;
        int sensorNum;
        TaskId courierTid;
        TrackNode destination;
    };

    union {
        int len;
        int timeout;
        TaskId trainTid;
        Path* altPath;
    };
    int distance;
    int trainNum;
    TrackNode startNode;
};
typedef struct SensorData SensorDataMsg;
typedef struct SensorData SensorsRequest;
typedef struct SensorData SensorTimeoutMsg;
typedef struct SensorData SensorBrokenMsg;
typedef struct SensorData WakeUpTrainMsg;
typedef struct SensorData TrainInitSSMsg;
typedef struct SensorData AttributePathMsg;

struct SensorsReply {
    ErrorCode error;
    int time;       // time from call until sensor hit
    int sensorNum;  //Actual Sensor triggered
    int real_time; // clock's time on sensor hit 
    int distance;
}; 
typedef struct SensorsReply SensorsReply;

void SensorServer();
