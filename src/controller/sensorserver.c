#include "sensorserver.h"
#include "common.h"
#include "kapi.h"
#include "nameserver_api.h"
#include "debug.h"
#include "io.h"
#include "clockserver_api.h"
#include "sensor_watchdog.h"
#include "track_helpers.h"
#include "path_finder_api.h"
#include "await_sensor_queue.h"
#include "track_locker.h"
#include "courier.h"
#include "new_train.h"

void printBrokenSensors(int* brokenSensors, int numBrokenSensors, TaskId channel) {
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    int startLine = 3;
    
    bufferPutStr(&b, "\033[");
    bufferPutInt(&b, startLine);
    bufferPutStr(&b, ";70H---BS---");

    int i = 0;
    for (i = 0; i < numBrokenSensors; i++) {
        bufferPutStr(&b, "\033[");
        bufferPutInt(&b, startLine + i + 1);
        bufferPutStr(&b, ";70H   ");
        bufferPutStr(&b, trackNode_IndexToName(brokenSensors[i]));
    }

    putB(channel, &b);
}

void SensorServer() {


    DEBUG("SENSORS PROVIDER TASK STARTED");
    int i;

    char sensorTable[MAX_SENSORS];
    for (i = 0; i < MAX_SENSORS; i++) {
        sensorTable[i] = (char) 0;
    }

    int prevActiveSensors[NUM_TRAINS];
    for (i = 0; i < NUM_TRAINS; i++) {
        prevActiveSensors[i] = 0;
    }
    int prevLen = 0;

    const int MAX_WAITING_ANY = 10;
    TaskId waitingAnySensor[MAX_WAITING_ANY];
    for (i = 0; i < MAX_WAITING_ANY; i++) {
        waitingAnySensor[i] = INVALID_TID;
    }
    int waitingAny = 0;

    TaskId waitingTrains[MAX_SENSORS];
    for (i = 0; i < MAX_SENSORS; i++) {
        waitingTrains[i] = INVALID_TID;
    }

    //Reverse relationship of waitingTrains table
    int awaitingSensors[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        awaitingSensors[i] = INVALID_SENSOR;
    }

    int startTime[MAX_TASK_COUNT];
    for (i = 0; i < MAX_TASK_COUNT; i++) {
        startTime[i] = 0;
    }

    int trainPrevSensor[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        trainPrevSensor[i] = INVALID_SENSOR;
    }

    TaskId courierTids[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        courierTids[i] = INVALID_TID;
    }

    int trainNums[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        trainNums[i] = 0;
    }

    TaskId trainIndex[NUM_TRAINS];
    for(i = 0; i < NUM_TRAINS; i++) {
        trainIndex[i] = INVALID_TID;
    }
    int numTrainsServed = 0;

    int brokenSensors[MAX_BROKEN_SENSORS];
    int numBrokenSensors = 0;

    //Queues of sensors for each train
    ASQueue sensorQueues[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        queueReset_ASQ(&(sensorQueues[i]));
    }

    int alternateTurnoutSensors[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        alternateTurnoutSensors[i] = INVALID_SENSOR;
    }

    TrackNode trainDestinations[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        trainDestinations[i] = INVALID_SENSOR;
    }

    TrackNode trainStartNodes[MAX_TASK_COUNT];
    for(i = 0; i < MAX_TASK_COUNT; i++) {
        trainStartNodes[i] = INVALID_SENSOR;
    }

    const track_node* track;
    getTrack(&track);

    TaskId term_out_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(term_out_channel) ) {
        ERROR("Switches failed to lookup term out channel: %d", term_out_channel);
        Exit();
    }

    // Get Track Locker
    TaskId track_locker = WhoIs(TRACK_LOCKER);
    while(! isValidTaskId(track_locker) ){
        ERROR("Failed to get track locker: %d. Trying again..", (ErrorCode)track_locker);
        track_locker = WhoIs(TRACK_LOCKER);
    }


    //Create Courier
    TaskId ss_courier = Create(MyPriority(), &Courier);
    if(! isValidTaskId(ss_courier)){
        ERROR("Failed to create SS courier: %d", (ErrorCode)track_locker);
    }
    volatile char* msg_peek = 0;
    CourierInitMsg message;
    message.forward = MyTid();
    message.message_size = sizeof(SensorsRequest);
    message.name = SENSOR_SERVER;
    message.reply_size = 0;
    message.msg_peek = &msg_peek;

    DEBUG("ss_courier: %d", ss_courier);
    
    ErrorCode err = Send(ss_courier, &message, sizeof(message), 0, 0);
    if (err) {
        ERROR("Initializing SS courier came across an error: %d", err);
    }

    FOREVER {
        //Receive sensor data and give it to any task that wants it
        TaskId tid = INVALID_TID;
        SensorDataMsg sdm;
        SensorsReply sreply;
        sreply.error = NO_ERROR;
        sreply.time = -69;
        //ErrorCode err = Receive(&tid, &sdm, sizeof(SensorDataMsg));
        ErrorCode err = Send(ss_courier, 0, 0, &sdm, sizeof(SensorDataMsg));
        if (err) ERROR("Got error from recv in sensorsprovider");

        switch (sdm.type) {
            case DATA:
                {
                //Update Sensor Table
                int maxLen = sdm.len > prevLen ? sdm.len : prevLen;

                //DEBUG("sdm:%d, prev:%d", sdm.len, prevLen);
                for (i = 0; i < maxLen; i++) {
                    //reset prev sensor data
                    if (i < prevLen) {
                        //Resetting prev sensor data:
                        //bwprintf(1, "i[%d]R%d", i, prevActiveSensors[i]);
                        if (sensorTable[prevActiveSensors[i]] != SENSOR_BROKEN) {
                            sensorTable[prevActiveSensors[i]] = SENSOR_INACTIVE;
                        }
                    }

                    //set new sensor data
                    if (i < sdm.len) {
                        //DEBUG("i[%d]S%d", i, sdm.activeSensors[i]);
                        if (sensorTable[sdm.activeSensors[i]] != SENSOR_BROKEN) {
                            SensorsReply reply;
                            reply.error = NO_ERROR;

                            sensorTable[sdm.activeSensors[i]] = SENSOR_ACTIVE;

                            TaskId trainTid = waitingTrains[sdm.activeSensors[i]];
                            DEBUG("Checking waitingTrains[%s] and is: %d", trackNode_IndexToName(sdm.activeSensors[i]), trainTid);
                            if(trainTid != INVALID_TID) {
                                DEBUG("Sensor %s triggered. Train %d is waiting on it", trackNode_IndexToName(sdm.activeSensors[i]), trainTid);
                                ASQueueItem* asqi = 0;
                                int accumulatedDist = 0;
                                int alternatePathTaken = FALSE;
                                //Check if we are hitting the alternate turnout sensor
                                if (alternateTurnoutSensors[trainTid] == sdm.activeSensors[i]) {
                                    //Train has went astray
                                    DEBUG("Train %d went on wrong switch edge", trainTid);
                                    
                                    //If the switch malfunctioned and the alternate path was taken, find the total distance
                                    Path alternatePath;
                                    getPath(trainPrevSensor[trainTid], sdm.activeSensors[i], &alternatePath, FALSE);
                                    accumulatedDist = alternatePath.distance;
                                    alternatePathTaken = TRUE;

                                    //Update Track to set switch edge as broken
                                }
                                
                                //Pop until we see the sdm.activeSensors[i] in the queue
                                //We shud ALWAYS see the sensor hit in this queue, esp since now we are reserving and tracking both turnout edges
                                while(!queueEmpty_ASQ(&(sensorQueues[trainTid])) && alternatePathTaken == FALSE) {
                                    asqi = queuePop_ASQ(&(sensorQueues[trainTid]));
                                    DEBUG("Popping sensor %s off of queue for train ", trackNode_IndexToName(asqi->sensorNum), trainTid);

                                    waitingTrains[asqi->sensorNum] = INVALID_TID;
                                    accumulatedDist += asqi->distance;

                                    if (asqi->sensorNum == sdm.activeSensors[i]) {
                                        break;
                                    }
                                }

                                //Since accumulatedDist is wrong now after making changes for ATTRIBUTE_PATH to require Path, just recompute the distance
                                Path actualPath;
                                int pathStart = trainPrevSensor[trainTid] != INVALID_SENSOR ? trainPrevSensor[trainTid] : trainStartNodes[trainTid];
                                DEBUG("COMPUTING PATH FROM START: %d", pathStart);
                                getPath(pathStart, sdm.activeSensors[i], &actualPath, FALSE);
                                if (accumulatedDist != actualPath.distance) {
                                    ERROR("accumulatedDist %d != %d (actualPath Distnace)", accumulatedDist, actualPath.distance);
                                }
                                accumulatedDist = actualPath.distance;

                                DEBUG("SENDING REPLY TO TRAIN %d for sensor %d with dist %d", trainTid, sdm.activeSensors[i], accumulatedDist);
                                TrainMsg tm;
                                tm.type = T_SENSOR;
                                tm.sensor_info.node = sdm.activeSensors[i];
                                tm.sensor_info.distance = accumulatedDist;
                                tm.sensor_info.time = Time();
                                DEBUG("Sending to train courier: %d", courierTids[trainTid]);
                                ErrorCode error = Send(courierTids[trainTid], &tm, sizeof(TrainMsg), 0, 0);
                                if (error) {
                                    ERROR("Error sending sensor trigger to courier for train %d: %d", trainNums[trainTid], error);
                                }

                                //Now the courier has the msg and the train can get the msg whenever it wants

                                //Update state values
                                trainPrevSensor[trainTid] = sdm.activeSensors[i];
                                waitingTrains[sdm.activeSensors[i]] = INVALID_TID;
                                if (!queueEmpty_ASQ(&(sensorQueues[trainTid]))) {
                                    awaitingSensors[trainTid] = queuePeek_ASQ(&(sensorQueues[trainTid]))->sensorNum;
                                } else {
                                    awaitingSensors[trainTid] = INVALID_SENSOR;
                                }
                            }//END if(trainTid != INVALID_TID)

                            //Store the active sensors into previous sensors
                            prevActiveSensors[i] = sdm.activeSensors[i];
                        }
                    }
                }

                prevLen = sdm.len;
            
                // Awake all waiting any
                int i = 0;
                SensorsReply reply;
                reply.error = NO_ERROR;
                reply.sensorNum = sdm.activeSensors[i];
                for(; i < waitingAny; i++) {
                    //It is possible now for this to be INVALID_TID if it was woken up
                    if (waitingAnySensor[i] != INVALID_TID) {
                        Reply(waitingAnySensor[i], &reply, sizeof(reply));
                    }
                }
                waitingAny = 0;

                }

                /*
                if (singleWaitingTrain != INVALID_TID) {
                    for(i = 0; i < MAX_SENSORS; i++) {
                        if (waitingTrains[i] == singleWaitingTrain) {
                            waitingTrains[i] = INVALID_TID;
                            break;
                        }
                    } 

                    SensorsReply reply;
                    reply.error = NO_ERROR;
                    reply.sensorNum = sdm.activeSensors[0];
                    Reply(singleWaitingTrain, &reply, sizeof(reply));
                    singleWaitingTrain = 0;
                }*/

                break;

            case TRAIN_INIT:
                {
                //Index the train and keep track of courier tid
                int l;
                int trainAlreadyIndexed = FALSE;
                for (l = 0; l < numTrainsServed; l++) {
                    if (trainIndex[l] == sdm.trainNum) {
                        trainAlreadyIndexed = TRUE;
                        break;
                    }
                }

                if (!trainAlreadyIndexed) {
                    trainIndex[numTrainsServed] = sdm.trainNum;
                    numTrainsServed++;
                    if (numTrainsServed >= NUM_TRAINS) {
                        ERROR("SENSOR SERVER SERVED MORE THAN %d TRAINS!: %d", NUM_TRAINS, numTrainsServed);
                    }
                }

                //Store its courier info
                courierTids[sdm.trainNum] = sdm.courierTid;
                DEBUG("Storing Courier TID: %d for train %d", courierTids[sdm.trainNum], sdm.trainNum);

                //store train number info (used for debug output)
                trainNums[sdm.trainNum] = sdm.trainNum;
                break;
                }
            case AWAIT:
                {

                //Take note that this train wants a message when sensor is triggered
                if (awaitingSensors[sdm.trainNum] == INVALID_SENSOR) {
                    ASQueueItem* asqi =  queuePeek_ASQ(&(sensorQueues[sdm.trainNum]));
                    awaitingSensors[sdm.trainNum] = asqi->sensorNum; 
                    startTime[sdm.trainNum] = Time();
                } else {
                    ERROR("Train already waiting on another sensor: %d", awaitingSensors[sdm.trainNum]);
                }
                
/*
                //First check if sensor is known as broken
                int isSensorBroken = FALSE;
                int l;
                for(l = 0; i < numBrokenSensors; l++) {
                    if (brokenSensors[l] == sdm.sensorNum) {
                        isSensorBroken = TRUE;  
                    }
                }

                //Make a train wait on a sensor
                if (waitingTrains[sdm.sensorNum] == INVALID_TID && !isSensorBroken) {
                    waitingTrains[sdm.sensorNum] = tid; 
                    if (awaitingSensors[tid] != INVALID_SENSOR) {
                        ERROR("waitingTrains[%d] is INVALID_TID but awaitingSensors[%d] is not INVALID_SENSOR",tid, awaitingSensors[tid]);
                        //Shouldn't happen, but just continue anyways cause yolo
                    }
                    awaitingSensors[tid] = sdm.sensorNum;
                    startTime[sdm.sensorNum] = Time();

                    //Check if the server has not already indexed this train
                    int l;
                    int trainAlreadyIndexed = FALSE;
                    for (l = 0; l < numTrainsServed; l++) {
                        if (trainIndex[l] == tid) {
                            trainAlreadyIndexed = TRUE;
                            break;
                        }
                    }

                    if (!trainAlreadyIndexed) {
                        trainIndex[numTrainsServed] = tid;
                        numTrainsServed++;
                        if (numTrainsServed >= NUM_TRAINS) {
                            ERROR("SENSOR SERVER SERVED MORE THAN %d TRAINS!: %d", NUM_TRAINS, numTrainsServed);
                        }
                    }

                    //ACtivate Sensor Watchdog
                    if (sdm.timeout > 0) {
                        SensorWatchDogInfo swdi;
                        swdi.timeout = sdm.timeout;
                        swdi.sensorNum = sdm.sensorNum;
                        ErrorCode err = Send(watchdog_tid, &swdi, sizeof(SensorWatchDogInfo), 0, 0);
                        if (err) ERROR("Error making watchdog do a delay");
                    }
                } else {
                    sreply.error = AWAIT_SENSOR_UNAVAILABLE;
                }

                */
                }
                break;

            case SENSOR_TIMEOUT:
                {
                //Wake up the train!!!
                TaskId trainTid = waitingTrains[sdm.sensorNum];
                if (trainTid != INVALID_TID) {
                    SensorsReply reply;
                    reply.error = AWAIT_SENSOR_TIMEOUT;
                    reply.time = 0;
                    Reply(trainTid, &reply, sizeof(SensorsReply));
                    waitingTrains[sdm.sensorNum] = INVALID_TID;
                    awaitingSensors[trainTid] = INVALID_SENSOR;
                    //singleWaitingTrain = INVALID_TID;
                }
                }
                break;

            case AWAIT_ALL:
                if(waitingAny == MAX_WAITING_ANY) {
                    sreply.error = AWAIT_SENSOR_UNAVAILABLE; 
                    break;
                } 

                waitingAnySensor[waitingAny++] = sdm.trainNum;

                if (trainIndex[numTrainsServed] == INVALID_TID) {
                    trainIndex[numTrainsServed] = sdm.trainNum;
                    numTrainsServed++;
                    if (numTrainsServed >= NUM_TRAINS) {
                        ERROR("SENSOR SERVER SERVED MORE THAN %d TRAINS!: %d", NUM_TRAINS, numTrainsServed);
                    }
                }
                break;

            case ATTRIBUTE:
                {
                    //Set some values
                    waitingTrains[sdm.sensorNum] = sdm.trainNum; 
                    if (awaitingSensors[sdm.trainNum] == INVALID_SENSOR) {
                        awaitingSensors[sdm.trainNum] = sdm.sensorNum;
                    }
                    queuePush_ASQ(&(sensorQueues[sdm.trainNum]), sdm.sensorNum, sdm.distance, sdm.trainNum);
                    DEBUG("ATTRIBUTED SENSOR %s to train %d", trackNode_IndexToName(sdm.sensorNum), sdm.trainNum); 
                    break;
                }
            case ATTRIBUTE_ALT_TURNOUT:
                    break;
            case ATTRIBUTE_PATH:
                {
                    //Keep track of destination of the path
                    trainDestinations[sdm.trainNum] = sdm.destination;
                    trainStartNodes[sdm.trainNum] = sdm.startNode;

                    //If there is an alternate path, determine the sensor after switch and remember it
                    if (sdm.altPath != 0) {
                        //An alternate path is specified
                        Path* altPath = sdm.altPath;
                        int l = 0;
                        for (l = 0; l < altPath->num_nodes - 1; l++) {
                            if (track[altPath->nodes[l]].type == NODE_BRANCH) {
                                //We have a switch node, now find the next sensor
                                int k; 
                                for(k = l; k < altPath->num_nodes - 1; k++) {
                                    if (track[altPath->nodes[k]].type == NODE_SENSOR) {
                                        alternateTurnoutSensors[sdm.trainNum] = altPath->nodes[k];
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            case TRAIN_RESET:
                {
                    //Flush out any state kept track by sensor server for this train 
                    while(!queueEmpty_ASQ(&(sensorQueues[sdm.trainNum]))) {
                        queuePop_ASQ(&(sensorQueues[sdm.trainNum]));
                    }
                    waitingTrains[awaitingSensors[sdm.trainNum]] = INVALID_TID;
                    awaitingSensors[sdm.trainNum] = INVALID_SENSOR;
                    alternateTurnoutSensors[sdm.trainNum] = INVALID_SENSOR;
                    trainPrevSensor[sdm.trainNum] = INVALID_SENSOR;
                    trainDestinations[sdm.trainNum] = INVALID_SENSOR;
                    trainStartNodes[sdm.trainNum] = INVALID_SENSOR;
                }
                break;
            case WAKEUP_TRAIN:
                {
                //Need to wake up a train
                //KDEBUG("WOKE UP A TRAIN: %d waiting on %d", tid, waitingTrains);
                int isAwaitingSensor = FALSE;
              
                if (awaitingSensors[sdm.trainTid] == INVALID_SENSOR) {
                    //A train is not waiting on a particular sensor,however maybe waiting for any sensor.
                    int l;
                    for(l = 0; l < MAX_WAITING_ANY; l++) {
                        if (waitingAnySensor[l] == sdm.trainNum) {
                            isAwaitingSensor = TRUE;
                            break;
                        }
                    }
                } else {
                    //Make sure the the wake up  message gets the awaited sensor stuff correct
                    int s = awaitingSensors[sdm.trainTid];
                    isAwaitingSensor = TRUE;
                    awaitingSensors[sdm.trainTid] = INVALID_SENSOR;
                    waitingTrains[s] = INVALID_TID;
                }

                if (isAwaitingSensor) {
                    //Wake up the train
                    ASQueueItem* asqi = queuePop_ASQ(&(sensorQueues[sdm.trainTid]));
                    SensorsReply reply;
                    reply.error = FORCE_WOKE_UP;
                    reply.sensorNum = asqi->sensorNum;
                    reply.real_time = Time();
                    reply.distance = asqi->distance;
                    Reply(sdm.trainTid, &reply, sizeof(SensorsReply));

                    //TODO consider if prevSensors should be updated to INVALID_SENSOR
                } else {
                    //tell the sender that it is wrong
                    sreply.error = NO_TRAIN_TO_WAKE_UP;
                }

                }
                break;

            default:
                sreply.error = ERR_BAD_REQUEST_TYPE;
                break;
        }
        if (sdm.type == DATA) {
            Receive(&tid, &sdm, sizeof(SensorDataMsg));
            Reply(tid, 0, 0);
        }
    }
}
