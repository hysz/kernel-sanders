#include "await_sensor_api.h"
#include "kapi.h"
#include "nameserver_api.h"

ErrorCode AwaitSensor(int sensorNum, int* time, int *real_time, int* actualSensorNum, float* distance) {
    return TimedAwaitSensor(sensorNum, time, real_time, 0, actualSensorNum, distance);
}

ErrorCode TimedAwaitSensor(int sensorNum, int* time, int *real_time, int timeout, int* actualSensorNum, float* distance) {
    SensorsRequest request;
    request.type = AWAIT;
    request.sensorNum = sensorNum;
    request.timeout = timeout;

    TaskId sensor_server_tid = WhoIs(SENSOR_SERVER);

    SensorsReply reply;
    Send(sensor_server_tid, &request, sizeof(SensorsRequest), &reply, sizeof(SensorsReply));   
    *time = reply.time;
    *real_time = reply.real_time;
    *actualSensorNum = reply.sensorNum;
    *distance = reply.distance;
    return reply.error;
}

ErrorCode AwaitAnySensor(int* sensorNum)
{
    SensorsRequest request;
    request.type = AWAIT_ALL;

    TaskId sensor_server_tid = WhoIs(SENSOR_SERVER);

    SensorsReply reply;
    Send(sensor_server_tid, &request, sizeof(SensorsRequest), &reply, sizeof(SensorsReply));   
    *sensorNum = reply.sensorNum;
    return reply.error;
}

ErrorCode AttributeSensor(int sensorNum, float distance, TaskId trainTid, int trainNum) {
    SensorsRequest request;
    request.type = ATTRIBUTE;
    request.sensorNum = sensorNum;
    request.distance = distance;
    request.trainTid = trainTid;
    request.trainNum = trainNum;

    TaskId sensor_server_tid = WhoIs(SENSOR_SERVER);

    SensorsReply reply;
    Send(sensor_server_tid, &request, sizeof(SensorsRequest), &reply, sizeof(SensorsReply));
    return reply.error;
}

ErrorCode WakeUpAwaiter2(TaskId awaiter, int sensor) {

    TaskId sensor_server_tid = WhoIs(SENSOR_SERVER);

    WakeUpTrainMsg wutm;
    wutm.type = WAKEUP_TRAIN;
    wutm.trainTid = awaiter;
    wutm.sensorNum = sensor;

    SensorsReply reply;
    Send(sensor_server_tid, &wutm, sizeof(WakeUpTrainMsg), &reply, sizeof(SensorsReply));
    return reply.error;
}

ErrorCode WakeUpAwaiter(TaskId awaiter) {
    return WakeUpAwaiter2(awaiter, 29);    
}
