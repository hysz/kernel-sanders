#pragma once

#include "common.h"
#include "errno.h"
#include "sensorserver.h"

ErrorCode AwaitSensor(int sensorNum, int* time, int* real_time, int* actualSensorNum, float* distance);

ErrorCode TimedAwaitSensor(int sensorNum, int* time, int* real_time, int timeout, int* actualSensorNum, float* distance);

ErrorCode AwaitAnySensor(int *sensorNum);

ErrorCode AttributeSensor(int sensorNum, float distance, TaskId trainTid, int trainNum);

ErrorCode WakeUpAwaiter(TaskId awaiter);

ErrorCode WakeUpAwaiter2(TaskId awaiter, int sensor);
