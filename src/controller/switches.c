#include "task.h"
#include "kapi.h"
#include "debug.h"
#include "common.h"
#include "io.h"
#include "errno.h"
#include "buffer.h"
#include "names.h"
#include "nameserver_api.h"
#include "clockserver_api.h"
#include "command.h"
#include "courier.h"

#define SOLENOID_OFF_CMD 32
#define SWITCH_STRAIGHT 33
#define SWITCH_CURVED 34

// 19,20,21,22 => 151, 152, 153, 154
#define MAX_SWITCH_ID 22 
#define NUM_SWITCHES 23

static int switchIndexToId(int index)
{
    if(index <= 18) return index;
    else {
        return index - 18 + 152;
    }   
}

static int switchIdToIndex(int id) 
{
    if(id <= 18) return id; 
    else {
        return id - 152 + 18; 
    }   
}

static ErrorCode printSwitch(TaskId channel, int i, char direction)
{
    Buffer b;
    bufferReset(PRINT_SWITCH, &b);
    
    bufferPutStr(&b, "\033[");
    bufferPutInt(&b, 4 + ((i-1)%5)); 
    bufferPut(&b, ';');
    bufferPutInt(&b, 25 + (8 * ((i-1)/5)));
    bufferPutStr(&b, "H");

    if(switchIndexToId(i) < 10) {
        bufferPutInt(&b, 0); 
    }   
    if(switchIndexToId(i) <= 18) {
        bufferPutInt(&b, 0); 
    }   
    
    bufferPutInt(&b, switchIndexToId(i));
    bufferPutStr(&b, ": ");

    if(direction == 's' || direction == 'S') bufferPut(&b, 'S');
    else if(direction == 'c' || direction == 'C') bufferPut(&b, 'C');
    else bufferPut(&b, direction);

    return putB(channel, &b);
}

static ErrorCode updateSwitchNoSolenoid(TaskId channel, int sw, char direction, char switches[NUM_SWITCHES])
{
    Buffer b;
    bufferReset(UPDATE_SWITCH, &b);

    if(direction == 's' || direction == 'S') {
        bufferPut(&b, SWITCH_STRAIGHT);
    } else {
        bufferPut(&b, SWITCH_CURVED);
    }   

    bufferPut(&b, sw);
    switches[switchIdToIndex(sw)] = direction;

    return putB(channel, &b);
}

static ErrorCode updateSwitch(TaskId channel, int sw, char direction, char switches[NUM_SWITCHES])
{
    Buffer b;
    bufferReset(UPDATE_SWITCH, &b);

    if(direction == 's' || direction == 'S') {
        bufferPut(&b, SWITCH_STRAIGHT);
    } else {
        bufferPut(&b, SWITCH_CURVED);
    }   

    bufferPut(&b, sw);
    bufferPut(&b, SOLENOID_OFF_CMD);

    switches[switchIdToIndex(sw)] = direction;

    return putB(channel, &b);
}

void Switches()
{
    DEBUG("Starting Switches");

    // Create a proxy to the switches server
    TaskId my_courier = Create(TRAIN_HELPER_HI_PRIORITY, &Courier);
    if(! isValidTaskId(my_courier) ) {
        ERROR("TrainInstructor Invalid TID=%d", (ErrorCode)my_courier);
        Exit();
    } 

    // Send Initialization Message
    {
        CourierInitMsg message;
        message.forward = MyTid();
        message.message_size = sizeof(Command);
        message.name = SWITCHES_COURIER;
        message.reply_size = sizeof(ErrorCode);
        message.msg_peek = (volatile char**)0;

        ErrorCode error = Send(my_courier, &message, sizeof(message), 0, 0);
        if(error) {
            ERROR("TrainInstructor Init Failed %d", error);
            Exit();
        }
    }

    RegisterAs(SWITCHES_SERVER);

    TaskId train_out_channel = WhoIs(TRAIN_PUT_SERVER);
    if(! isValidTaskId(train_out_channel) ) {
        ERROR("Switches failed to lookup train out channel: %d", train_out_channel);
        Exit();
    }

    TaskId term_out_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(term_out_channel) ) {
        ERROR("Switches failed to lookup term out channel: %d", term_out_channel);
        Exit();
    }

    // switches
    char switches[NUM_SWITCHES];
    int i;
    for(i = 0; i < NUM_SWITCHES; ++i) {
        switches[i] = 'C';
    }

    // Make a figure Loop
    switches[switchIdToIndex(155)] = 'S';
    switches[switchIdToIndex(156)] = 'C';
    switches[switchIdToIndex(153)] = 'C';
    switches[switchIdToIndex(154)] = 'S';

    // Print Header
    putS(term_out_channel, "\033[3;25H-----------Switch Positions-----------");

    // Go Command - must be executed before any other train communication will go through
    for(i = 1; i <= MAX_SWITCH_ID; ++i) {
        ErrorCode error = NO_ERROR;
        if(i < MAX_SWITCH_ID) {
            updateSwitchNoSolenoid(train_out_channel, switchIndexToId(i), switches[i], switches);
        } else {
            updateSwitch(train_out_channel, switchIndexToId(i), switches[i], switches);
        }
        if(error) {
            ERROR("Failed to initialize switch %d: %d", switchIndexToId(i), error);
            //continue;
        }
        printSwitch(term_out_channel, i, switches[i]); 
    }

    FOREVER {
        Command cmd;
        Send(my_courier, 0, 0, &cmd, sizeof(cmd));

        ErrorCode error = NO_ERROR;
        switch(cmd.type) {
        case SWITCH:
            error = updateSwitch(train_out_channel, cmd.args[0], (char)cmd.args[1], switches); 
            if(! error) {
                error = printSwitch(term_out_channel, switchIdToIndex(cmd.args[0]), (char)cmd.args[1]); 
            }
            break;

        case P_SWITCH:
            error = printSwitch(term_out_channel, switchIdToIndex(cmd.args[0]), (char)cmd.args[1]); 
            break;

        default:
            error = UNRECOGNIZED_SPEED_COMMAND;
            break;
        }

        if(error) {
            ERROR("Failed to run switch command: %d", error); 
        }
    } 

    DEBUG("Finished Switches()");
    Exit();
}
