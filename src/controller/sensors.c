#include "common.h"
#include "sensor_queue.h"
#include "io.h"
#include "names.h"
#include "nameserver_api.h"
#include "common.h"
#include "debug.h"
#include "kapi.h"
#include "buffer.h"
#include "sensorserver.h"
#include "sensors.h"
#include "command.h"

static int recordS88State(Queue *q, int s88_id, int sid_start, char *old_state, char new_state, int* activeSensors, int* activeLen, int* brokenSensors, int numBrokenSensors)
{
    if(*old_state == new_state) return FALSE; // nothing to change

    int j = 7;
    for(j = 7; j >= 0; --j) {
        if( (new_state & (0x1 << j)) && (*old_state & (0x1 << j)) != (new_state & (0x1 << j)) ) { 
            int sensorId = (s88_id - 1) * 16 + sid_start + 7 - j - 1;

            //Check if sensor is set to broken by input
            int k;
            int isBroken = 0;
            for(k = 0; k < numBrokenSensors; k++) {
                if (sensorId == brokenSensors[k]) {
                    isBroken = 1;
                    break;
                }
            } 

            if (isBroken) {
                continue;
            }

            //Add Sensor trigger to output queue and active sensors
            queuePush(q, s88_id, sid_start + (7 - j)); 

            if (*activeLen > 6) ERROR("MORE THAN 6 SENSORS TRIGGERED");

            activeSensors[*activeLen] = (s88_id - 1) * 16 + sid_start + 7 - j - 1;
            (*activeLen) += 1;
        }   
    }   

    *old_state = new_state;

    return TRUE;
}

static void requestS88Status(TaskId train_out_channel, int max_s88_id)
{
    ErrorCode error;
    Buffer b;
    bufferReset(ANONYMOUS, &b);
    bufferPut(&b, 128 + max_s88_id);

    if( (error = putB_Blocking(train_out_channel, &b)) ) {
        ERROR("Failed to request s88 status");
    }
}

static void printSensors(TaskId output_channel, Queue *sensor_q)
{
    Buffer b;
    bufferReset(PRINT_SENSORS, &b);
        
    queuePrint(sensor_q, &b);
    putB(output_channel, &b);
}

void Sensors()
{
    DEBUG("Sensors Starting");

    const int min_s88_id = 1;
    const int max_s88_id = 5;

    // Initialize Sensors
    char sensors[max_s88_id + 1][2];
    int i;
    for(i = 1; i < max_s88_id + 1; ++i) {
        int j = 0;
        for(j = 0; j < 2; ++j) {
            sensors[i][j] = '\0'; // All 0's
        }   
    }   

    int activeSensors[NUM_TRAINS];
    int activeLen = 0;

    int brokenSensors[MAX_BROKEN_SENSORS];
    int numBrokenSensors = 0;

    // For tracking which s88 id we're observing
    int s88_id = 1;
    int s88_received[2];
    s88_received[0] = FALSE;
    s88_received[1] = FALSE;

    Queue sensor_q;
    queueReset(&sensor_q);

    TaskId input_channel = WhoIs(TRAIN_GET_SERVER);
    if(! isValidTaskId(input_channel) ) {
        ERROR("Sensors failed to lookup input channel: %d", input_channel);
        Exit();
    }

    TaskId train_out_channel = WhoIs(TRAIN_PUT_SERVER);
    if(! isValidTaskId(train_out_channel) ) {
        ERROR("Sensors failed to lookup output channel: %d", train_out_channel);
        Exit();
    }

    TaskId term_output_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(term_output_channel) ) {
        ERROR("Sensors failed to lookup output channel: %d", term_output_channel);
        Exit();
    }

    // Print header
    putS(term_output_channel, "\033[3;0H--Recent Sensors--");

    // Go Command - must be executed before any other train communication will go through
    putc(train_out_channel, 0x60);
    putc(train_out_channel, 192);

    int first_iteration = TRUE;
    requestS88Status(train_out_channel, max_s88_id);

    TaskId dataReceiver = INVALID_TID;

    //See if anyone wants sensor data
    dataReceiver = WhoIs(SENSOR_SERVER);

    FOREVER {

        if(s88_received[0] && s88_received[1]) {
            s88_received[0] = FALSE;
            s88_received[1] = FALSE;

            if(s88_id == min_s88_id) {
                if(! first_iteration) {
                    printSensors(term_output_channel, &sensor_q);

                    //Determine if data needs to be sent to anyone
                    if(activeLen > 0 && ! isValidTaskId(dataReceiver)) {
                        dataReceiver = WhoIs(SENSOR_SERVER);
                        if(! isValidTaskId(dataReceiver)) {
                            ERROR("CANNOT SEND TO SENSOR SERVER");
                        }
                    }

                    //ERROR("Send?: %d %d", dataReceiver, activeLen);
                    if (isValidTaskId(dataReceiver) && activeLen > 0) {
                        SensorDataMsg sdm;
                        sdm.activeSensors = activeSensors;
                        sdm.len = activeLen;
                        sdm.type = DATA;
                        Send(dataReceiver, &sdm, sizeof(SensorDataMsg), &sr, sizeof(SensorsReply));
                        //if (sr.error) ERROR("SensorReply receive error: %d", sr.error);

                        int i = 0;
                        for (i = 0; i < activeLen; i++) {
                            activeSensors[i] = 0;
                        }
                        activeLen = 0;
                    }

                    //Check if we have any broken sensor requests
                    TaskId tid = INVALID_TID;
                    Command cmd;
                    ErrorCode err = Peek(&tid, &cmd, sizeof(Command));
                    if (!err) {
                        //Theres a message to receive
                        Receive(&tid, &cmd, sizeof(Command));
                        if (cmd.type == BREAK_SENSOR) {
                            brokenSensors[numBrokenSensors] = cmd.args[0]; 
                            numBrokenSensors++;
                        } else {
                            ERROR("Sensors: Unexpected Command: %d, From Tid: %d", cmd.type, tid);
                        }

                        ErrorCode error = NO_ERROR;
                        Reply(tid, &error, sizeof(error));
                    } else if (err !=  NO_MSG_TO_RECEIVE) {
                        ERROR("Sensor Notifier got error from peek: %d", err);
                    }
                }

                first_iteration = FALSE;
                requestS88Status(train_out_channel, max_s88_id);
            }
        } else {
            char c = getc(input_channel);

            if(! s88_received[0]) {
                s88_received[0] = TRUE;
                if(! first_iteration) recordS88State(&sensor_q, s88_id, 1, &sensors[s88_id][0], c, activeSensors, &activeLen, brokenSensors, numBrokenSensors);
            } else {
                s88_received[1] = TRUE;
                if(! first_iteration) recordS88State(&sensor_q, s88_id, 9, &sensors[s88_id][1], c, activeSensors, &activeLen, brokenSensors, numBrokenSensors);

                s88_id = (s88_id + 1) % (max_s88_id + 1); 
                if(s88_id == 0) s88_id = min_s88_id;
            }   
        }  
    }

    DEBUG("Sensors Finished");
    Exit();
}

