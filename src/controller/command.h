#pragma once

typedef enum {NONE, GO, STOP, SPEED, REVERSE, SWITCH, P_SWITCH, CHECK_PATH, INIT, MOVE, ACCELERATION, DECCELERATION, VELOCITY, STOPPING_DISTANCE_CALIBRATION, BREAK_SENSOR, STOPPING_TIME_CALIBRATION} CommandType;
struct Command {
    char raw_type[2];
    CommandType type;
    int args[2];
    int train_index;
};
typedef struct Command Command;
