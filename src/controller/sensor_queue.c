#include "sensor_queue.h"
#include "buffer.h"

void queueReset(Queue *q)
{
    q->next = 0;
    q->len = 0;
}

void queuePush(Queue* q, int d1, int d2) 
{
    q->data[q->next][0] = d1; 
    q->data[q->next][1] = d2; 

    q->next = (q->next + 1) % QUEUE_SIZE;
    if(q->len < QUEUE_SIZE) q->len++;
}

void queuePrint(Queue* q, Buffer *b) 
{
    int j = 4;  // line number to print on
    int i = 0;

    for(i = 0; i < q->len; ++i) {
        // Add text
        int k = (q->next - 1) - i;
        if(k < 0) k += QUEUE_SIZE;

    bufferPutStr(b, "\033[");
    bufferPutInt(b, j); 
    bufferPutStr(b, ";7H");

    // Clear line up to where we want to add

        bufferPut(b,    (char)(64 + q->data[k][0])); // A,B,C,...
        bufferPut(b, ' ');
        if(q->data[k][1] < 10) {
            bufferPut(b,    '0');
        }   
        bufferPutInt(b, q->data[k][1]); // 1..16
        bufferPut(b,    ' ');

        ++j;
    }   
}
