#pragma once

#define QUEUE_SIZE 5

#include "buffer.h"

struct Queue {
    int data[QUEUE_SIZE][2];
    int next; 
    int len;
};
typedef struct Queue Queue;

void queueReset(Queue *q);
void queuePush(Queue* q, int d1, int d2);
void queuePrint(Queue* q, Buffer *b);
