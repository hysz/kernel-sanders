#include "task.h"
#include "kapi.h"
#include "debug.h"
#include "common.h"
#include "io.h"
#include "errno.h"
#include "buffer.h"
#include "names.h"
#include "nameserver_api.h"
#include "clockserver_api.h"
#include "command.h"

enum {MAX_TRAIN_ID = 80};

static ErrorCode setSpeed(TaskId channel, int train, int speed, int speeds[MAX_TRAIN_ID])
{
    Buffer b;
    bufferReset(SET_SPEED, &b);
    
    bufferPut(&b, speed);
    bufferPut(&b, train);

    speeds[train] = speed;
    return putB(channel, &b);
}

static ErrorCode reverse(TaskId channel, int train, int speeds[MAX_TRAIN_ID])
{
    // Stop Train
    Buffer b;
    bufferReset(STOP_REVERSE, &b);
    
    bufferPut(&b, 0); 
    bufferPut(&b, train);
   
   ErrorCode error = putB(channel, &b); 
   if(error) {
        ERROR("Failed to stop train before reversing");
    }

    // Sleep for some period of time
    Delay(3 * (SECOND_DELAY));

    bufferReset(DO_REVERSE, &b);

    // Reverse Train
    bufferPut(&b, 15);
    bufferPut(&b, train);

    // Set Train Speed 
    bufferPut(&b, speeds[train]);
    bufferPut(&b, train);

    return putB(channel, &b);
}

static ErrorCode printTrain(TaskId channel, int train, int speed)
{
    Buffer b;
    bufferReset(PRINT_TRAIN, &b);

    int line = 4;
    switch(train) {
    case 45:
        line = 4;
        break;

    case 48:
        line = 5;
        break;

    case 54:
        line = 6;
        break;

    case 56:
        line = 7;
        break;

    case 58:
        line = 8;
        break;

    default:
        ERROR("printTrain: unrecognized train number, %d", train); 
        break;
    }
    
    bufferPutStr(&b, "\033[");
    bufferPutInt(&b, line);
    bufferPutStr(&b, ";26H");
    
    bufferPutInt(&b, train);
    bufferPutStr(&b, ": ");

    if(speed < 10) bufferPut(&b, '0');
    bufferPutInt(&b, speed);

    return putB(channel, &b);
}

void Trains()
{
    DEBUG("Starting Trains()");

    TaskId train_out_channel = WhoIs(TRAIN_PUT_SERVER);
    if(! isValidTaskId(train_out_channel) ) {
        ERROR("Trains failed to lookup train out channel: %d", train_out_channel);
        Exit();
    }

    TaskId term_out_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(term_out_channel) ) {
        ERROR("Trains failed to lookup term out channel: %d", term_out_channel);
        Exit();
    }

    // Array of train speeds
    int speeds[MAX_TRAIN_ID];
    int i = 0;
    for(i = 0; i < MAX_TRAIN_ID; ++i) speeds[i] = 0;

    // Go Command - must be executed before any other train communication will go through
    putc(train_out_channel, 0x60);

    // Stop all trains
    setSpeed(train_out_channel, 12, 0, speeds); 
    setSpeed(train_out_channel, 24, 0, speeds); 
    setSpeed(train_out_channel, 45, 0, speeds); 
    setSpeed(train_out_channel, 48, 0, speeds); 
    setSpeed(train_out_channel, 54, 0, speeds); 
    setSpeed(train_out_channel, 56, 0, speeds); 
    setSpeed(train_out_channel, 58, 0, speeds); 
    setSpeed(train_out_channel, 62, 0, speeds); 

    // Print Header
    putS(term_out_channel, "\033[3;24H--Trains--");
    printTrain(term_out_channel, 45, 0);
    printTrain(term_out_channel, 48, 0);
    printTrain(term_out_channel, 54, 0);
    printTrain(term_out_channel, 56, 0);
    printTrain(term_out_channel, 58, 0);


    FOREVER {
        TaskId tid;
        Command cmd;
        Receive(&tid, &cmd, sizeof(cmd));

        ErrorCode error = NO_ERROR;
        Reply(tid, &error, sizeof(error));
        switch(cmd.type) {
        case SPEED:
            // ERROR("Setting Speed - %dx%d", cmd.args[0], cmd.args[1]);
            error = setSpeed(train_out_channel, cmd.args[0], cmd.args[1], speeds); 
            if(! error) {
                error = printTrain(term_out_channel, cmd.args[0], cmd.args[1]);
            }
            break;

        case REVERSE:
            error = reverse(train_out_channel, cmd.args[0], speeds); 
            break;

        case GO: 
            error = putc(train_out_channel, 0x60);
            break;

        case STOP:
            error = putc(train_out_channel, 0x61);
            break;

        default:
            error = UNRECOGNIZED_SPEED_COMMAND;
            break;
        }

        if(error) {
            ERROR("Failed to run train command: %d", error); 
        }
    } 

    DEBUG("Finished Trains()");
    Exit();
}
