#include "controller.h"
#include "task.h"
#include "io.h"
#include "kapi.h"
#include "errno.h"
#include "buffer.h"
#include "debug.h"
#include "names.h"
#include "nameserver_api.h"
#include "common.h"
#include "command.h"
#include "clockserver_api.h"
#include "track_dict.h"
#include "path_finder.h"
#include "path_finder_api.h"
#include "sensorserver.h"
#include "await_sensor_api.h"
#include "bwio.h"
#include "courier.h"
#include "track_locker.h"

enum {MAX_TRAIN_ID = 80};

extern void TimeKeeper();
extern void Sensors();
extern void Switches();
// extern void Trains();
extern void Train();
extern void PathFinder();

static void printResult(TaskId output_channel, Buffer *result, ErrorCode exec_error)
{
    if(exec_error) {
        bufferPutStr(result, " -- Execution Failed: ");
        bufferPutInt(result, -1 * exec_error);
    }

    // Erase to end of line after adding current text
    bufferPutStr(result, "\033[K");
    putB(output_channel, result);
}

static ErrorCode parseCommand(Command *cmd, Buffer *cmd_buffer, Buffer *result)
{
    bufferPutStr(result, "\033[31;0H");

    char raw_cmd[4];
    raw_cmd[0] = ' ';
    raw_cmd[1] = ' ';
    raw_cmd[2] = ' ';
    raw_cmd[3] = ' ';
    bufferGetCmd(cmd_buffer, raw_cmd);

    if(bufferEmpty(cmd_buffer)) {
        ERROR("\r\nparseCommand: About to pop empty buffer");
    }

    if(! isWhitespace(bufferPop(cmd_buffer)) ) { 
        bufferPutStr(result, "Unrecognized Command");
        return PARSE_ERROR;
    }   

/* The go and stop commands seem to break sensors
    if( raw_cmd[0] == 'g') {
        cmd->type = GO;
        bufferPutStr(result, "Go Train, Go!");
    } else if(raw_cmd[0] == 's' && raw_cmd[1] == 't') { 
        cmd->type = STOP;
        bufferPutStr(result, "Stopping Train!");
    } else */ 
    if( raw_cmd[0] == 't') {
        cmd->type = SPEED;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(bufferPop(cmd_buffer) != ' ' || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int speed = bufferGetInt(cmd_buffer, 2); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || speed < 0 || speed > 14) {
            bufferPutStr(result, "Bad Speed Number. Use Range [1..14]. Got ");
            bufferPutInt(result, speed);
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;
        cmd->args[1] = speed;

        bufferPutStr(result, "Setting Speed: "); 
        bufferPutInt(result, train);
        bufferPutStr(result, "x"); 
        bufferPutInt(result, speed);
    } else if(raw_cmd[0] == 'r') {
        cmd->type = REVERSE;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number");
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;

        // Done Parsing, now reverse train!
        bufferPutStr(result, "Reversing Train: "); 
        bufferPutInt(result, train);
    } else if(raw_cmd[0] == 's' && raw_cmd[1] == 'w') {
        cmd->type = SWITCH;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int sw = bufferGetInt(cmd_buffer, 3); // 1..256
        if(bufferPop(cmd_buffer) != ' ' || sw < 1 || sw > 256) {
            bufferPutStr(result, "Bad Switch Number. Must be in range: [1..256]");
            return PARSE_ERROR;
        }   

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        char direction = bufferPop(cmd_buffer);
        if(! isWhitespace(bufferPop(cmd_buffer)) || ( direction != 's' && direction != 'S' && direction != 'c' && direction != 'C')) {
            bufferPutStr(result, "Bad Switch Direction. Must be 'S' or 'C'.");
            return PARSE_ERROR;
        }   

        cmd->args[0] = sw;
        cmd->args[1] = direction;
        
        // Done Parsing, now change switch direction!
        bufferPutStr(result, "Setting Switch: "); 
        bufferPutInt(result, sw);
        bufferPutStr(result, "x"); 
        bufferPut(result, direction);
    } else if(raw_cmd[0] == 'c' && raw_cmd[1] == 'p') {
        cmd->type = CHECK_PATH;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        // Get Source
        char source_str[6];
        int i = 0;
        while( ! isWhitespace( (source_str[i] = bufferPop(cmd_buffer)) ) ) {
            i++; 
            if(i >= 6) {
                bufferPutStr(result, "Bad source. Name too long.");
                return PARSE_ERROR;
            }
        }
        source_str[i] = '\0';
        cmd->args[0] = trackNode_NameToIndex(source_str);

        // Get Destination
        char dest_str[6];
        i = 0;
        while( ! isWhitespace( (dest_str[i] = bufferPop(cmd_buffer)) ) ) {
            i++; 
            if(i >= 6) {
                bufferPutStr(result, "Bad dest. Name too long.");
                return PARSE_ERROR;
            }
        }
        dest_str[i] = '\0';
        cmd->args[1] = trackNode_NameToIndex(dest_str);
        
        // Done Parsing, update result
        bufferPutStr(result, "Finding Path: "); 
        bufferPutStr(result, source_str);
        bufferPut(result, '(');
        bufferPutInt(result, cmd->args[0]);
        bufferPut(result, ')');
        bufferPutStr(result, " to "); 
        bufferPutStr(result, dest_str);
        bufferPut(result, '(');
        bufferPutInt(result, cmd->args[1]);
        bufferPut(result, ')');
    } else if( raw_cmd[0] == 'p') {
        cmd->type = MOVE;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   
        cmd->args[0] = train;

        if(isWhitespace( bufferPeek(cmd_buffer) ) ) {
            bufferPop(cmd_buffer);
        }

        cmd->args[1] = -1;

        bufferPutStr(result, "Playing with Train "); 
        bufferPutInt(result, train);
    } else if( raw_cmd[0] == 'm') {
        cmd->type = MOVE;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(bufferPop(cmd_buffer) != ' ' || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   
        cmd->args[0] = train;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        } else if(isWhitespace( bufferPeek(cmd_buffer) ) ) {
            bufferPop(cmd_buffer);
        }

        // Get Destination
        char dest_str[6];
        int i = 0;
        while( ! isWhitespace( (dest_str[i] = bufferPop(cmd_buffer)) ) ) {
            i++; 
            if(i >= 6) {
                bufferPutStr(result, "Bad dest. Name too long.");
                return PARSE_ERROR;
            }
        }
        dest_str[i] = '\0';
        cmd->args[1] = trackNode_NameToIndex(dest_str);

        bufferPutStr(result, "Moving Train "); 
        bufferPutInt(result, train);
        bufferPutStr(result, " To "); 
        bufferPutStr(result, dest_str);
    } else if(raw_cmd[0] == 'i') {
        cmd->type = INIT;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number");
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;

        if(isWhitespace( bufferPeek(cmd_buffer) ) ) {
            bufferPop(cmd_buffer);
        } 

        if(bufferEmpty(cmd_buffer)) {
            cmd->args[1] = -1;

            // Done Parsing, now reverse train!
            bufferPutStr(result, "Initializing Train "); 
            bufferPutInt(result, train);
            bufferPutStr(result, " with autofind");
        } else {
            // Get Destination
            char dest_str[6];
            int i = 0;
            while( ! isWhitespace( (dest_str[i] = bufferPop(cmd_buffer)) ) ) {
                i++; 
                if(i >= 6) {
                    bufferPutStr(result, "Bad dest. Name too long.");
                    return PARSE_ERROR;
                }
            }
            dest_str[i] = '\0';
            cmd->args[1] = trackNode_NameToIndex(dest_str);

            bufferPutStr(result, "Initializing Train "); 
            bufferPutInt(result, train);
            bufferPutStr(result, " at node ");
            bufferPutStr(result, dest_str);
        }
    } else if( raw_cmd[0] == 'a') {
        cmd->type = ACCELERATION;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(bufferPop(cmd_buffer) != ' ' || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int accel = bufferGetInt(cmd_buffer, 3); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || accel < 0) {
            bufferPutStr(result, "Bad Acceleration: ");
            bufferPutInt(result, accel);
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;
        cmd->args[1] = accel;

        bufferPutStr(result, "Setting Acceleration: "); 
        bufferPutInt(result, train);
        bufferPutStr(result, "x"); 
        bufferPutInt(result, accel);

    } else if( raw_cmd[0] == 'v') {
        cmd->type = VELOCITY;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(bufferPop(cmd_buffer) != ' ' || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int velocity = bufferGetInt(cmd_buffer, 3); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || velocity < 0) {
            bufferPutStr(result, "Bad Velocity");
            bufferPutInt(result, velocity);
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;
        cmd->args[1] = velocity;

        bufferPutStr(result, "Setting Velocity: "); 
        bufferPutInt(result, train);
        bufferPutStr(result, "x"); 
        bufferPutInt(result, velocity);

    } else if( raw_cmd[0] == 'd') {
        cmd->type = DECCELERATION;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(bufferPop(cmd_buffer) != ' ' || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int deccel = bufferGetInt(cmd_buffer, 3); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || deccel < 0) {
            bufferPutStr(result, "Bad Decceleration");
            bufferPutInt(result, deccel);
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;
        cmd->args[1] = deccel;

        bufferPutStr(result, "Setting Decceleration: "); 
        bufferPutInt(result, train);
        bufferPutStr(result, "x"); 
        bufferPutInt(result, deccel);
    } else if( raw_cmd[0] == 's' && raw_cmd[1] == 'd') {
        cmd->type = STOPPING_DISTANCE_CALIBRATION;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(bufferPop(cmd_buffer) != ' ' || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int speed = bufferGetInt(cmd_buffer, 2); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || speed < 0 || speed > 14) {
            bufferPutStr(result, "Bad Speed Number. Use Range [1..14]. Got ");
            bufferPutInt(result, speed);
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;
        cmd->args[1] = speed;

        bufferPutStr(result, "Stopping Distance Calibration, Train: "); 
        bufferPutInt(result, train);
        bufferPutStr(result, ", Speed: "); 
        bufferPutInt(result, speed);
    } else if( raw_cmd[0] == 's' && raw_cmd[1] == 't') {
        cmd->type = STOPPING_TIME_CALIBRATION;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int train = bufferGetInt(cmd_buffer, 2); 
        if(bufferPop(cmd_buffer) != ' ' || train < 1 || train > 80) {
            bufferPutStr(result, "Bad Train Number. Must be in range [1..80]");
            return PARSE_ERROR;
        }   

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        int speed = bufferGetInt(cmd_buffer, 2); 
        if(! isWhitespace(bufferPop(cmd_buffer)) || speed < 0 || speed > 14) {
            bufferPutStr(result, "Bad Speed Number. Use Range [1..14]. Got ");
            bufferPutInt(result, speed);
            return PARSE_ERROR;
        }   

        cmd->args[0] = train;
        cmd->args[1] = speed;

        bufferPutStr(result, "Stopping Time Calibration, Train: "); 
        bufferPutInt(result, train);
        bufferPutStr(result, ", Speed: "); 
        bufferPutInt(result, speed);
    } else if( raw_cmd[0] == 'b' && raw_cmd[1] == 's') {
        cmd->type = BREAK_SENSOR;

        if(bufferEmpty(cmd_buffer)) {
            bufferPutStr(result, "Malformed Command");
            return PARSE_ERROR;
        }

        // Get Destination
        char dest_str[6];
        int i = 0;
        while( ! isWhitespace( (dest_str[i] = bufferPop(cmd_buffer)) ) ) {
            i++; 
            if(i >= 6) {
                bufferPutStr(result, "Bad dest. Name too long.");
                return PARSE_ERROR;
            }
        }
        dest_str[i] = '\0';
        cmd->args[0] = trackNode_NameToIndex(dest_str);

        bufferPutStr(result, "Breaking Sensor "); 
        bufferPutStr(result, dest_str);

    } else {
        bufferPutStr(result, "Unrecognized Command");
        return PARSE_ERROR;
    }   

    return NO_ERROR;
}

static void resetCommand(Command *cmd)
{
    cmd->type = NONE;
}

// returns index into trains array
static int trainToIndex(int train_number)
{
    // Stop all trains
    switch(train_number) {
    case 45: return 0;
    case 48: return 1;
    case 54: return 2;
    case 56: return 3;
    case 58: return 4;
    case 12: return 5;
    case 24: return 6;
    case 62: return 7;
    default:
        // Do nothing
        break;
    }

    ERROR("trainToIndex: Invalid Train Number: %d", train_number); 
    return ERR_INVALID_TRAIN_NUMBER;
}
 
void Controller()
{
    // Sleep for 2 seconds before doing anything
    Delay(2 * SECOND_DELAY);

    TaskId input_channel = WhoIs(TERM_GET_SERVER);
    if(! isValidTaskId(input_channel) ) {
        ERROR("Controller failed to get input channel: %d", input_channel); 
        Exit();
    }

    TaskId output_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(output_channel) ) {
        ERROR("Controller failed to get output channel: %d", output_channel); 
        Exit();
    }

    TaskId train_out_channel = WhoIs(TRAIN_PUT_SERVER);
    if(! isValidTaskId(train_out_channel) ) { 
        ERROR("Trains failed to lookup train out channel: %d", train_out_channel);
        Exit();
    }   

    // Clear Screen and Hide Cursor
    putS(output_channel, "\033[2J\033[?25l");
    putS(output_channel, "\033[32;0HInitializing Train Controller");

    // GO!
    putc(train_out_channel, 0x60);

    // Start independent worker tasks
    TaskId timekeeper_tid = Create(CLOCK_SERVER_PRIORITY, &TimeKeeper);
    if(! isValidTaskId(timekeeper_tid) ) {
        ERROR("Controller failed to create TimeKeeper: %d", timekeeper_tid); 
        Exit();
    }

    TaskId switches_tid = Create(TRAIN_HELPER_HI_PRIORITY, &Switches);
    if(! isValidTaskId(switches_tid) ) {
        ERROR("Controller failed to create Terminal: %d", switches_tid); 
        Exit();
    }

    TaskId sensors_server_tid = Create(TRACK_SERVERS_PRIORITY, &SensorServer);
    if(! isValidTaskId(sensors_server_tid) ) {
        ERROR("Failed to create Sensors Task: %d", sensors_server_tid); 
        Exit();
    }

    TaskId sensors_tid = Create(TRACK_IO_NOTIFIER_PRIORITY, &Sensors);
    if(! isValidTaskId(sensors_tid) ) {
        ERROR("Failed to create Sensors Task: %d", sensors_tid); 
        Exit();
    }

    TaskId track_locker_tid = Create(TRACK_SERVERS_PRIORITY, &TrackLocker);
    if(! isValidTaskId(track_locker_tid) ) {
        ERROR("Failed to create Sensors Task: %d", (ErrorCode)track_locker_tid); 
        Exit();
    }

    DEBUG("sensors_server_td = %d", sensors_server_tid);

    // Path Finder
    TaskId path_finder_tid = Create(TRAIN_HELPER_LO_PRIORITY, &PathFinder);
    if(! isValidTaskId(path_finder_tid) ) {
        ERROR("Failed to create Sensors Task: %d", path_finder_tid); 
        Exit();
    }

    char template[10];
    template[0] = '\033';
    template[1] = '[';
    template[2] = '3';
    template[3] = '2';
    template[4] = ';';
    template[5] = '*';
    template[6] = '*';
    template[7] = 'H';
    template[8] = '*';
    template[9] = '\0';

    // Wait for initialization messages from trains & switches
/*
    int i = 9;
    for(; i >= 0; --i) {
        template[5] = '3';
        template[6] = '1';
        template[8] = '0' + (char)i;
        putS(output_channel, template);

        Delay(SECOND_DELAY);
    }
*/

    // Array of train speeds
    int trains[MAX_TRAIN_ID];
    int j = 0;
    for(j = 0; j < MAX_TRAIN_ID; ++j) trains[j] = 0;

/*
    int train_number = 45;
    int train_index = trainToIndex(45);
    trains[train_index] = Create(USER_HI_PRIORITY, &Train); 
    Send(trains[train_index], &train_number, sizeof(train_number), 0, 0);
    Command cmd;
    cmd.args[1] = trackNode_NameToIndex("D7");
    cmd.type = MOVE;
    ErrorCode exec_error = NO_ERROR;  
    Send(trains[train_index], &cmd, sizeof(cmd), &exec_error, sizeof(exec_error));
*/

    TaskId switches_courier = INVALID_TID;

    int num_trains = 0;

    FOREVER {
        // Setup User Prompt
        putS(output_channel, "\033[32;0H\033[2KPrompt> ");

        // Get User Command
        Buffer command_buffer;
        bufferReset(COMMAND_BUFFER, &command_buffer);
        ErrorCode read_error = NO_ERROR;
        int size = 9;
        while(( read_error = getc(input_channel) ) != 0x0D) { // does not equal <Enter>
            if(read_error < 0) {
                ERROR("Controller failed to get character from term get server: %d", read_error);
                break;
            }

            char c = (char)read_error;
            if(c == 0x08) {
                if(size == 9) continue;

                bufferBackspace(&command_buffer);
                // Erase last character
                size -= 1;
                template[6] = '0' + (char)(size % 10); 
                template[5] = '0' + (char)((size / 10) % 10); 
                template[8] = ' ';
            } else {
                bufferPut(&command_buffer, c);

                // Move cursor to correct position and print next character
                template[6] = '0' + (char)(size % 10); 
                template[5] = '0' + (char)((size / 10) % 10); 
                template[8] = c;
                size++;
            }

            putS(output_channel, template);
        }
        bufferPut(&command_buffer, 0x0D);

        // Parse Command
        Command cmd;
        resetCommand(&cmd);
        Buffer cmd_str;
        bufferReset(COMMAND_STR, &cmd_str);
        if(parseCommand(&cmd, &command_buffer, &cmd_str)) {
            printResult(output_channel, &cmd_str, NO_ERROR);
            continue;
        }       
    
        // Handle Command
        ErrorCode send_error = NO_ERROR;
        ErrorCode exec_error = NO_ERROR;  
        switch(cmd.type) {
/*
            case SWITCH:
                send_error = Send(switches_tid, &cmd, sizeof(cmd), &exec_error, sizeof(exec_error));
                break;
*/
            case CHECK_PATH:
                {
                    Path path;
                    ErrorCode send_error = getPath(cmd.args[0], cmd.args[1], &path, FALSE);
                    if(! send_error) {
                        bufferPut(&cmd_str, ':');
                        bufferPutStr(&cmd_str, " dist=");
                        bufferPutInt(&cmd_str, path.distance); 
                        bufferPutStr(&cmd_str, " mm .. ");
                        int i = 0;
                        for(; i < path.num_nodes; ++i) {
                            bufferPut(&cmd_str, ' ');
                            bufferPutStr(&cmd_str, (char*)trackNode_IndexToName(path.nodes[i]));
                        }   
                    }
                }
                break;

            case INIT:
                {

                ERROR("\r\nA1+%d", cmd.args[1]);
                    int train_number = cmd.args[0];
                    int train_index = trainToIndex(train_number);
                    if(trains[train_index] == 0) {
                        trains[train_index] = Create(TRAIN_DRIVER_PRIORITY, &Train); 
                        if(! isValidTaskId(trains[train_index]) ) {
                            bufferPutStr(&cmd_str, ": Failed to create Train Task: ");
                            bufferPutInt(&cmd_str, (ErrorCode)trains[train_index]);
                        }
                    }

                    cmd.train_index = num_trains;
                    num_trains++;

                    ErrorCode error = Send(trains[train_index], &cmd, sizeof(cmd), &exec_error, sizeof(exec_error));
                    if(error) {
                        bufferPutStr(&cmd_str, ": Failed to create Send Init Msg: ");
                        bufferPutInt(&cmd_str, (ErrorCode)trains[train_index]);
                    }
                }
                break;

            case SWITCH:
                while(! isValidTaskId(switches_courier)) {
                    switches_courier = WhoIs(SWITCHES_COURIER);
                } 
                exec_error = Send(switches_courier, &cmd, sizeof(cmd), &exec_error, sizeof(exec_error));
                break;

            case ACCELERATION:
            case DECCELERATION:
            case VELOCITY:
            case SPEED:
            case REVERSE:
            case MOVE:
            case STOPPING_DISTANCE_CALIBRATION:
            case STOPPING_TIME_CALIBRATION:
                {
                    int train_number = cmd.args[0];
                    int train_index = trainToIndex(train_number);
                    if(trains[train_index] == 0) {
                        bufferPutStr(&cmd_str, ": Train ");
                        bufferPutInt(&cmd_str, train_number);
                        bufferPutStr(&cmd_str, " Is Not Initialized");

                        send_error = ERR_TRAIN_NOT_INITIALIZED;
                        break;
                    }

                    send_error = Send(trains[train_index], &cmd, sizeof(cmd), &exec_error, sizeof(exec_error));
                }
                break;

            case BREAK_SENSOR:
                send_error = Send(sensors_tid, &cmd, sizeof(cmd), &exec_error, sizeof(exec_error)); 
                break;

            default:
                // Unrecognized Command
                exec_error = UNRECOGNIZED_USER_COMMAND;
                break;
        }

        if(send_error)      printResult(output_channel, &cmd_str, send_error);
        else if(exec_error) printResult(output_channel, &cmd_str, exec_error);
        else                printResult(output_channel, &cmd_str, NO_ERROR);
    } 
    
    DEBUG("Controller Finished Run");
    Exit();
}
