#include "clock.h"
#include "clockserver_api.h"
#include "io.h"
#include "task.h"
#include "debug.h"
#include "common.h"
#include "nameserver_api.h"
#include "buffer.h"

static void printClock(TaskId output_channel, Clock const *c) 
{
    char template[19];//"\033[HClock: xx:xx:xx"; 
    template[0] = '\033';
    template[1] = '[';
    template[2] = 'H';
    template[3] = 'C';
    template[4] = 'l';
    template[5] = 'o';
    template[6] = 'c';
    template[7] = 'k';
    template[8] = ':';
    template[9] = ' ';
    template[10] = '0';
    template[11] = '0';
    template[12] = ':';
    template[13] = '0';
    template[14] = '0';
    template[15] = ':';
    template[16] = '0';
    template[17] = '0';
    template[18] = '\0';
    
    template[11] = '0' + (char)(c->minute % 10);
    template[10] = '0' + (char)((c->minute / 10) % 10);

    template[14] = '0' + (char)(c->second % 10);
    template[13] = '0' + (char)((c->second / 10) % 10);

    int decisecond = c->hs / 10; 
    template[17] = '0' + (char)(decisecond % 10);
    template[16] = '0' + (char)((decisecond / 10) % 10);

    ErrorCode error = putS(output_channel, template);
    if(error) {
        ERROR("Failed to print clock: %d", error);
    }
}

void TimeKeeper()
{
    Clock clock;        
    resetClock(&clock);

    TaskId output_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(output_channel) ) {
        ERROR("Timekeeper failed to lookup terminal put server: %d", output_channel);
    }

    FOREVER {
/*
        putS(output_channel, "");
        putD(output_channel, i++);
*/
        printClock(output_channel, &clock);
        Delay(DECISECOND_DELAY);
        incrementClockDS(&clock);
    }
}

