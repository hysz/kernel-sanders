#pragma once


struct SensorWatchDogInfo {
    int timeout;
    int sensorNum;
};
typedef struct SensorWatchDogInfo SensorWatchDogInfo;

void SensorWatchDog(int timeout);
