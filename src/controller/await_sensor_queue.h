#pragma once

#include "errno.h"
#include "buffer.h"
#include "task.h"

#define ASQ_QUEUE_SIZE 10
#define ASQ_MAX_MSG_LEN 1024

struct ASQueueItem {
    int sensorNum;
    float distance;
    int trainNum;
};
typedef struct ASQueueItem ASQueueItem;

struct ASQueue {
    ASQueueItem data[ASQ_QUEUE_SIZE];
    int next; 
    int len;
};
typedef struct ASQueue ASQueue;

void queueReset_ASQ(ASQueue *q);
ErrorCode queuePush_ASQ(ASQueue * q, int sensorNum, float distance, int trainNum);
int queueEmpty_ASQ(ASQueue* q);
ASQueueItem* queuePop_ASQ(ASQueue* q);
ASQueueItem* queuePeek_ASQ(ASQueue* q);
