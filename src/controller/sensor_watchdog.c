#include "sensor_watchdog.h"
#include "kapi.h"
#include "clockserver_api.h"
#include "common.h"
#include "sensorserver.h"
#include "nameserver_api.h"
#include "debug.h"

void SensorWatchDog(int timeout) {
    
    TaskId sensor_server_tid = WhoIs(SENSOR_SERVER);
    SensorWatchDogInfo swdi;

    FOREVER {
        TaskId tid = INVALID_TID;
        Receive(&tid, &swdi, sizeof(SensorWatchDogInfo));
        if (tid != sensor_server_tid) {
            ERROR("Uhh..WHo is using the SENSOR WATCHDOG??? : %d", tid);
            Exit();
        }
        Reply(tid, 0, 0);

        //we got a job!
        Delay(swdi.timeout);

        //Tell the sensor server
        SensorTimeoutMsg stm;
        stm.type = SENSOR_TIMEOUT;
        stm.sensorNum = swdi.sensorNum;
        SensorsReply sr;
        Send(sensor_server_tid, &stm, sizeof(SensorTimeoutMsg), &sr, sizeof(SensorsReply));
    }
}

