#include "common.h"
#include "task.h"
#include "task_set.h"
#include "context_switch.h"
#include "clock.h"
#include "hardware.h"
#include "kernel_tasks.h"
#include "debug.h"
#include "term.h"
#include "hw_train.h"
#include "buffer.h"
#include "track_helpers.h"
#include "ts7200.h"
#include "io.h"
#include "bwio.h"

int main(int argc, char* argv[])
{
    volatile int numDebugLines = 0;
    numDebugLines = 0;

    /*** Squash Previous Train/Term State ***/
    while(trainHasChar()) getTrainChar();
    while(termHasChar()) getTermChar();

    /*** Initialize Kernel Data Structures ***/
    // Tasks
    TaskSet tasks;
    Task* active_task;
    resetTaskSet(&tasks);

    // Messages
    Message messages[MAX_TASK_COUNT + 1];

    // Events
    int eventIdTable[EVENT_TABLE_SIZE]; 
    EventWaitInfo waitingTasks[EVENT_TABLE_SIZE];
    initEventTables(eventIdTable, waitingTasks);
    int track_ready[2] = {0};

    // Track
    const track_node track[TRACK_MAX];
    initTrack((track_node*)track);

    /****** Enter Kernel Mode & Enable Caching ******/
    kerinit();

    /*** Initialize Interrupts & Hardware ***/
    // Important that interrupts are initialized first so we don't miss any!!!
    //bwprintf(COM2, "Main");
    initializeHardware();
    initializeInterrupts();

    // We want to read the address to get things moving. This hack gets rid of the unused variable warning by gcc.
    int a = *(int*)(UART1_BASE + UART_MDMSTS_OFFSET) & 0x10;
    a = 0;

    /*** Bootstrap Task ***/
    TaskId active_tid = createTask(BOOTSTRAP_PRIORITY, &bootstrapTask, &tasks, KERNEL_PARENT_TASK_ID); 
    if (active_tid != BOOTSTRAP_TID) {
        KDEBUG("Bootstrap Task created with unexpected Task Id: %d", active_tid);
        return 1;
    }

    // Send initialization message containin ptr to track
    int k = (int)track;
    ErrorCode error = sendMessage(BOOTSTRAP_TID, &k, sizeof(k), 0, 0, &(tasks.data[BOOTSTRAP_TID]), messages, &tasks);
    if(error) {
        KERROR("Failed to send init message to bootstrap task: %d", error);
        return 1;
    }

/* Measurement Tests
    TaskId active_tid;
    createTask(HI_PRIORITY, &sendTask, &tasks, KERNEL_PARENT_TASK_ID); 
    createTask(HI_PRIORITY, &recvTask, &tasks, KERNEL_PARENT_TASK_ID); 
*/

    char idle_time_template[19];
    idle_time_template[0] = '\033';
    idle_time_template[1] = '[';
    idle_time_template[2] = '1';
    idle_time_template[3] = ';';
    idle_time_template[4] = '2';
    idle_time_template[5] = '0';
    idle_time_template[6] = 'H';
    idle_time_template[7] = 'I';
    idle_time_template[8] = 'D';
    idle_time_template[9] = 'L';
    idle_time_template[10] = 'E';
    idle_time_template[11] = ':';
    idle_time_template[12] = ' ';
    idle_time_template[13] = '0';
    idle_time_template[14] = '0';
    idle_time_template[15] = '.';
    idle_time_template[16] = '0';
    idle_time_template[17] = '%';
    idle_time_template[18] = '\0';

    unsigned int idle_time = 0;
    int percentage = 0;
    unsigned int total_time = 0;
    int i = 0;

    FOREVER {
        active_tid = getNextTask(&tasks, &active_task);
        if( ! isValidTaskId(active_tid) ) {
            int error = active_tid;
            if(error != ERR_QUEUE_EMPTY) { // -1 -> queue empty
                KDEBUG("\033[2J KERNEL KERROR: Failed to get next task: %d", error);
                goto Die;
            }

            continue;
        } 

        // Sanity Test on Task before we enable it
        //KERROR("Task %d 0x%x 0x%x 0x%x", active_task->id, active_task->sp, active_task->stack, active_task->pc);
        if(active_task->sp < (int)active_task->stack) {
            KERROR("\033[2J KERNEL KERROR: Stack Overflow!!! (%d)", active_task->id);
            goto Die;
        }

        // Process active task
        int *user_args;
        switch(active_task->state) {
        case READY:
            active_task->state = ACTIVE;
            int start_time = *(unsigned int*)(TIMER3_BASE + VAL_OFFSET);//  * ((float)1000 / 5080);;

/*
                KERROR("\033[2JSS=%d", (int)start_time);
                goto Die;
*/

            user_args = kerxit(active_task->sp, active_task->cpsr, active_task->pc, active_task->retval);
            if(active_task->sp < (int)active_task->stack) {
                KERROR("\033[2J KERNEL KERROR: Stack Overflow!!! (%d)", active_task->id);
                goto Die;
            }

            int end_time = *(unsigned int*)(TIMER3_BASE + VAL_OFFSET);// * ((float)1000 / 5080);
            if(i > 100) total_time += (start_time - end_time);// * (float)1000/508;

            newHandle(user_args, active_task, &tasks, messages, waitingTasks, eventIdTable, track_ready);

            if(i > 100 && active_task->priority == IDLE_PRIORITY) {
                // Hacky -- hardcoded priority of io put daemon
                idle_time += (start_time - end_time);// * (float)1000/508;
                
                int new_percentage = 1000 * ( (float)idle_time / total_time);
/*
                    if(i++ > 100) {
                        KERROR("P=%d; TT=%u, TI=%u, Ye=", new_percentage, total_time, idle_time);
                        goto Die;
                    }
*/
                if(percentage != new_percentage) {
                    percentage = new_percentage;

                    idle_time_template[13] = '0';
                    idle_time_template[14] = '0';
                    idle_time_template[16] = '0';

                    if(percentage < 10) {
                        idle_time_template[16] = '0' + (char)percentage;
                    } else if(percentage < 100) {
                        idle_time_template[16] = '0' + (char)(percentage % 10);
                        percentage /= 10;
                        idle_time_template[14] = '0' + (char)percentage;
                    } else if(percentage < 1000) {
                        idle_time_template[16] = '0' + (char)(percentage % 10);
                        percentage /= 10;
                        idle_time_template[14] = '0' + (char)(percentage % 10);
                        percentage /= 10;
                        idle_time_template[13] = '0' + (char)percentage;
                    } else {
                        idle_time_template[16] = '9';
                        idle_time_template[14] = '9';
                        idle_time_template[13] = '9';
                    }
                    percentage = new_percentage;

                    PutRequest req;
                    req.type = PUT_S;
                    req.str = idle_time_template;

                    PutReply reply;
                    reply.error = NO_ERROR;
                    
                    //ErrorCode error = sendMessage(6, &req, sizeof(req), 0, 0, &(tasks.data[6]), messages, &tasks);
                    if(error) {
                        KERROR("\r\nKERNEL FAILED TO UPDATE IDLE TIME: %d", reply.error);
                    }
                    
                }
            }   
            ++i;

            break;

        case SEND_UNBLOCKED:
            active_task->state = ACTIVE;
            newHandle(active_task->kapi_args, active_task, &tasks, messages, waitingTasks, eventIdTable, track_ready);
            break;

        default:                
            // Do nothing
            break;
        }

        // Add task back to ready queue
        if(active_task->state == ACTIVE) {
            int error = enqueueTask(active_task->id, active_task->priority, &tasks); 
            if(error != NO_ERROR) {
                KDEBUG("\033[2J KERNEL KERROR: Failed enqueue task: %d", error);
                goto Die;
            }
        } else {
            //KDEBUG("ZOMBIE!: %d", active_tid);
        }
    }

    Die:
    KDEBUG("KERNEL LOOP EXITED");
    return 0;
}
