#include "event.h"
#include "debug.h"

void initEventTables(int* eventBitTable, EventWaitInfo* waitInfoTable) {
    int i;
    for (i = 0; i < EVENT_TABLE_SIZE; i++) {
        eventBitTable[i] = 0;
        waitInfoTable[i].waiter = UNOCCUPIED_WAIT_TID;
        waitInfoTable[i].eventData = 0;
        waitInfoTable[i].len = 0;
    }

    //Store all Event IDs into eventBitTable in priority greatest -> lowest
    eventBitTable[TIMER_EVENT] = TIMER3_INT_BIT;
    eventBitTable[UART1_OR_EVENT] = UART1_OR_INT_BIT;
    eventBitTable[UART2_OR_EVENT] = UART2_OR_INT_BIT;
}

int getBitNumFromId(int eventId, int* eventBitTable) {
    if (eventId < 0 || eventId > MAX_INTERRUPTS) {
        return INVALID_INTRPT_BIT_ERROR;
    }
    return eventBitTable[eventId];
}

int awaitEvent(Task* active_task, EventType eventType, void* event, int eventlen, EventWaitInfo* waitInfoTable, int* eventBitTable) {
//KDEBUG("%d AWAITING %d", active_task->id, eventType);
    if (eventType < 0 || eventType >= NUM_EVENTS) {
        KERROR("awaitEvent() Invalid Event Type = %d", eventType);
        return INVALID_EVENT_TYPE_ERROR;
    }

    //Check if a task is already waiting on the event
    EventWaitInfo* ewi = &(waitInfoTable[eventType]);
    if (ewi->waiter != UNOCCUPIED_WAIT_TID) {
        KERROR("awaitEvent() another task is already blocked on this event");
        return EVENT_WAIT_FULL_ERROR;    
    }

    //Store the information into corresponding EventWaitInfo
    ewi->waiter = active_task->id;
    ewi->eventData = event;
    ewi->len = eventlen;

    //Event Block the active task
    //KDEBUG("awaitEvent task id: %d", active_task->id);
    active_task->state = EVENT_BLOCKED;

    // Enable required interrupts
    switch(eventType) {
        case TERM_GET_READY:
            enableTermRxInterrupts();
            break;

        case TERM_PUT_READY:
            enableTermTxInterrupts();
            break;

        case TRAIN_GET_READY:
            enableTrainRxInterrupts();
            break;

        case TRAIN_PUT_READY:
            enableTrainTxInterrupts();
            break;

        default:
            // do nothing
            break;
    }

    return NO_ERROR;
}

int wakeUpTask(EventType recentlyOccuredEvent, EventWaitInfo* waitInfoTable, TaskSet* tasks) {
    //KDEBUG("Wakeup Task");
    if (recentlyOccuredEvent < 0 || recentlyOccuredEvent > NUM_EVENTS) {
        return INVALID_EVENT_TYPE_ERROR;
    }

    EventWaitInfo* ewi = &(waitInfoTable[recentlyOccuredEvent]);
    //If there is no task waiting on the event, just return
    if (ewi->waiter == UNOCCUPIED_WAIT_TID) {
        //KDEBUG("No Task Waiting");
        return NO_ERROR;
    }

    //Error checking code, can be removed later
    if (tasks->data[ewi->waiter].state != EVENT_BLOCKED && tasks->data[ewi->waiter].state != CTS_BLOCKED) {
        return WAITER_NOT_EVENT_BLOCKED_ERROR;
    }

    //Wake Up the event blocked task and enqueue it
    tasks->data[ewi->waiter].state = READY; 
    int error = enqueueTask(ewi->waiter, tasks->data[ewi->waiter].priority, tasks);
    if(error < 0) {
        KERROR("wakeUpTask() Failed to enqueue task. id=%d; error=%d", tasks->data[ewi->waiter].id, error);
    }

    //Reset the EventWaitInfo
    ewi->waiter = UNOCCUPIED_WAIT_TID;
    ewi->eventData = 0;
    ewi->len = 0;

    // Disable interrupt if event is associated with a get/put
    switch(recentlyOccuredEvent) {
        case TERM_GET_READY:
            disableTermRxInterrupts();
            break;

        case TERM_PUT_READY:
            disableTermTxInterrupts();
            break;

        case TRAIN_GET_READY:
            disableTrainRxInterrupts();
            break;

        case TRAIN_PUT_READY:
            disableTrainTxInterrupts();
            break;

        default:
            // do nothing
            break;
    }

    //Should be NO_KERROR if task enqueueing was successful
    return error;
}

ErrorCode transferEventData(EventWaitInfo* ewi, void* source, int sourcelen) {
    if (source != 0 && sourcelen != 0) {
        if (ewi->len < sourcelen) {
            KERROR("\n\rEvent Data length mismatch (%d < %d)", ewi->len, sourcelen);
            return EVENT_DATA_LEN_MISMATCH;
        }
        cpyN(ewi->eventData, source, sourcelen);
    }
    return NO_ERROR;
}

