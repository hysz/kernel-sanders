#pragma once
#include "task.h"
#include "task_set.h"
#include "interrupt.h"
#include "errno.h"
#include "common.h"
#include "debug.h"

#define EVENT_TABLE_SIZE 64
#define UNOCCUPIED_WAIT_TID -1

/* The EventTable is a table of interrupt bit numbers. 
 * It is indexed by EventType.
 *
 * The EventWaitInfo is a table of waiting tasks (& other data) for each event.
 * Currently only one task can wait for any event at any given time. 
 */

struct EventWaitInfo {
    TaskId waiter;
    void* eventData;
    int len;
};
typedef struct EventWaitInfo EventWaitInfo;

/* Events ordered in descending priority. Re-assess priorities later?
 * For UARTS, UART1 > UART2 cause Trains > Everything else
 * RCV > TX (atleast for now, I figured RCV is blocking so it should unblock ASAP)
 */
typedef enum {  
                TIMER_EVENT,
                UART1_OR_EVENT,
                UART2_OR_EVENT,
                TERM_PUT_READY,
                TERM_GET_READY,
                TRAIN_PUT_READY,
                TRAIN_GET_READY,
                NUM_EVENTS
            } EventType;

//Event Data structs

struct TermGetChar {
    char c;
};
typedef struct TermGetChar TermGetChar;

void initEventTables(int* eventBitTable, EventWaitInfo* waitInfoTable);

int getBitNumFromId(int eventId, int* eventBitTable);

int awaitEvent(Task* active_task, EventType eventType, void* event, int eventlen, EventWaitInfo* waitInfoTable, int* eventBitTable);

int wakeUpTask(EventType recentlyOccuredEvent, EventWaitInfo* waitInfoTable, TaskSet* tasks);

int transferEventData(EventWaitInfo* ewi, void* source, int sourcelen);

