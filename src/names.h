#pragma once

/* These should be enumerated 1..N */
/* ^^ Lol, maybe use an enumerated type?? */
/* ^^ Go away */

#include "task.h"

typedef enum {
    NO_NAME,
    IDLE_TASK,
    NAMESERVER,
    CLOCK_SERVER,
    TERM_PUT_SERVER,
    TERM_GET_SERVER,
    TRAIN_PUT_SERVER,
    TRAIN_GET_SERVER,
    PATH_SERVER,
    TRACK_SERVER,
    SENSOR_SERVER,
    SWITCHES_SERVER,
    SWITCHES_COURIER,
    TRACK_LOCKER
} Name;
