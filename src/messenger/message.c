#include "message.h"
#include "common.h"
#include "debug.h"

int copyStackData(Task* from, Task* to, void *data, int len)
{
    int new_to_sp = to->sp - len;
    if(new_to_sp < (int)to->stack) return -1; 
    to->sp = new_to_sp;
    
    return cpyN((void *)new_to_sp, data, len);
}

int sendMessage(TaskId tid, void *msg, int msg_len, void *reply, int replylen, Task *active_task, Message *messages, TaskSet *tasks)
{
    Task* receiver_task = &tasks->data[tid];
    if (receiver_task->rcv_queue_size >= MAX_RECEIVE_QUEUE_SIZE) {
        KERROR("Receiver queue full (recv tid = %d)", tid);
        return RECEIVE_QUEUE_FULL;
    }

    Message *message = &messages[active_task->id];

    // Update Fields of Message
    message->sender = active_task->id;
    message->receiver = tid;
    message->message = msg;
    message->mslen = msg_len;
    message->reply = reply;
    message->rplen = replylen;

    //Put sender tid to receiver's queue
    int pushIndex = (receiver_task->rcv_msg_front + receiver_task->rcv_queue_size) % MAX_RECEIVE_QUEUE_SIZE;
    receiver_task->rcv_msg_indices[pushIndex] = message->sender;
    receiver_task->rcv_queue_size++;
 
    // Update Sender State to RECEIVE_BLOCKED
    // Allow sending a message to oneself
    // This allows kernel to send messages to tasks without a real sender :)
    if(active_task->id != tid) active_task->state = RECEIVE_BLOCKED;

    //Update receiver task state to SEND_UNBLOCKED and putit on the task queue
    if(receiver_task->state == SEND_BLOCKED) {
        int error = enqueueTask(receiver_task->id, receiver_task->priority, tasks); 
        if(error != NO_ERROR) KDEBUG("KERNEL KERROR: Failed enqueue task: %d", error);

        receiver_task->state = SEND_UNBLOCKED;
    } 

    return 0;
}

/* A task enters this function with ACTIVE state and leaves with state {ACTIVE, SEND_BLOCKED}
 *
 */
int receiveMessage(Task* active_task, TaskId *senderTid, void *msg, int msglen, Message* messages, TaskSet* tasks, int peeking)
{
    if(active_task->rcv_queue_size == 0) {
        //Change this tasks state to BLOCKED
        if (!peeking) {
            active_task->state = SEND_BLOCKED;
        }
        return NO_MSG_TO_RECEIVE;
    }

    //Copy the message content and message length
    *senderTid = active_task->rcv_msg_indices[active_task->rcv_msg_front];
    Message* m = &messages[*senderTid];
    if(m->mslen > msglen) {
        KERROR("Recv MSG LEN MISMATCH: %d > %d (from=%d, to=%d)", m->mslen , msglen, *senderTid, active_task->id);
        return MSG_LEN_MISMATCH;
    }
    cpyN(msg, m->message, m->mslen);

    if (peeking) {
        //If peeking, just return after taking the data!
        return NO_ERROR;
    }

    //Shift the front index of receive queue
    active_task->rcv_msg_indices[active_task->rcv_msg_front] = 0;
    active_task->rcv_msg_front = (active_task->rcv_msg_front + 1) % MAX_RECEIVE_QUEUE_SIZE;
    active_task->rcv_queue_size--;
    if (active_task->rcv_queue_size == 0) {
        active_task->rcv_msg_front = 0;
    }

    //Error Checking code, can be removed later
    // The first check is for if the task is sending a message to itself
    if (*senderTid != active_task->id && tasks->data[*senderTid].state != RECEIVE_BLOCKED) {
        return SENDER_NOT_RECEIVE_BLOCKED_ERROR;
    }
    //Set the sender state to REPLY_BLOCKED
    if(tasks->data[*senderTid].state == RECEIVE_BLOCKED) {
        tasks->data[*senderTid].state = REPLY_BLOCKED;
    }
    
    return NO_ERROR;
}

int replyMessage(TaskId tid, void *reply, int replylen, Message* messages, TaskSet* tasks)
{
    //KDEBUG("Replying to TID: %d", tid);
    
    //Copy the reply message into the original sender's Message Box
    Message* m = &messages[tid];
    if (m->rplen < replylen) {
        return REPLY_MSG_LEN_MISMATCH;
    }
    cpyN(m->reply, reply, replylen);

    //Error Checking code, can be removed later
    if (tasks->data[tid].state != REPLY_BLOCKED) {
        return SENDER_NOT_REPLY_BLOCKED_ERROR;
    }
    //Set the original sender's state to READY and enqueue it
    tasks->data[tid].state = READY; 
    int error = enqueueTask(tid,tasks->data[tid].priority, tasks);
    if(error != NO_ERROR) KDEBUG("KERNEL KERROR: Failed enqueue task: %d", error);

    return error;
}
