#pragma once
#include "task_set.h"

struct Message {
    TaskId sender;
    TaskId receiver;
    void *message;
    int mslen;
    void *reply;
    int rplen;
};
typedef struct Message Message;

/*
 * Description: Used to copy data from one task stack to another task's stack
 * Returns:
 *              Number of bytes copied
 *              -1 if stack overflow on <to>'s stack
 */
int copyStackData(Task* from, Task* to, void *data, int len);

int sendMessage(TaskId tid, void *msg, int msg_len, void *reply, int replylen, Task* active_task, Message *messages, TaskSet* tasks);

int receiveMessage(Task* active_task, TaskId *senderTid, void *msg, int msglen, Message* messages, TaskSet* tasks, int peeking);

int replyMessage(TaskId tid, void *reply, int replylen, Message* messages, TaskSet*tasks);
