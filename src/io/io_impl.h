#pragma once

#include "errno.h"
#include "task.h"
#include "names.h"
#include "event.h"

#define GET_C_QUEUE_SIZE 100

typedef enum {IO_GETTER, IO_PUTTER} IoServerType;

struct IoInitMsg {
    IoServerType server_type;
    Name name;
    int* address;
    int* flags;
    EventType notifier_event; 
};
typedef struct IoInitMsg IoInitMsg;

int* termPutAddress();
int* termGetAddress();
int* trainPutAddress();
int* trainGetAddress();

void IoNotifier();
void IoServer();
