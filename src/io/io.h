#pragma once

#include "errno.h"
#include "task.h"
#include "buffer.h"

typedef enum {PUT_C, PUT_S, PUT_B, PUT_B_BLOCKING, GET, NOTIFY, TIMEOUT} IORequest_Type;

/*
 * Put Request Structures
 */
struct PutRequest {
    IORequest_Type type;
    union {
        char value;
        char* str;
        Buffer* buf;
    };
};  
typedef struct PutRequest PutRequest;

struct PutReply {
    ErrorCode error;
};  
typedef struct PutReply PutReply;

/*
 * Get Request Structures
 */
struct GetRequest {
    IORequest_Type type;
    union {
        char value; // from notify
        char* buf; // expects pointer to size 20!!
    };
};  
typedef struct GetRequest GetRequest;

struct BufferedGetRequest {
    char buffer;
    int index; // from hwi handler
};  
typedef struct BufferedGetRequest BufferedGetRequest;

struct GetReply {
    ErrorCode error;
    char value;
};  
typedef struct GetReply GetReply;

ErrorCode getc(TaskId source);
ErrorCode putc(TaskId sink, char c);

ErrorCode putS(TaskId sink, char* str);
ErrorCode putD(TaskId sink, int d);
ErrorCode putB(TaskId sink, Buffer *b);
ErrorCode putB_Blocking(TaskId sink, Buffer* buf);
