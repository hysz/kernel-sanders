#include "buffer.h"
#include "ts7200.h"
#include "kapi.h"
#include "nameserver_api.h"
#include "names.h"
#include "common.h"
#include "buffer.h"
#include "io.h"
#include "event.h"
#include "debug.h"
#include "io_impl.h"
#include "kapi.h"

extern void IoPutDaemon(int*, TaskId);
extern void IoPutNotifierDaemon(TaskId, EventType);
extern void IoGetDaemon(int*);
extern void IoGetNotifierDaemon(TaskId, EventType);

void IoNotifier()
{
    // Get Initialization Message From Parent
    const TaskId server_tid = MyParentTid(); 
    TaskId init_tid = 0;
    IoInitMsg init_msg;
    Receive(&init_tid, &init_msg, sizeof(init_msg));
    if(init_tid != server_tid) {
        ERROR("IO Initialization Message Not From Parent: %d (parent=%d)", init_tid, server_tid);
        Exit();
    }
    Reply(server_tid, 0, 0);

    // Spawn Notifier Daemon
    switch(init_msg.server_type) { 
    case IO_PUTTER:
        IoPutNotifierDaemon(server_tid, init_msg.notifier_event);
        break;

    case IO_GETTER:
        IoGetNotifierDaemon(server_tid, init_msg.notifier_event);
        break;

    default:
        ERROR("Unexpected Io Notifier Type: %d", init_msg.server_type);
        Exit();
        break;
    }
}


void IoServer()
{
    // Get Initialization Message From Parent
    TaskId init_tid = 0;
    IoInitMsg init_msg;
    Receive(&init_tid, &init_msg, sizeof(init_msg));
    if(init_tid != MyParentTid()) {
        ERROR("IO Initialization Message Not From Parent: %d (parent=%d)", init_tid, MyParentTid());
        Exit();
    }
    Reply(init_tid, 0, 0);

    // Make Available to Name Server
    RegisterAs(init_msg.name);

    // Create Notifier
    TaskId notifier_tid = notifier_tid = INVALID_TID;
    if(init_msg.address == trainPutAddress() || init_msg.address == trainGetAddress()) {
        notifier_tid = Create(TRACK_IO_NOTIFIER_PRIORITY, &IoNotifier);
    } else {
        notifier_tid = Create(TERMINAL_IO_NOTIFER_PRIORITY, &IoNotifier);
    }
    if(! isValidTaskId(notifier_tid) ) {
        ERROR("Unexpected Notifier Tid: %d", notifier_tid);
        Exit();
    }

    // Forward initialization message to notifier
    ErrorCode error = Send(notifier_tid, &init_msg, sizeof(init_msg), 0, 0);
    if(error) {
        ERROR("Failed to send initialization message to Notifier Task: %d", error);
        Exit();
    }

    // Start IO Daemon
    switch(init_msg.server_type) {
    case IO_PUTTER:
        IoPutDaemon(init_msg.address, notifier_tid);
        break;

    case IO_GETTER:
        IoGetDaemon(init_msg.address);
        break;

    default:
        ERROR("Unexpected Io Server Type: %d", init_msg.server_type);
        Exit();
        break;
    }

    Exit();
}

int* termPutAddress()
{
    return (int *)(UART2_BASE + UART_DATA_OFFSET);
}

int* termGetAddress()
{
    return (int *)(UART2_BASE + UART_DATA_OFFSET);
}

int* trainPutAddress()
{
    return (int *)( UART1_BASE + UART_DATA_OFFSET );
}

int* trainGetAddress()
{
    return (int *)( UART1_BASE + UART_DATA_OFFSET );
}
