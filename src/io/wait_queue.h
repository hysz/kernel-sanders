#pragma once

#define WAIT_QUEUE_SIZE 20

#include "buffer.h"
#include "errno.h"
#include "task.h"

struct WaitQueue {
    int data[WAIT_QUEUE_SIZE][2];
    int next; 
    int len;
};
typedef struct WaitQueue WaitQueue;

void waitQueueReset(WaitQueue *q);
ErrorCode waitQueuePush(WaitQueue* q, int d1, int d2);
void waitQueuePrint(TaskId channel, WaitQueue* q);
int waitQueueSend(WaitQueue* q);
int waitQueuePop(WaitQueue* q);
