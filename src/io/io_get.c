#include "ts7200.h"
#include "kapi.h"
#include "nameserver_api.h"
#include "names.h"
#include "common.h"
#include "buffer.h"
#include "io.h"
#include "event.h"
#include "debug.h"

void IoGetNotifierDaemon(TaskId server_tid, EventType listen_event)
{
    // Run    
    char buf[20];
    DEBUG("buf addr: %x", buf);
    FOREVER {
        ErrorCode err = AwaitEvent(listen_event, buf, 20); 
        if (err) {
            DEBUG("\n\rAwaitEvent returned error: %d", err);
        }

        GetRequest request;
        request.type = NOTIFY;
        request.buf = buf;

        GetReply reply;
        Send(server_tid, &request, sizeof(request), &reply, sizeof(GetReply));
    };
}

void IoGetDaemon(int *address)
{
    // Handle Requests
    const int request_len = sizeof(GetRequest);
    const int reply_len = sizeof(GetReply);

    Buffer io_buffer;
    bufferReset(IO_GET_DAEMON, &io_buffer);
    
    Buffer getCQueue;
    bufferReset(IO_GET_DAEMON_CQUEUE, &getCQueue);

    FOREVER {
        // Get Request
        TaskId tid = INVALID_TID; 
        GetRequest request;
        Receive(&tid, &request, request_len);

        // Process Request
        GetReply reply;
        reply.error = NO_ERROR;
        switch(request.type) {
        case NOTIFY:
            {
                int i = 0;
                if(request.buf[16] > 16) {
                    ERROR("IoGetDaemon: Length from request buffer exceeds fifo size of 16");
                    reply.error = BAD_BUF_SIZE; 
                } else {
                    for(; i < request.buf[16]; ++i) { // hacky, but 16 holds number of chars to read from buf: context_switch.c
                        bufferPut(&io_buffer, request.buf[i]); 
                    }
                }

                Reply(tid, &reply, reply_len);
            }
            break;
    
        case GET:
            if (bufferPut(&getCQueue, tid)) {
                ERROR("Cannot add tid to get c queue (full)");
                reply.error = GET_C_QUEUE_FULL_ERROR;
            }
            break;
    
        default:
            reply.error = ERR_BAD_REQUEST_TYPE;
            Reply(tid, &reply, reply_len);
            break; 
        }   
            
        if (!bufferEmpty(&io_buffer) && !bufferEmpty(&getCQueue)) {
            GetReply getCReply;
            TaskId getCWaiter = bufferPop(&getCQueue);
            getCReply.value = bufferPop(&io_buffer);
            getCReply.error = NO_ERROR;

            Reply(getCWaiter, &getCReply, reply_len); 
        }
    }   
}
