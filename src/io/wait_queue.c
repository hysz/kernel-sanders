#include "wait_queue.h"
#include "buffer.h"
#include "debug.h"
#include "errno.h"
#include "io.h"

void waitQueueReset(WaitQueue *q)
{
    q->next = 0;
    q->len = 0;
}

ErrorCode waitQueuePush(WaitQueue* q, int d1, int d2) 
{
    q->data[q->next][0] = d1; 
    q->data[q->next][1] = d2; 

    q->next = (q->next + 1) % WAIT_QUEUE_SIZE;
    if(q->len < WAIT_QUEUE_SIZE) q->len++;
    else return ERR_PUSH_FULL_WAITERS_QUEUE;

    return NO_ERROR;
}

void waitQueuePrint(TaskId channel, WaitQueue* q)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    int j = 4;  // line number to print on
    int i = 0;

    for(i = 0; i < q->len; ++i) {
        // Add text
        int k = (q->next - 1) - i;
        if(k < 0) k += WAIT_QUEUE_SIZE;

    bufferPutStr(&b, "\033[");
    bufferPutInt(&b, j); 
    bufferPutStr(&b, ";7H");

    // Clear line up to where we want to add

        bufferPutInt(&b,  k);
        bufferPut(&b,  ' ');
        bufferPutInt(&b,  q->data[k][0]); // A,B,C,...
        bufferPut(&b, ' ');
        if(q->data[k][1] < 10) {
            bufferPut(&b,    '0');
        }   
        bufferPutInt(&b, q->data[k][1]); // 1..16
        bufferPut(&b,    ' ');

        ++j;
    }   

    putB(channel, &b);
}

int waitQueueSend(WaitQueue* q)
{
    if(q->len <= 0) return ERR_SEND_EMPTY_WAITERS_QUEUE;
    
    int k = q->next - q->len;
    if(k < 0) k += WAIT_QUEUE_SIZE;

    q->data[k][1]--;
    if(q->data[k][1] < 0) {
        ERROR("WaitQueue: Too Many Sends! k=%d %d %d", k, q->data[k][0], q->data[k][1]);
    }

    return q->data[k][1];
}

int waitQueuePop(WaitQueue* q)
{
    if(q->len <= 0) return ERR_POP_EMPTY_WAITERS_QUEUE;
    
    int k = q->next - q->len;
    if(k < 0) k += WAIT_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;
    return q->data[k][0];
}
