#include "io.h"
#include "task.h"
#include "kapi.h"
#include "debug.h"

ErrorCode getc(TaskId source)
{
    if(! isValidTaskId(source) ) {
        return ERR_INVALID_TID;
    } 

    GetRequest req;
    req.type = GET;
    GetReply reply;
    ErrorCode err;
    if( ( err = Send(source, &req, sizeof(req), &reply, sizeof(reply) ) ) ) {
        return err;
    } else if(reply.error) {
        DEBUG("\n\rGOT REPLY ERROR %d", reply.error);
        return reply.error;
    }

    return reply.value;
}

ErrorCode putc(TaskId sink, char c)
{
    if(! isValidTaskId(sink) ) {
        return ERR_INVALID_TID;
    } 

    PutRequest req;
    req.type = PUT_C;
    req.value = c;

    PutReply reply;
    ErrorCode err;
    if( ( err = Send(sink, &req, sizeof(req), &reply, sizeof(reply) ) ) ) {
        return err;
    }

    return reply.error;
}

ErrorCode putS(TaskId sink, char* str)
{
    if(! isValidTaskId(sink) ) {
        return ERR_INVALID_TID;
    } 

    PutRequest req;
    req.type = PUT_S;
    req.str = str;

    PutReply reply;
    ErrorCode err;
    if( ( err = Send(sink, &req, sizeof(req), &reply, sizeof(reply) ) ) ) {
        return err;
    }

    return reply.error;
}

ErrorCode putD(TaskId sink, int d)
{
    char num[10];
    int i = 0;
    for(; d > 0; d /= 10) {
        num[i++] = '0' + (char)(d % 10);
    } 
    i -= 1;

    ErrorCode error = NO_ERROR;
    while(i >= 0) {
        if( ( error = putc(sink, num[i--]) ) ) break;
    }

    return error;
}

ErrorCode putB(TaskId sink, Buffer* buf)
{
    if(! isValidTaskId(sink) ) {
        return ERR_INVALID_TID;
    } 

    PutRequest req;
    req.type = PUT_B;
    req.buf = buf;

    PutReply reply;
    ErrorCode err;
    if( ( err = Send(sink, &req, sizeof(req), &reply, sizeof(reply) ) ) ) {
        return err;
    }

    return reply.error;
}

ErrorCode putB_Blocking(TaskId sink, Buffer* buf)
{
    if(! isValidTaskId(sink) ) {
        return ERR_INVALID_TID;
    } 

    PutRequest req;
    req.type = PUT_B;
    req.buf = buf;

    PutReply reply;
    ErrorCode err;
    if( ( err = Send(sink, &req, sizeof(req), &reply, sizeof(reply) ) ) ) {
        return err;
    }

    return reply.error;
}
