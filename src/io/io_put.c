#include "buffer.h"
#include "ts7200.h"
#include "kapi.h"
#include "nameserver_api.h"
#include "names.h"
#include "common.h"
#include "buffer.h"
#include "io.h"
#include "event.h"
#include "debug.h"
#include "clockserver_api.h"
#include "io_impl.h"
#include "big_buffer.h"
#include "wait_queue.h"

void IoPutNotifierDaemon(TaskId server_tid, EventType listen_event, int* address)
{
    // Run    
    PutRequest request;
    request.type = NOTIFY;

    FOREVER {
        TaskId tid;
        Receive(&tid, 0, 0);
        if(tid != server_tid) {
            // IOERROR("\r\nUnexpected server tid: %d != %d", tid, server_tid);
        }
        Reply(tid, 0, 0);

        ErrorCode err = AwaitEvent(listen_event, 0, 0); 
        if (err) {
            IODEBUG("\n\rAwaitEvent returned error: %d", err);
        }   

        if(listen_event == TRAIN_PUT_READY) {
            // Delay an extra 10th of a second
            //Delay(10);
        }

        PutReply reply;
        Send(server_tid, &request, sizeof(request), &reply, sizeof(reply));
        if(reply.error) {
            // IOERROR("\r\nIoPutNotifier Got Error From IoPutDaemon: %d", reply.error); 
        }   
    };  
}

void IoPutDaemon(int *put_address, TaskId notifier_tid)
{
    // Handle Requests
    const int request_len = sizeof(PutRequest);
    const int reply_len = sizeof(PutReply);
    typedef enum {READY, BUSY} TermOut_State;
    
    BigBuffer io_buffer;
    BigBufferReset(&io_buffer);

    WaitQueue waiters;
    waitQueueReset(&waiters);

    TermOut_State state = BUSY;
    Send(notifier_tid, 0, 0, 0, 0);

    FOREVER {
        // Get Request
        TaskId tid = INVALID_TID; 
        PutRequest request;
        Receive(&tid, &request, request_len);


        // Process Request
        PutReply reply;
        Bool block = FALSE;
        switch(request.type) {
        case NOTIFY:
            state = READY;
            reply.error = NO_ERROR;
            break;
    
        case PUT_C:
            if(put_address == termPutAddress()) {
                reply.error = BigBufferPut(&io_buffer, request.value);
            } else {
                reply.error = waitQueuePush(&waiters, tid, 1);
                if(reply.error) break;

                reply.error = BigBufferPut(&io_buffer, request.value);
                block = (reply.error == NO_ERROR);
                //waitQueuePrint(6, &waiters);
            }
            break;

        case PUT_S:
            if(put_address == termPutAddress()) {
                reply.error = BigBufferPutStr(&io_buffer, request.str);
            } else {
                reply.error = waitQueuePush(&waiters, tid, 1);
                if(reply.error) break;

                reply.error = BigBufferPutStr(&io_buffer, request.str);
                block = (reply.error == NO_ERROR);
                //waitQueuePrint(6, &waiters);
            }
            break;

        case PUT_B:
            if(put_address == termPutAddress()) {
                reply.error = BigBufferCopy(&io_buffer, request.buf);
            } else {
                reply.error = waitQueuePush(&waiters, tid, request.buf->size);
                if(reply.error) break;

                reply.error = BigBufferCopy(&io_buffer, request.buf);    
                block = (reply.error == NO_ERROR);

                //waitQueuePrint(6, &waiters);
            }
            break;

        default:
            reply.error = ERR_BAD_REQUEST_TYPE;
            break; 
        }   
        if(reply.error) {
            // IOERROR("IoPutDaemon: %d", reply.error);
        }

       // if(! block) {
            Reply(tid, &reply, reply_len);
       // }
        if(put_address != termPutAddress()) continue;

        if(state == READY && ! BigBufferEmpty(&io_buffer)) {
            int i = 0;
            // Send 8 bytes at a time to the terminal
            // Send 1 byte at a time to the train
            // This is because the train is a bottleneck and we need to
            // wait between sending bytes. O/w the train will miss some commands
            const int BUF_SIZE = (put_address == termPutAddress()) ? 8 : 1; // 8 when FIFOs enabled
            while(i < BUF_SIZE && ! BigBufferEmpty(&io_buffer)) {
                char next = BigBufferPeek(&io_buffer);
                if(put_address == termPutAddress() && i > 0 && next == '\033') {
                    // Escape code is at most 8 bytes and must be sent all at once
                    break;
                }
                cpyN(put_address, &next, 1);

                BigBufferPop(&io_buffer);
                ++i;

                if(waiters.len > 0) {
                    int num_remaining_sends = waitQueueSend(&waiters);
                    if(num_remaining_sends > 0) continue;
                    Reply(waitQueuePop(&waiters), &reply, reply_len);
                    //waitQueuePrint(6, &waiters);
                }
            }

            state = BUSY;
            Send(notifier_tid, 0, 0, 0, 0);
        }
    }   
}
