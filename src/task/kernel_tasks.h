#pragma once

#define BOOTSTRAP_TID 1

// Sanity Test
// #define FIRST_USER_TASK &idleTask

// ClockServer Test
// #include "clockserver_test.h"
// #define FIRST_USER_TASK &k3ClockTest

// TermOut Test
////#include "io_test.h"
//#define FIRST_USER_TASK &fifoTermTestEsc

// TermOut Test
//#include "termout_test.h"
//#define FIRST_USER_TASK &simpleTest
//#define FIRST_USER_TASK &complexTest

//#include "termin_test.h"
//#define FIRST_USER_TASK &terminTest

// Train Controller
#include "controller.h"
#define FIRST_USER_TASK &Controller

// HWI Controller 
//#include "hwi_test.h"
//#define FIRST_USER_TASK &HwiTest

// HWI Controller 
//#include "hwi_test.h"
//#define FIRST_USER_TASK &HwiTest

//Sensors
//#include "sensors.h"
//#define FIRST_USER_TASK &Sensors

// First task to run on the system
// Bootstraps all other tasks
void bootstrapTask();
