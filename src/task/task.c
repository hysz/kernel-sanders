#include "task.h"
#include "common.h"

void resetTask(Task *task, TaskId tid, TaskPriority priority, TaskId parent_tid)
{
    task->id = tid;

    // Special Registers
    task->sp = (int)(task->stack + DEFAULT_STACK_SIZE - 1);
    task->pc = 0;

    task->cpsr = 16; // user mode
    task->parent = parent_tid;
    task->state = READY;
    task->kapi_args = (int*)-1;
    
    // Priority Queue Stuff
    task->priority = priority;
    task->next = INVALID_TID;

    task->retval = -5;

    //Message Queue Stuff
    task->rcv_msg_front = 0;
    task->rcv_queue_size = 0;
}

int isValidPriority(TaskPriority priority)
{
    if(priority < MIN_TASK_PRIORITY || priority > MAX_TASK_PRIORITY) return FALSE;
    return TRUE; 
}

int isValidTaskId(TaskId tid)
{
    if((tid < MIN_TASK_ID || tid > MAX_TASK_ID) && tid != KERNEL_PARENT_TASK_ID) return FALSE;
    return TRUE;
}
