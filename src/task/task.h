#pragma once

#define MIN_TASK_ID 1
#define MAX_TASK_ID 80
#define INVALID_TID 100
#define KERNEL_PARENT_TASK_ID 6969
#define MAX_TASK_COUNT ((MAX_TASK_ID) - (MIN_TASK_ID) + 1)

#define MIN_TASK_PRIORITY 1
#define MAX_TASK_PRIORITY 10

/*
#define HI_PRIORITY 1
#define MED_PRIORITY 5
#define LOW_PRIORITY 9
*/

#define BOOTSTRAP_PRIORITY              1
#define CLOCK_NOTIFIER_PRIORITY         1 
#define TRACK_IO_NOTIFIER_PRIORITY      2 
#define TRACK_SERVERS_PRIORITY          3
#define CLOCK_SERVER_PRIORITY           4
#define TRAIN_DRIVER_PRIORITY           5
#define TRAIN_HELPER_HI_PRIORITY        6
#define TRAIN_NEGOTIATOR_PRIORITY       7
#define TERMINAL_IO_NOTIFER_PRIORITY    8
#define TRAIN_HELPER_LO_PRIORITY        8
#define GENERAL_SERVER_PRIORITY         9
#define IDLE_PRIORITY                   10 

/*
#define NOTIFIER_PRIORITY 1
#define KERNEL_SERVER_PRIORITY 2
#define USER_SERVER_PRIORITY 3
#define USER_HI_PRIORITY 4
#define USER_MED_PRIORITY 5
#define USER_LO_PRIORITY 6
#define IDLE_PRIORITY 10
*/

#define NUM_PRIORITIES ((MAX_TASK_PRIORITY) - (MIN_TASK_PRIORITY) + 1)

#define MAX_RECEIVE_QUEUE_SIZE 100
#define MAX_NUM_BLOCKED_STATE_ARGS 3

// 512 words of memory
#define DEFAULT_STACK_SIZE 65536

typedef int TaskId;
typedef int Register;
typedef int TaskPriority;
typedef enum {
                ACTIVE,
                READY, 
                SEND_BLOCKED, 
                SEND_UNBLOCKED, 
                RECEIVE_BLOCKED, 
                REPLY_BLOCKED, 
                EVENT_BLOCKED, 
                CTS_BLOCKED,
                ZOMBIE, 
                UNINITIALIZED
            } TaskState;

struct Task {
    // Return value of last call
    int                 retval; 

    // Task Descriptor
    TaskId              id;                     // id (this is how it's referenced in TaskSet
    Register            sp;                    // Stack Pointer   - pointers into <registers>
    Register            pc;                    // Program Counter - points into <registers>
    Register            cpsr;                   // value of current processor status register
    TaskId              parent;                 // parent task
    TaskState           state;                  // Active, Ready, or Blocked

    int *               kapi_args;              // Pointer on stack for kapi args

    // Priority Queue Stuff
    TaskPriority        priority;               // priority of task
    TaskId              next;

    // Additional Data
    int                    stack[DEFAULT_STACK_SIZE];

    //Receive queue
    int                    rcv_msg_indices[MAX_RECEIVE_QUEUE_SIZE];
    int                    rcv_msg_front;
    int                    rcv_queue_size;    
};
typedef struct Task Task;

void resetTask(Task *td, TaskId tid, TaskPriority priority, TaskId parent_tid);
int isValidPriority(TaskPriority priority);
int isValidTaskId(TaskId tid);
