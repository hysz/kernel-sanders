#pragma once

#include "task.h"
#include "errno.h"

/*
 * Description: Holds set of task descriptors and a priority queue for which task to run next.
 */
struct TaskSet
{
    // A maximum of ***99*** tasks. Valid ids from [1..99]. 
    // This is to protect us from accidentally dereferencing NULL (=0) 
    // as an id. 
    Task data[MAX_TASK_COUNT + 1];

    // A queue of tasks
    TaskId ready_queue_top[NUM_PRIORITIES + 1];
    TaskId ready_queue_bot[NUM_PRIORITIES + 1];

    // Priority Tracker
    unsigned int active_priorities; 
    
};
typedef struct TaskSet TaskSet;

// Resets TaskSet to default values
void resetTaskSet(TaskSet *tasks);

// Adds task <tid> to priority queue
ErrorCode enqueueTask(TaskId tid, int priority, TaskSet *tasks);

/*
 * Description: Creates task; adds to TaskSet
 * Parameters: 
 *              <priority>: Priority of task in range [1..9] 
 *              <code>    : Function pointer to task code
 *              <tasks>   : Set of currently running tasks 
 * Returns:
 *              Task ID int range [MIN_TASK_ID..MAX_TASK_ID]
 *              Error otherwise
 */
int createTask(int priority, void (*code) (), TaskSet *tasks, TaskId parent);

// Deletes Task
ErrorCode deleteTask(TaskId tid, TaskSet *tasks);

// Returns TRUE only if <tid> is *not* ZOMBIE/UNINITIALIZED
int isTaskAlive(TaskId tid, TaskSet *tasks);

/*
 * Description: Returns pointer to next task in set 
 * Returns:
 *              Task Id of next task
 *              Error otherwise
 */
int getNextTask(TaskSet *tasks, Task **td);

/*
 * Description: Gets task descriptor for <tid>
 * Returns:
 *              Task Descriptor for <tid>
 *              -9 if <tid> is bad task id
 */
int getTask(TaskSet *tasks, TaskId tid, Task **td);
