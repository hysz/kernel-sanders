#include "kernel_tasks.h"
#include "debug.h"
#include "common.h"
#include "task.h"
#include "kapi.h"
#include "nameserver.h"
#include "clockserver.h"
#include "io_impl.h"
#include "event.h"
#include "track_server.h"
#include "bwio.h"

static void idleTask()
{
    FOREVER { /* DEBUG LINE: DEBUG("."); */}
}

static void createIdleTask()
{
    // Creates IDLE Task
    KDEBUG("Creating Idle Task");
    TaskId idle_tid = Create(IDLE_PRIORITY, &idleTask); 
    if(! isValidTaskId(idle_tid) ) {
        KERROR("KERROR: Idle Task created with unexpected Task Id: %d", idle_tid);
        Exit();
    }
    KDEBUG("DONE CREATEIDLE TASK");
}

static void createNameserver()
{
    KDEBUG("Creating Nameserver");
    TaskId nameserver_tid = Create(GENERAL_SERVER_PRIORITY, &nameserverTask); 
    if(nameserver_tid != NAMESERVER_TID) {
        KERROR("NameServer task created with unexpected Task Id: %d (expected %d)", nameserver_tid, NAMESERVER_TID);
        Exit();
    }
}

static void createClockServer()
{
    // Creates Clock Server
    KDEBUG("Creating ClockServer");
    TaskId clockserver_tid = Create(CLOCK_SERVER_PRIORITY, &ClockServerTask);
    if(! isValidTaskId(clockserver_tid) ) {
        KERROR("KERROR: ClockServer Task created with unexpected Task Id: %d", clockserver_tid);
        Exit();
    }
}

static void createTermPutServer()
{
    KDEBUG("Creating Term Put IO Server");

    // Create Server Task
    TaskId server_tid = Create(GENERAL_SERVER_PRIORITY, &IoServer);
    if(! isValidTaskId(server_tid) ) {
        KERROR("KERROR: IO Task created with unexpected Task Id: %d", server_tid);
        Exit();
    } 

    //KERROR("\r\n$$$$=%d", server_tid);
    
    // Send Initialization Message
    IoInitMsg msg;
    msg.server_type = IO_PUTTER; 
    msg.name = TERM_PUT_SERVER;
    msg.address = termPutAddress();
    msg.notifier_event = TERM_PUT_READY;
    ErrorCode error = Send(server_tid, &msg, sizeof(msg), 0, 0);
    if(error) {
        KERROR("KERROR: Failed to send initialization message to IO Task: %d", error);
        Exit();
    }
}

static void createTermGetServer()
{
    //DEBUG("Creating Term Get IO Server");

    // Create Server Task
    TaskId server_tid = Create(GENERAL_SERVER_PRIORITY, &IoServer);
    if(! isValidTaskId(server_tid) ) {
        KERROR("KERROR: IO Task created with unexpected Task Id: %d", server_tid);
        Exit();
    } 
    
    // Send Initialization Message
    IoInitMsg msg;
    msg.server_type = IO_GETTER; 
    msg.name = TERM_GET_SERVER;
    msg.address = termGetAddress();
    msg.notifier_event = TERM_GET_READY;
    ErrorCode error = Send(server_tid, &msg, sizeof(msg), 0, 0);
    if(error) {
        KERROR("KERROR: Failed to send initialization message to IO Task: %d", error);
        Exit();
    }
}

static void createTrainPutServer()
{
    KDEBUG("Creating Train Put IO Server");

    // Create Server Task
    TaskId server_tid = Create(TRACK_SERVERS_PRIORITY, &IoServer);
    if(! isValidTaskId(server_tid) ) {
        KERROR("KERROR: IO Task created with unexpected Task Id: %d", server_tid);
        Exit();
    } 
    
    // Send Initialization Message
    IoInitMsg msg;
    msg.server_type = IO_PUTTER; 
    msg.name = TRAIN_PUT_SERVER;
    msg.address = trainPutAddress();
    msg.notifier_event = TRAIN_PUT_READY;
    ErrorCode error = Send(server_tid, &msg, sizeof(msg), 0, 0);
    if(error) {
        KERROR("KERROR: Failed to send initialization message to IO Task: %d", error);
        Exit();
    }
}

static void createTrainGetServer()
{
    KDEBUG("Creating Train Get IO Server");

    // Create Server Task
    TaskId server_tid = Create(TRACK_SERVERS_PRIORITY, &IoServer);
    if(! isValidTaskId(server_tid) ) {
        KERROR("KERROR: IO Task created with unexpected Task Id: %d", server_tid);
        Exit();
    } 
    
    // Send Initialization Message
    IoInitMsg msg;
    msg.server_type = IO_GETTER; 
    msg.name = TRAIN_GET_SERVER;
    msg.address = trainGetAddress();
    msg.notifier_event = TRAIN_GET_READY;
    ErrorCode error = Send(server_tid, &msg, sizeof(msg), 0, 0);
    if(error) {
        KERROR("KERROR: Failed to send initialization message to IO Task: %d", error);
        Exit();
    }
}

static void createIoServers()
{
    createTermPutServer();
    createTrainPutServer();
    createTermGetServer();
    createTrainGetServer();
}

static void createTrackServer(const track_node* track)
{
    // Create Server Task
    TaskId server_tid = Create(TRAIN_HELPER_LO_PRIORITY, &TrackServer);
    if(! isValidTaskId(server_tid) ) {
        KERROR("KERROR: IO Task created with unexpected Task Id: %d", server_tid);
        Exit();
    } 
    
    int k = (int)track;
    ErrorCode error = Send(server_tid, &k, sizeof(k), 0, 0);
    if(error) {
        KERROR("Failed to send initialization message to Track Server: %d", error);
        Exit();
    } 
}

void bootstrapTask()
{
    // Receive track ptr from parent
    int ktid = 0;
    const track_node* track;
    ErrorCode init_error = Receive(&ktid, &track, sizeof(track));
    if(init_error) {
        KERROR("createTrackServer: Failed to get track ptr from kernel (init_error=%d)", init_error);
    }

    // Kernel Initialization
    KDEBUG("BOOTING");
    bwprintf(1, "BOOTIN");
    createIdleTask();
    createNameserver();
    createClockServer();
    createIoServers();
    createTrackServer(track);

    // Init Task
    bwprintf(1, "Runnin user prog");
    //KDEBUG("Running User Program");
    ErrorCode err = Create(GENERAL_SERVER_PRIORITY, FIRST_USER_TASK);
    if (err < 0) {
        KERROR("Failed to run User Program: %d", err);
        Exit();
    }

    //KDEBUG("Bootstrap Task Exiting");
    Exit();
}
