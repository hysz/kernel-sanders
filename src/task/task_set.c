#include "task_set.h"
#include "common.h"
#include "task.h"
#include "debug.h"
#include "errno.h"

void resetTaskSet(TaskSet *tasks)
{
   int i = MIN_TASK_PRIORITY;
    for(; i <= MAX_TASK_PRIORITY; ++i) {
        tasks->ready_queue_top[i] = INVALID_TID;
        tasks->ready_queue_bot[i] = INVALID_TID;
    }

    for(i = MIN_TASK_ID; i <= MAX_TASK_ID; ++i) {
        tasks->data[i].state = UNINITIALIZED;
    }

    tasks->active_priorities = 0;
}

ErrorCode enqueueTask(TaskId tid, int priority, TaskSet *tasks)
{
    // DEBUG("Enqueue %d with priority %d", tid, priority);
    if(! isValidTaskId(tid) ) return ERR_INVALID_TID;
    if(! isValidPriority(priority) ) return ERR_INVALID_PRIORITY;

    tasks->data[tid].next = INVALID_TID;
    tasks->data[tid].state = READY;

    if(tasks->ready_queue_top[priority] == INVALID_TID && tasks->ready_queue_bot[priority] == INVALID_TID) {
        // queue is empty
        tasks->ready_queue_top[priority] = tid;
        tasks->ready_queue_bot[priority] = tid;

        tasks->active_priorities |= (1 << priority);

        return NO_ERROR;
    } 
    if(! ( isValidTaskId(tasks->ready_queue_top[priority]) && isValidTaskId(tasks->ready_queue_bot[priority]))) {
        ERROR("Failed to enqueue task. Top or Bottom of Queue is Invalid");
        return ERR_CORRUPTED_QUEUE;
    } 

    tasks->data[tasks->ready_queue_bot[priority]].next = tid;
    tasks->ready_queue_bot[priority] = tid;

    tasks->active_priorities |= (1 << priority);

    return NO_ERROR;
}

int createTask(int priority, void (*code) (), TaskSet *tasks, TaskId parent)
{
    if(! isValidPriority(priority) )    return ERR_INVALID_PRIORITY;
    if(! code )                         return ERR_INVALID_TASK_PTR;
    if(! isValidTaskId(parent) )        return ERR_INVALID_PARENT_TID;

    // Get a task id
    TaskId tid = MIN_TASK_ID;
    for(; tid <= MAX_TASK_ID; ++tid) {
        if(tasks->data[tid].state == UNINITIALIZED) break;
    }
    if(tasks->data[tid].state != UNINITIALIZED) return ERR_NO_TASK_DESCRIPTORS_LEFT;

    // Reset Task Descriptor & Add Task to Ready Queue
    resetTask(&tasks->data[tid], tid, priority, parent);
    int queue_error = NO_ERROR;
    if((queue_error = enqueueTask(tid, priority, tasks)) != NO_ERROR) return queue_error;

    // Store PC
    tasks->data[tid].pc = (int)code + PROGRAM_OFFSET;

    return tid; 
}

ErrorCode deleteTask(TaskId tid, TaskSet *tasks)
{
    if(tasks->data[tid].id != tid)                return ERR_TID_MISMATCH;
    if(tasks->data[tid].state == ZOMBIE)          return ERR_QUEUE_DOUBLE_DELETE;
    if(tasks->data[tid].state != ACTIVE)          return ERR_QUEUE_DELETE_TASK_NOT_ACTIVE;

    tasks->data[tid].state = ZOMBIE;
    return NO_ERROR;
}

int isTaskAlive(TaskId tid, TaskSet *tasks)
{
    switch(tasks->data[tid].state) {
        case ZOMBIE:
        case UNINITIALIZED:
            return FALSE;
        default:
            return TRUE;
    }

    return FALSE;
}

TaskPriority lookupP(int val)
{
/*
    This doesn't work with our compiler:
    * replace with jump table

    static const int Mod37BitPosition[] =       // map a bit value mod 37 to its position
    {
      32, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4,
      7, 17, 0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5,
      20, 8, 19, 18
    };
*/

    switch(val) {
        case 0: return 32;
        case 1: return 0;
        case 2: return 1;
        case 3: return 26;
        case 4: return 2;
        case 5: return 23;
        case 6: return 27;
        case 7: return 0;
        case 8: return 3;
        case 9: return 16;
        case 10: return 24;
        case 11: return 30;
        case 12: return 28;
        case 13: return 11;
        case 14: return 0;
        case 15: return 13;
        case 16: return 4;
        case 17: return 7;
        case 18: return 17;
        case 19: return 0;
        case 20: return 25;
        case 21: return 22;
        case 22: return 31;
        case 23: return 15;
        case 24: return 29;
        case 25: return 10;
        case 26: return 12;
        case 27: return 6;
        case 28: return 0;
        case 29: return 21;
        case 30: return 14;
        case 31: return 9;
        case 32: return 5;
        case 33: return 20;
        case 34: return 8;
        case 35: return 19;
        case 36: return 18;
        default: /* Do Nothing - Catch Below */
            break;
    }

    ERROR("Failed to lookup priority - returning -1");

    return -1; 

}
int getNextTask(TaskSet *tasks, Task **td)
{
    *td = 0;

    // find the number of trailing zeros in 32-bit tasks->active_priorities
    unsigned int v = tasks->active_priorities;
    TaskPriority priority = lookupP((-v & v) % 37);                      // result goes here
    if(! isValidPriority(priority)) return ERR_QUEUE_EMPTY;

    TaskPriority test_priority = MIN_TASK_PRIORITY;
    for(;test_priority <= MAX_TASK_PRIORITY; ++test_priority) {
        if(isValidTaskId(tasks->ready_queue_top[test_priority])) break;
    }
    if(priority != test_priority) {
        ERROR("Priority != Test Priority");
    }
   
    

    //DEBUG("getNextTask with priority %d", priority);

    TaskId tid = tasks->ready_queue_top[priority];
    if(! isValidTaskId(tid) ) {
        ERROR("getNextTask: Invalid Tid at priority %d", priority);
    }   

    int get_status;
    if( (get_status = getTask(tasks, tid, td)) == tid) { 
        tasks->ready_queue_top[priority] = (*td)->next;
        (*td)->next = INVALID_TID;

        if(! isValidTaskId( tasks->ready_queue_top[priority] ) ) {
            // Queue is empty
            tasks->ready_queue_bot[priority] = INVALID_TID;
            tasks->active_priorities &= ~(1 << priority);
        }
    }

    return get_status;
}

int getTask(TaskSet *tasks, TaskId tid, Task **td)
{
    *td = 0;     

    // Assert that task we're returning is alive
    if(! isTaskAlive(tid, tasks)) return ERR_TRIED_TO_GET_DEAD_TASK;

    *td = &(tasks->data[tid]);
    return tid;
}
