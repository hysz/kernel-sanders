#include "nameserver.h"
#include "common.h"
#include "task.h"
#include "errno.h"
#include "kapi.h"
#include "debug.h"

static int isValidName(Name name)
{
    if(name < 0 || name >= NAME_COUNT) return FALSE;
    return TRUE;
}

static void registerName(NameServerReply *reply, TaskId tid, NameServerRequest const *request, TaskId *names)
{
    if(! isValidTaskId(tid) ) {
        reply->error = ERR_INVALID_TID;
        return;
    } else if( names[request->name] != INVALID_TID ) {
        reply->error = ERR_ALREADY_REGISTERED;
        return;
    }   

    names[request->name] = tid; 
    reply->error = NO_ERROR;
}

static void unregisterName(NameServerReply *reply, NameServerRequest const *request, TaskId *names)
{
    if( ! isValidName( request->name ) ) {
        reply->error = ERR_INVALID_NAME; 
        return;
    } else if( ! isValidTaskId(names[request->name]) ) {
        reply->error = ERR_UNKNOWN_NAME;
        return;
    }

    names[request->name] = INVALID_TID;
    reply->error = NO_ERROR;
}

static void lookupName(NameServerReply *reply, NameServerRequest const *request, TaskId const *names)
{
    if( ! isValidName( request->name ) ) {
        reply->error = ERR_INVALID_NAME; 
        return;
    } else if( ! isValidTaskId(names[request->name]) ) {
        reply->error = ERR_UNKNOWN_NAME;
        return;
    }

    reply->tid = names[request->name];
    reply->error = NO_ERROR;
}

void nameserverTask()
{

    // Task Initialization
    TaskId names[NAME_COUNT];
    int i = 0; 
    for(; i < NAME_COUNT; ++i) names[i] = INVALID_TID;

    const int request_len = sizeof(NameServerRequest); 
    const int reply_len = sizeof(NameServerReply);
     
    FOREVER {
        // Get Request
        TaskId tid = 0;
        NameServerRequest request;
        Receive(&tid, &request, request_len);

        // Process Request
        NameServerReply reply; 
        switch(request.type) {
        case REGISTER:
            registerName(&reply, tid, &request, names);
            break;
        
        case UNREGISTER:
            unregisterName(&reply, &request, names);
            break;
    
        case LOOKUP:
            lookupName(&reply, &request, names);
            break;

        default:
            reply.error = ERR_BAD_REQUEST_TYPE;
            break; 
        }

        // Send Reply
        Reply(tid, &reply, reply_len);
    } 
}
