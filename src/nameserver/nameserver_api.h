#pragma once

#include "nameserver.h"
#include "errno.h"

ErrorCode RegisterAs(Name name);
ErrorCode unregisterName(Name name);
TaskId WhoIs(Name name);
