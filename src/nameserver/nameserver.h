#pragma once

#include "errno.h"
#include "task.h"
#include "names.h"

#define NAME_COUNT (MAX_TASK_COUNT + 1) 
#define NAMESERVER_TID 3

typedef enum {REGISTER, UNREGISTER, LOOKUP} NameServerRequest_Type;

struct NameServerRequest {
    NameServerRequest_Type type;
    Name   name;  
};
typedef struct NameServerRequest NameServerRequest; 

struct NameServerReply {
    ErrorCode error;
    TaskId tid;  
};
typedef struct NameServerReply NameServerReply; 

void nameserverTask();
