#include "nameserver_api.h"
#include "kapi.h"

ErrorCode RegisterAs(Name name)
{
    NameServerRequest req;
    req.type = REGISTER;
    req.name = name;

    NameServerReply reply;
    Send(NAMESERVER_TID, &req, sizeof(req), &reply, sizeof(reply)); 
    if(reply.error) {
        return reply.error;
    }

    return NO_ERROR;
}

ErrorCode unregisterName(Name name)
{
    NameServerRequest req;
    req.type = UNREGISTER;
    req.name = name;

    NameServerReply reply;
    Send(NAMESERVER_TID, &req, sizeof(req), &reply, sizeof(reply)); 
    if(reply.error) {
        return reply.error;
    }

    return NO_ERROR;
} 

TaskId WhoIs(Name name)
{
    NameServerRequest req;
    req.type = LOOKUP;
    req.name = name;
    
    NameServerReply reply;
    Send(NAMESERVER_TID, &req, sizeof(req), &reply, sizeof(reply)); 
    if(reply.error) {
        return reply.error;
    }

    return reply.tid;
}
