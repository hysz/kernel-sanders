#include "buffer.h"
#include "common.h"
#include "errno.h"
#include "debug.h"

void bufferReset(BufferName name, Buffer *b)
{
    b->name = name;
    b->base = 0;
    b->next = 0;
    b->size = 0;
}

int bufferPut(Buffer* b, char c)
{
    if(b->size + 1 > BUFFER_SIZE) {
        // Would be overwriting an unread character
        return ERR_BUFFER_FULL;
    }

    b->data[b->next] = c;
    b->next = (b->next + 1) % BUFFER_SIZE;
    b->size++;

    return NO_ERROR; 
}

int bufferCopy(Buffer *dest, Buffer *src)
{
    if(dest->size + src->size > BUFFER_SIZE) {
        return ERR_BUFFER_FULL;
    }

    /*
     * When we exit, src should appear to have been untouched
     * and dest should have all of src's content appended to it
     */
    int i = 0;
    int total = src->size;
    for(; i < total; ++i) { 
        if(bufferEmpty(src)) {
            ERROR("Buffer Empty During Copy (id=%d)", src->name);
        }
        char c = bufferPeek(src);
        bufferPut(dest, c);
        //bufferPut(src, c);

        bufferPop(src);
    }

    return NO_ERROR;
}

// IMPORTANT HACK 
// ASSUMES WILL NEVER PASS IN EMPTY STRING
int bufferPutStr(Buffer* b, const char* str)
{
    // 1/2: Can we fit?
    int strlen = 0;
    const char *c = str;
    while(c == str || *c != '\0') {
        strlen++; 
        c++;
    }
    
    if( b->size + strlen > BUFFER_SIZE ) {
        // Would be overwriting an unread character
        return ERR_BUFFER_FULL;
    }

    // 2/2: Copy string to buffer
    c = str;
    while(c == str || *c != '\0') {
        b->data[b->next] = *c; 
        c++;
        b->next = (b->next + 1) % BUFFER_SIZE;
    }

    b->size += strlen;
    return NO_ERROR; 
}

int bufferPutInt(Buffer* b, int d)
{
    if(d == 0) return bufferPut(b, '0');

    int negative = d < 0;
    if(negative) d *= -1;

    // First, count
    int num_digits = 0;
    int tmp = 0;
    for(tmp = d; tmp > 0; tmp /= 10) {
        num_digits++;
    }

    // First, add character 
    if( b->size + num_digits + negative > BUFFER_SIZE) {
        // Would be overwriting an unread character
        return ERR_BUFFER_FULL;
    }

    if(negative) {
        bufferPut(b, '-');
    }

    int i = 0;
    for(tmp = d; tmp > 0; tmp /= 10) {
        b->data[b->next + num_digits - 1 - i] = (char)(((int)'0')+(tmp % 10)); 
        ++i;
    }

    b->next = (b->next + num_digits) % BUFFER_SIZE;
    b->size += num_digits;

    return NO_ERROR;
}

int bufferEmpty(Buffer* b)
{
    return (b->size == 0);
}

char bufferPeek(Buffer* b)
{
    if( bufferEmpty( b ) ) {
        // You should check bufferEmpty before calling bufferPop
        ERROR("Buffer Peek on Empty Buffer (id=%d)", b->name);
        return ' ';
    }
    
    char c = b->data[b->base]; 

    return c;
} 

char bufferPop(Buffer* b)
{
    if( bufferEmpty( b ) ) {
        // You should check bufferEmpty before calling bufferPop
        // If you start seeing '$' characters then this is why 
        ERROR("Buffer Pop on Empty Buffer (id=%d)", b->name);
        return '$';
    }

    char c = bufferPeek(b);
    b->base = (b->base + 1) % BUFFER_SIZE;
    b->size--;
    if(b->size < 0) b->size = 0;

    return c;
} 

int bufferGetInt(Buffer *b, int max_digits)
{
    int d = 0;
    int i = 0;

    while(! isWhitespace(bufferPeek(b)) && i++ < max_digits) {
        d *= 10;
        d += ( bufferPop(b) - '0' ); 
    } 

    return d;
}

void bufferGetCmd(Buffer *b, char cmd[4])
{
    int i = 0;
    while(! isWhitespace(bufferPeek(b)) && i < 4) {
        cmd[i] = bufferPop(b); 
        i++;
    }
}

int isWhitespace(char c)
{
    if(c == ' ' || c == 0x0D) {
        return TRUE;
    }

    return FALSE;
}

void bufferBackspace(Buffer *b)
{
    b->next = (b->next - 1);
    if(b->next < 0) b->next += BUFFER_SIZE;

    b->size -= 1;
}
