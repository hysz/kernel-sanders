#pragma once

#define BUFFER_SIZE 1000

typedef enum {
    IO_GET_DAEMON = 1, 
    IO_PUT_DAEMON,
    IO_GET_DAEMON_CQUEUE,
    PRINT_SENSORS,
    PRINT_SWITCH,
    UPDATE_SWITCH,
    COMMAND_BUFFER,
    COMMAND_STR,
    SET_SPEED,
    STOP_REVERSE,
    DO_REVERSE,
    PRINT_TRAIN,
    IO_GET_NOTIFIER,
    TRAIN_GET,
    TERM_GET,
    ERROR_LOG,
    ANONYMOUS
} BufferName;

struct Buffer {
    int base;
    int next; 
    int size;
    char data[BUFFER_SIZE];
    BufferName name;
};
typedef struct Buffer Buffer;

/*
 * Returns zero'd buffer
 */
// Buffer createBuffer();

void bufferReset(BufferName name, Buffer* b);

/*
 * Returns TRUE if placed in buffer
 * Returns FALSE if buffer full
 */
int bufferPut(Buffer* b, char c);

/*
 * Returns TRUE if entire array placed in buffer
 * Returns FALSE if buffer full
 */
int bufferPutStr(Buffer* b, const char* c);

int bufferPutInt(Buffer* b, int d);

/* 
 * Returns TRUE if empty
 * FALSE otherwise
 */
int bufferEmpty(Buffer* b);

/*
 * Returns next character in buffer.
 * Returns Null Byte if Empty. 
 */
char bufferPeek(Buffer* b);

/*
 * Returns next character in buffer.
 * Returns Null Byte if Empty. 
 * Also pops the character from the buffer
 */
char bufferPop(Buffer* b);

int bufferGetInt(Buffer *b, int max_digits);

void bufferGetCmd(Buffer *b, char cmd[2]);

int isWhitespace(char c);

int bufferCopy(Buffer *dest, Buffer *src);

void bufferBackspace( Buffer *src);
