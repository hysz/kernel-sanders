#pragma once

#define BIG_BUFFER_SIZE 5000

#include "buffer.h"

struct BigBuffer {
    int base;
    int next; 
    int size;
    char data[BIG_BUFFER_SIZE];
};
typedef struct BigBuffer BigBuffer;

/*
 * Returns zero'd BigBuffer
 */
// BigBuffer createBigBuffer();

void BigBufferReset(BigBuffer* b);

/*
 * Returns TRUE if placed in BigBuffer
 * Returns FALSE if BigBuffer full
 */
int BigBufferPut(BigBuffer* b, char c);

/*
 * Returns TRUE if entire array placed in BigBuffer
 * Returns FALSE if BigBuffer full
 */
int BigBufferPutStr(BigBuffer* b, char* c);

int BigBufferPutInt(BigBuffer* b, int d);

/* 
 * Returns TRUE if empty
 * FALSE otherwise
 */
int BigBufferEmpty(BigBuffer* b);

/*
 * Returns next character in BigBuffer.
 * Returns Null Byte if Empty. 
 */
char BigBufferPeek(BigBuffer* b);

/*
 * Returns next character in BigBuffer.
 * Returns Null Byte if Empty. 
 * Also pops the character from the BigBuffer
 */
char BigBufferPop(BigBuffer* b);

int BigBufferGetInt(BigBuffer *b, int max_digits);

void BigBufferGetCmd(BigBuffer *b, char cmd[2]);

int isWhitespace(char c);

int BigBufferCopy(BigBuffer *dest, Buffer *src);
