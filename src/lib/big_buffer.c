#include "big_buffer.h"
#include "common.h"
#include "errno.h"
#include "debug.h"
#include "buffer.h"

void BigBufferReset(BigBuffer *b)
{
    b->base = 0;
    b->next = 0;
    b->size = 0;
}

int BigBufferPut(BigBuffer* b, char c)
{
    if(b->size + 1 > BIG_BUFFER_SIZE) {
        // Would be overwriting an unread character
        return ERR_BUFFER_FULL;
    }

    b->data[b->next] = c;
    b->next = (b->next + 1) % BIG_BUFFER_SIZE;
    b->size++;

    return NO_ERROR; 
}

int BigBufferCopy(BigBuffer *dest, Buffer *src)
{
    if(dest->size + src->size > BIG_BUFFER_SIZE) {
        return ERR_BUFFER_FULL;
    }

    /*
     * When we exit, src should appear to have been untouched
     * and dest should have all of src's content appended to it
     */
    int i = 0;
    int total = src->size;
    for(; i < total; ++i) { 
        if(bufferEmpty(src)) {
            ERROR("BigBuffer Empty During Copy (id=%d)");
        }
        char c = bufferPop(src);
        BigBufferPut(dest, c);
    }

    return NO_ERROR;
}

// IMPORTANT HACK 
// ASSUMES WILL NEVER PASS IN EMPTY STRING
int BigBufferPutStr(BigBuffer* b, char* str)
{
    // 1/2: Can we fit?
    int strlen = 0;
    char *c = str;
    while(c == str || *c != '\0') {
        strlen++; 
        c++;
    }
    
    if( b->size + strlen > BIG_BUFFER_SIZE ) {
        // Would be overwriting an unread character
        return ERR_BUFFER_FULL;
    }

    // 2/2: Copy string to BigBuffer
    c = str;
    while(c == str || *c != '\0') {
        b->data[b->next] = *c; 
        c++;
        b->next = (b->next + 1) % BIG_BUFFER_SIZE;
    }

    b->size += strlen;
    return NO_ERROR; 
}

int BigBufferPutInt(BigBuffer* b, int d)
{
    if(d == 0) return BigBufferPut(b, '0');

    // First, count
    int num_digits = 0;
    int tmp = 0;
    for(tmp = d; tmp > 0; tmp /= 10) {
        num_digits++;
    }

    // First, add character 
    if( b->size + num_digits > BIG_BUFFER_SIZE) {
        // Would be overwriting an unread character
        return ERR_BUFFER_FULL;
    }

    int i = 0;
    for(tmp = d; tmp > 0; tmp /= 10) {
        b->data[b->next + num_digits - 1 - i] = (char)(((int)'0')+(tmp % 10)); 
        ++i;
    }

    b->next = (b->next + num_digits) % BIG_BUFFER_SIZE;
    b->size += num_digits;

    return NO_ERROR;
}

int BigBufferEmpty(BigBuffer* b)
{
    return (b->size == 0);
}

char BigBufferPeek(BigBuffer* b)
{
    if( BigBufferEmpty( b ) ) {
        // You should check BigBufferEmpty before calling BigBufferPop
        ERROR("BigBuffer Peek on Empty BigBuffer (id=%d)");
        return ' ';
    }
    
    char c = b->data[b->base]; 

    return c;
} 

char BigBufferPop(BigBuffer* b)
{
    if( BigBufferEmpty( b ) ) {
        // You should check BigBufferEmpty before calling BigBufferPop
        // If you start seeing '$' characters then this is why 
        ERROR("BigBuffer Pop on Empty BigBuffer (id=%d)");
        return '$';
    }

    char c = BigBufferPeek(b);
    b->base = (b->base + 1) % BIG_BUFFER_SIZE;
    b->size--;
    if(b->size < 0) b->size = 0;

    return c;
} 

int BigBufferGetInt(BigBuffer *b, int max_digits)
{
    int d = 0;
    int i = 0;

    while(! isWhitespace(BigBufferPeek(b)) && i++ < max_digits) {
        d *= 10;
        d += ( BigBufferPop(b) - '0' ); 
    } 

    return d;
}

void BigBufferGetCmd(BigBuffer *b, char cmd[4])
{
    int i = 0;
    while(! isWhitespace(BigBufferPeek(b)) && i < 4) {
        cmd[i] = BigBufferPop(b); 
        i++;
    }
}
