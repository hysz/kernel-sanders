#include "new_train.h"
#include "nameserver_api.h"
#include "common.h"
#include "task.h"
#include "courier.h"
#include "train_printer.h"
#include "node_queue.h"
#include "clockserver_api.h"
#include "train_physics.h"
#include "train_helpers_track.h"
#include "track_locker.h"
#include "sensorserver.h"
#include "track_locker_api.h"

static void resetState(State *state)
{
    state->track = 0;
    {
        ErrorCode error = getTrack(&state->track);
        if(error) {
            ERROR("Failed to get track: %d", (ErrorCode)error);
            Exit();
        }
    }
    state->motion = STOPPED;
    state->orientation = FORWARD;
    state->reverse = FALSE;
    state->distance_to_travel = 0;
    state->distance_travelled = 0;
    state->velocity = 0;
    state->start_time = 0;
    state->time = 0;
    state->prediction_delta = 0;

    state->last_sensor = -1;
    state->last_sensor_distance = 0;
    state->first_sensor_distance = 0;
    state->first_sensor_time = 0;

    state->final_node = -1;
    queueReset_TN(&state->nodes);
    queueReset_TN(&state->stops);

    state->velocity_at_peek = 0;
    state->distance_since_peek = 0;
    state->destination_offset = 0;

    state->next_landmark.n = -1;
    state->next_landmark.dist = 0;

    state->last_landmark_dist = 0;
    state->last_landmark = -1;

    state->distance_spent = 0;

    state->from = -1;
    state->to = -1;
}

static void resetSpeed(State *state)
{
    state->distance_travelled = 0;
    state->velocity = 0;
    state->start_time = Time();
    state->time = 0;


   // state->last_sensor_distance = -1;
    //state->first_sensor_distance = -1;
    state->first_sensor_time = -1;
    state->last_sensor = -1;

    state->first_sensor_time = 0;
    state->velocity_at_peek = 0;
    state->distance_since_peek = 0;
    state->destination_offset = 0;

    state->next_landmark.n = -1;
    state->next_landmark.dist = 0;

    state->last_landmark_dist = 0;

    state->distance_spent = 0;
}

static void setAccelerating(State *state, const Friends *friends, const Info *info)
{
    setSpeed(friends->train_out_channel, info->train_number, 10, friends->term_out_channel);

    state->motion = ACCELERATING;
    state->velocity_at_peek = state->velocity;
    state->distance_since_peek = 0;
    state->start_time = Time();
    state->time = 0;
}

static void setDecelerating(State *state, const Friends *friends, const Info *info)
{
    setSpeed(friends->train_out_channel, info->train_number, 0, friends->term_out_channel);

    state->motion = DECELERATING;
    state->velocity_at_peek = state->velocity;
    state->distance_since_peek = 0;
    state->start_time = Time();
    state->time = 0;
}

static float nextStop(State *state)
{
    if(! queueEmpty_TN(&state->stops)) {
        if(queuePeekItem_TN(&state->stops).type != Q_STOP) {
            ERROR("Next stop not a stop!");
        } else {
            return queuePeekItem_TN(&state->stops).dist;
        }
    }

    return state->distance_to_travel;
}

static void runReleaseNode(State *state, const Friends *friends, const Info *info)
{
    // ERROR("Train %d Releasing %s", info->train_number, trackNode_IndexToName(state->next_landmark.n));
    ErrorCode error = releaseNode(info->train_number,
                      state->next_landmark.n,
                      state->next_landmark.dist - state->distance_spent,
                      friends->track_locker);
    if(error) {
        ERROR("Train %d failed to release node %s", info->train_number, trackNode_IndexToName(state->next_landmark.n));
    }

    // Remove from <nodes> as we've passed the landmark
    state->last_landmark = queuePop_TN(&state->nodes);
    state->last_landmark_dist += state->next_landmark.dist;
    state->distance_spent = 0;

    // Get our next landmark!
    if(! queueEmpty_TN(&state->nodes)) {
        state->next_landmark = queuePeekItem_TN(&state->nodes);
    } else {
        state->next_landmark.n = -1;
        state->next_landmark.dist = 0;
    }
}

static Bool step(State *state, const Friends *friends, const Info *info)
{
    if(state->motion == STOPPED && state->distance_travelled >= state->distance_to_travel) return FALSE;
    int last_time = state->time;
    state->time = Delay(10) - state->start_time;
    int delay = state->time - last_time;
    
    switch(state->motion) {
    case STOPPED:
        resetSpeed(state);

        state->last_landmark_dist = 0;
        if(state->next_landmark.n < 0 && ! queueEmpty_TN(&state->nodes)) {
            state->next_landmark = queuePeekItem_TN(&state->nodes);
        }

        setAccelerating(state, friends, info);
        break;

    case ACCELERATING:
    {
        float new_distance_since_peek = getDistanceDuringAcceleration(state->time / (float)100, state->velocity_at_peek, info->train_number, 10);
        
        state->distance_travelled = (state->distance_travelled - state->distance_since_peek) + new_distance_since_peek;
        state->distance_since_peek = new_distance_since_peek;

        state->velocity = getVelocityDuringAcceleration(state->time / (float)100, state->velocity_at_peek, info->train_number, 10);

        if(state->distance_since_peek >= getStoppingDistanceFromSpeed(info->train_number, 10)) {
            state->motion = CONSTANT;
        }
    }
    break;

    case DECELERATING:
    {
        float new_distance_since_peek = getDistanceDuringDeceleration(state->time / (float)100, state->velocity_at_peek, info->train_number); 

        state->distance_travelled = (state->distance_travelled - state->distance_since_peek) + new_distance_since_peek;
        state->distance_since_peek = new_distance_since_peek;

        state->velocity = getVelocityDuringDeceleration(state->time / (float)100, state->velocity_at_peek, info->train_number);

        if((int)state->velocity == 0 /*state->distance_travelled >= state->distance_to_travel*/) {
            //ERROR("MOTION TO STOPPED!");

            if(! queueEmpty_TN(&state->stops)) {
                // Pop stop that we just reached
                queuePop_TN(&state->stops);
        
                // Set switches
                while(! queueEmpty_TN(&state->stops) && queuePeekItem_TN(&state->stops).type == Q_SWITCH) {
                    queueItem item = queuePopItem_TN(&state->stops);
                    
                    updateSwitch(friends->train_out_channel,
                                 state->track[item.n].num,
                                 item.sw_position,
                                 friends->switches_server);
                }
                
                // Reverse Train
                reverse(friends->train_out_channel, info->train_number, friends->term_out_channel); 

                // Continue on path!
                setAccelerating(state, friends, info);
            } else {
                state->destination_offset = state->distance_travelled - state->distance_to_travel;
                state->distance_travelled = state->distance_to_travel;

                state->motion = STOPPED;
            }
        } else if(! (getStoppingDistance(info->train_number, state->velocity) >= (nextStop(state) - state->distance_travelled))) {
            setAccelerating(state, friends, info);
        }
    }
    break;

    case CONSTANT:
        state->distance_travelled += state->velocity * (delay / 100.0); // delay is in centiseconds 
        break;

    default:
        // Do nothing
        break;
    }

    if(state->motion == STOPPED) return TRUE;

    // See if we've reached our next landmark!
    if(state->next_landmark.n >= 0 && state->distance_travelled - state->last_landmark_dist >= state->next_landmark.dist) {
        if(state->nodes.len == 1) {
            // Don't release if this is my last node!
            //ERROR("Train %d NOT Releasing %s", info->train_number, trackNode_IndexToName(state->next_landmark.n));
            updatePosition(info->train_number, state->next_landmark.dist - state->distance_spent, friends->track_locker);
            state->distance_spent = state->next_landmark.dist;
        } else { 
            runReleaseNode(state, friends, info); 
        }
    }

    // Update our distance; fetch more nodes if needed
    if(state->distance_spent == 0) {
        if(state->distance_to_travel - state->distance_travelled <= 1.5 * getStoppingDistanceFromSpeed(info->train_number, 10)) {
            state->distance_spent = state->distance_travelled - state->last_landmark_dist;
            updatePosition(info->train_number, state->distance_spent, friends->track_locker); 
        }
    }

    // Should we start decelerating?
    if(state->motion != DECELERATING) {
        if(getStoppingDistance(info->train_number, state->velocity) >= (nextStop(state) - state->distance_travelled)) {
            setDecelerating(state, friends, info);
        }
    }

    return TRUE;
}

static void handleRequest(TrainMsg *request, State *state, const Friends *friends, const Info *info)
{
    switch(request->type) {
    case T_RESET:
        ERROR("Resetting");
        while(state->nodes.len > 1) { // Keep final node
            runReleaseNode(state, friends, info); 
        }
        resetSpeed(state);
        state->distance_to_travel = 0;

        state->from = request->from;
        state->to = request->to;
        break;

    case T_GO:
    {
        ERROR("GO!");
        // Get Path
        // Path path;
        // getPath(state->final_node, request->destination, &path, TRUE);

        // Update Track Locker of Journey
        TrackLockerRequest track_req;
        track_req.type = START_ROUTE;
        track_req.courier = friends->my_courier;
        track_req.train = info->train_number;
        track_req.from = state->final_node;
        track_req.to =   request->destination; 

        ERROR("Sending Go Command");
        ErrorCode error = Send(friends->track_locker, &track_req, sizeof(track_req), 0, 0); 
        if(error) {
            ERROR("Failed to send route request (%s -> %s): %d",
                    trackNode_IndexToName(state->last_sensor),
                    trackNode_IndexToName(request->destination),
                    error);
        }

        ERROR("Done Sending Go Command");
/*
        //Tell Sensor Server of Epic Journey
        AttributePathMsg apm;
        apm.type = ATTRIBUTE_PATH;
        apm.destination = path.nodes[path.num_nodes - 1];
        apm.altPath = NO_ALTERNATE_PATH;
        apm.trainNum = info->train_number;
        apm.startNode = path.nodes[0];
        error = Send(friends->sensor_server, &apm, sizeof(AttributePathMsg), 0, 0);
        if(error) {
            ERROR("Failed to send path info to SS: %d", error);
        }
*/
    }   
    break;

    case T_CONTINUE:
    {
        if(state->final_node >= 0 && state->final_node == request->node) {
            TrackNode mer = queuePop_TN(&state->nodes);
            if(mer != state->final_node) {
                ERROR("Unexpected node pop: %s", trackNode_IndexToName(mer));
            }
        }

        ERROR("CONT! %s", trackNode_IndexToName(request->node));
        float dist = 0;
        if(state->final_node >= 0 && state->final_node != request->node) { // first node is always equal to <final_node> from last run.
            if(isSynonym(state->track, request->node, state->final_node)) {
                queuePushStop_TN(&state->stops, request->node, state->distance_to_travel);
                dist = 0;
            } else {
                //ERROR("Getting Edge Taken");
                Edge e = getEdgeTaken(state->track, request->node, state->final_node); 
                if(! e.edge) {
                    ERROR("Failed to get edge: %s (%d)-> %s (%d)",
                        trackNode_IndexToName(state->final_node), state->final_node, trackNode_IndexToName(request->node), request->node);
                }

                dist = e.edge->dist;

                // Flip Switches
                if(state->track[state->final_node].type == NODE_BRANCH) {
                    if(queueEmpty_TN(&state->stops)) {
                        updateSwitch(friends->train_out_channel,
                                     state->track[state->final_node].num,
                                     e.curved ? 'C' : 'S',
                                     friends->switches_server);
                    } else {
                        queuePushSwitch_TN(&state->stops, state->final_node, e.curved ? 'C' : 'S');
                    }
                }
            }

            //Tell Sensor Server if node is a SENSOR
            if (state->track[request->node].type == NODE_SENSOR) {
                //ERROR("Notiftying sensorseerver");
                SensorsRequest sr;
                sr.type = ATTRIBUTE;
                sr.sensorNum = request->node;
                sr.distance = dist;
                sr.trainNum = info->train_number;
                ErrorCode attrError = NO_ERROR;//Send(friends->sensor_server, &sr, sizeof(SensorsRequest), 0, 0);
                if (attrError) {
                    ERROR("Failed to send ATTRIBUTE msg to SENSOR SERVER: %d", attrError);
                }
                DEBUG("Told SENSOR SERVEr to attribute sensor %d", trackNode_IndexToName(request->node));
            }
        }

        queuePush_TN(&state->nodes, request->node, dist); 
        
        state->final_node = request->node;
        state->distance_to_travel += dist;

        if(state->next_landmark.n < 0) {
            state->next_landmark = queuePeekItem_TN(&state->nodes);
        }

        //ERROR("CONTINUE TO %s", trackNode_IndexToName(request->node));
    }
    break;

    case T_SENSOR:
    {
        // Correct Distance
        state->last_sensor_distance += request->sensor_info.distance;
        state->last_sensor = request->sensor_info.node;

        // Try to correct velocity
        if(state->motion == CONSTANT && state->first_sensor_time < 0) {
            state->first_sensor_distance = request->sensor_info.distance;
            state->first_sensor_time = request->sensor_info.time;
        } else if(state->motion == CONSTANT) { 
            state->distance_travelled = state->last_sensor_distance + request->sensor_info.distance; 
            state->velocity = (state->last_sensor_distance - state->first_sensor_distance) / (request->sensor_info.time - state->first_sensor_time); 
        }
    }             
    break;

    case T_FIND:
        ERROR("FIND");
        ERROR("FIND: %s", trackNode_IndexToName(request->node));
        reserveNode(info->train_number, request->node, friends->my_courier, friends->track_locker); 
        ERROR("Finished Find: %s", trackNode_IndexToName(request->node));

    break;

    default:
        ERROR("Unrecognized request type: %d", request->type);
        break;
    }
}

void NewTrain()
{
    // Initialization Message
    Info info;
    TaskId init_tid;
    ErrorCode init_error = Receive(&init_tid, &info, sizeof(info));
    if(init_error) {
        ERROR("Recv Init Message %d", init_error);
        Exit();
    }

    Friends friends;
    friends.train_out_channel = WhoIs(TRAIN_PUT_SERVER);
    if(! isValidTaskId(friends.train_out_channel)) {
        ERROR("Failed to get TRAIN_PUT_SERVER: %d", (ErrorCode)friends.train_out_channel);
        Exit();
    }

    friends.term_out_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(friends.term_out_channel)) {
        ERROR("Failed to get TERM_PUT_SERVER: %d", (ErrorCode)friends.term_out_channel);
        Exit();
    }

    friends.switches_server = WhoIs(SWITCHES_COURIER);
    if(! isValidTaskId(friends.switches_server)) {
        ERROR("Failed to get SWITCHES: %d", (ErrorCode)friends.switches_server);
        Exit();
    }

    friends.path_server = WhoIs(PATH_SERVER);
    if(! isValidTaskId(friends.path_server)) {
        ERROR("Failed to get PATH_SERVER: %d", (ErrorCode)friends.path_server);
        Exit();
    }

    friends.track_locker = WhoIs(TRACK_LOCKER);
    if(! isValidTaskId(friends.track_locker)) {
        ERROR("Failed to get TRACK_LOCKER: %d", (ErrorCode)friends.track_locker);
        Exit();
    }

    friends.sensor_server = WhoIs(SENSOR_SERVER);
    if(! isValidTaskId(friends.sensor_server)) {
        ERROR("Failed to get SENSOR_SERVER: %d", (ErrorCode)friends.sensor_server);
        Exit();
    }

    // Spawn Courier
    friends.my_courier = Create(MyPriority(), &Courier);
    if(! isValidTaskId(friends.my_courier) ) {
        ERROR("TrainInstructor Invalid TID=%d", (ErrorCode)friends.my_courier);
        Exit();
    }

    // Send Initialization Message
    volatile char* msg_peek = 0;
    {
        CourierInitMsg message;
        message.forward = MyTid();
        message.message_size = sizeof(TrainMsg);
        message.name = 0;
        message.reply_size = 0;
        message.msg_peek = &msg_peek;

        ErrorCode error = Send(friends.my_courier, &message, sizeof(message), 0, 0);
        if(error) {
            ERROR("TrainInstructor Init Failed %d", error);
            Exit();
        }
    } 
    Reply(init_tid, &friends.my_courier, sizeof(friends.my_courier));

    State state;
    resetState(&state);

    // Create Train Printer
    TaskId printer = Create(TRAIN_HELPER_LO_PRIORITY, &TrainPrinter);
    if(! isValidTaskId(printer) ) {
        ERROR("TrainInstructor Invalid TID=%d", (ErrorCode)printer);
        Exit();
    } 

    // Send Initialization Message
    {
        PrinterInit message;
        message.info = &info;
        message.state = &state;

        ErrorCode error = Send(printer, &message, sizeof(message), 0, 0);
        if(error) {
            ERROR("TrainPrinter Init Failed %d", error);
            Exit();
        }
    }

    //Send INIT message to Sensor Server
    TrainInitSSMsg tim;
    tim.type = TRAIN_INIT;
    tim.trainNum = info.train_number; 
    tim.courierTid = friends.my_courier;
    ErrorCode init_ss_error = NO_ERROR;//Send(friends.sensor_server, &tim, sizeof(TrainInitSSMsg), 0, 0);
    if (init_ss_error) {
        ERROR("Unable to INITIALIZE SENSOR SERVER: %d", init_ss_error);
    }

    Bool stepped = FALSE;
    FOREVER {
        if(! stepped || msg_peek) { // <Send> blocks. If we're stepping, then do not block. O/w we're fine to block.
            TrainMsg request;
            ErrorCode error = Send(friends.my_courier, 0, 0, &request, sizeof(request));
            if(error) {
                ERROR("Request Failed: %d", error);
            } else {
                handleRequest(&request, &state, &friends, &info);
            }
        }
        stepped = step(&state, &friends, &info);
    }
}
