#pragma once

#include "path.h"
#include "track_node.h"
#include "task.h"
#include "instruction.h"
#include "node_queue.h"

struct Friends {
    TaskId train_out_channel;
    TaskId term_out_channel;
    TaskId switches_server;
    TaskId my_courier;
    TaskId track_locker;
    TaskId path_server;
    TaskId sensor_server;
};
typedef struct Friends Friends;

struct Info {
    int train_number;
    int train_index;
};
typedef struct Info Info;

typedef enum {ACCELERATING, DECELERATING, CONSTANT, STOPPED} Motion;

struct State {
    const track_node* track;

    Motion motion;
    Orientation orientation;
    Bool reverse; 

    float last_sensor_distance;
    float first_sensor_distance;
    int first_sensor_time;
    int last_sensor;

    float distance_to_travel;
    float distance_travelled;
    float velocity;

    int time;
    int start_time;
    
    float prediction_delta;

    int final_node;       // this is the node at the end of <nodes>
    TrackNodeQueue nodes;
    TrackNodeQueue stops;

    float velocity_at_peek;
    float distance_since_peek;
    float destination_offset;

    queueItem next_landmark;
    float last_landmark_dist;
    int last_landmark;

    float distance_spent;

    int from;
    int to;
};
typedef struct State State;

typedef enum {T_GO = 11, T_CONTINUE, T_SENSOR, T_FIND, T_RESET} TrainMsgType;

struct TrainMsg {
    TrainMsgType type;
    union {
        TrackNode destination;
        TrackNode node;
        TrackNode from;
    };

    union {
        TrackNode to;
    };
};
typedef struct TrainMsg TrainMsg;

void NewTrain();
