#include "train_helpers_term.h"
#include "task.h"
#include "errno.h"
#include "buffer.h"
#include "big_buffer.h"
#include "task.h"
#include "kapi.h"
#include "debug.h"
#include "common.h"
#include "io.h"
#include "errno.h"
#include "buffer.h"
#include "names.h"
#include "nameserver_api.h"
#include "clockserver_api.h"
#include "command.h"
#include "path_finder.h"
#include "train.h"
#include "path_finder.h"
#include "track_helpers.h"
#include "path_finder_api.h"
#include "train_physics.h"
#include "path.h"
#include "instruction.h"

ErrorCode printSpeed(TaskId print_channel, int train, int speed)
{
    return NO_ERROR;
    Buffer term;
    bufferReset(SET_SPEED, &term); 
    bufferPutStr(&term, "\033[5;32H");
    bufferPutInt(&term, speed); 

    return putB(print_channel, &term); 
}

ErrorCode printPath(TaskId print_channel, Path *path)
{
    Buffer term;
    bufferReset(ANONYMOUS, &term);

    int i = 0;
    int line = 5;
    bufferPutStr(&term, "\033[4;64H");
    bufferPutInt(&term, path->distance);
    for(; i < path->num_nodes; ++i) {
        bufferPutStr(&term, "\033[");
        bufferPutInt(&term, line);
        bufferPutStr(&term, ";64H");
        bufferPutStr(&term, (char*)trackNode_IndexToName(path->nodes[i])); 
        bufferPutStr(&term, "    "); // delete any leftover characters from last print

        putB(print_channel, (Buffer*)&term); 
        ++line;
    }

    while(i++ < MAX_INSTRUCTIONS) {
        bufferPutStr(&term, "\033[");
        bufferPutInt(&term, line);
        bufferPutStr(&term, ";64H");
        bufferPutStr(&term, "       "); // delete any leftover characters from last print

        putB(print_channel, (Buffer*)&term); 
        ++line;
    }

    return NO_ERROR;
}

ErrorCode printOrientation(TaskId print_channel, int train, int speed)
{
    Buffer term;
    bufferReset(SET_SPEED, &term); 
    bufferPutStr(&term, "\033[5;32H");
    bufferPutInt(&term, speed); 

    return putB(print_channel, &term); 
}

ErrorCode printTimeError(const TaskId print_channel, const int diff)
{
    Buffer term;
    bufferReset(ANONYMOUS, &term); 
    bufferPutStr(&term, "\033[7;41H");
    if(diff < 0) {
        bufferPut(&term, '-');
        bufferPutInt(&term, -1 * diff);
    } else {
        bufferPutInt(&term, diff);
    }

    bufferPutStr(&term, "ms      ");

   return putB(print_channel, &term); 
}

ErrorCode printDirection(const TaskId print_channel, const Orientation orientation)
{
    if(orientation != FORWARD) {
        return putS(print_channel, "\033[5;36HBackward");
    }

    return putS(print_channel, "\033[5;36HForward    ");
}

ErrorCode printMeasurements(TaskId print_channel, const TrainMeasurements *m)
{
    Buffer term;
    bufferReset(ANONYMOUS, &term); 

    bufferPutStr(&term, "\033[8;25HMax Velocity: ");
    bufferPutInt(&term, m->velocity);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[9;25HMax Acceleration: ");
    bufferPutInt(&term, m->acceleration);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[10;25HDist to Accel: ");
    bufferPutInt(&term, m->dist_to_accel);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[11;25HTime to Accel: ");
    bufferPutInt(&term, m->time_to_accel);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[12;25HMax Deceleration: ");
    bufferPutInt(&term, m->decceleration);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[13;25HDist to Decel: ");
    bufferPutInt(&term, m->dist_to_deccel);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[14;25HTime to Decel: ");
    bufferPutInt(&term, m->time_to_deccel);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[15;25HFront Length: ");
    bufferPutInt(&term, m->front_length);
    bufferPutStr(&term, "    ");

    bufferPutStr(&term, "\033[16;25HBack Length: ");
    bufferPutInt(&term, m->back_length);
    bufferPutStr(&term, "    ");

   return putB(print_channel, &term); 
}

ErrorCode printPos(const TaskId print_channel, const TrackNode node)
{
    Buffer term;
    bufferReset(SET_SPEED, &term); 

    bufferPutStr(&term, "\033[6;35H");
    bufferPutStr(&term, (char*)trackNode_IndexToName(node));
    bufferPutStr(&term, "             ");

   return putB(print_channel, &term); 
}
