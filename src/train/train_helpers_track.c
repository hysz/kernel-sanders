#include "train_helpers_track.h"
#include "task.h"
#include "kapi.h"
#include "debug.h"
#include "common.h"
#include "io.h"
#include "errno.h"
#include "buffer.h"
#include "names.h"
#include "nameserver_api.h"
#include "clockserver_api.h"
#include "command.h"
#include "path_finder.h"
#include "train.h"
#include "path_finder.h"
#include "track_helpers.h"
#include "path_finder_api.h"
#include "train_physics.h"
#include "path.h"
#include "instruction.h"
#include "train_helpers_term.h"
#include "await_sensor_api.h"

 ErrorCode setSpeed(TaskId channel, int train, int speed, TaskId print_channel)
{
    Buffer b;
    bufferReset(SET_SPEED, &b);
    
    bufferPut(&b, speed);
    bufferPut(&b, train);

    ErrorCode error = putB_Blocking(channel, &b);
    if(! error) printSpeed(print_channel, train, speed);

    return error;
}

 ErrorCode reverse(TaskId channel, int train, TaskId term_out_channel)
{
    // Stop Train
    Buffer b;
    bufferReset(STOP_REVERSE, &b);
    
    // Reverse Train
    bufferPut(&b, 15);
    bufferPut(&b, train);

    return putB_Blocking(channel, &b);
}

 ErrorCode reverseAndGo(TaskId channel, int train, int speed, TaskId term_out_channel)
{
    // Stop Train
    Buffer b;
    bufferReset(STOP_REVERSE, &b);
    
    bufferPut(&b, 0); 
    bufferPut(&b, train);
   
   ErrorCode error = putB(channel, &b); 
   if(error) {
        ERROR("Failed to stop train before reversing");
    }

    // Sleep for some period of time
    Delay(3 * (SECOND_DELAY));

    bufferReset(DO_REVERSE, &b);

    // Reverse Train
    bufferPut(&b, 15);
    bufferPut(&b, train);

    // Set Train Speed 
    bufferPut(&b, speed);
    bufferPut(&b, train);

    return putB(channel, &b);
}

 ErrorCode updateSwitch(TaskId channel, int sw, char direction, TaskId switches_server_tid)
{
    enum {SOLENOID_OFF_CMD = 32, SWITCH_STRAIGHT = 33, SWITCH_CURVED = 34};

    Buffer b;
    bufferReset(UPDATE_SWITCH, &b);

    if(direction == 's' || direction == 'S') {
        bufferPut(&b, SWITCH_STRAIGHT);
    } else {
        bufferPut(&b, SWITCH_CURVED);
    }   

    bufferPut(&b, sw);
    bufferPut(&b, SOLENOID_OFF_CMD);

    putB(channel, &b);

    Command cmd;
    cmd.type = P_SWITCH;
    cmd.args[0] = sw;
    cmd.args[1] = direction;

    ErrorCode print_error = NO_ERROR;
    Send(switches_server_tid, &cmd, sizeof(cmd), &print_error, sizeof(print_error)); 
    return print_error;
}
 
ErrorCode move( const int train_number,
                Orientation *orientation,
                const TrainMeasurements *train_measurements,
                const track_node *track,
                const int current_node,
                const int destination_node,
                const TaskId train_out_channel,
                const TaskId term_out_channel,
                const TaskId switches_server)
{
/*
    Path path;
    ErrorCode error = getPath(current_node, destination_node, &path);
    if(error) {
        ERROR("move: Failed to get path (error=%d)", error);
        return error;
    }

    printPath(term_out_channel, &path);

    Instruction instructions[MAX_INSTRUCTIONS];
    int num_instructions = compileInstructions(&path, *orientation, train_measurements, track, instructions);
    if(num_instructions < 0) {
        ERROR("Failed to compile instructions: %d", (ErrorCode)num_instructions);
        return (ErrorCode)num_instructions;
    }

    printInstructions(term_out_channel, num_instructions, instructions, train_measurements, &path, track);
    // return NO_ERROR;

    int last_sensor_wait = -1;
    int w = num_instructions - 1;
    for(; w >= 0; w--) {
        Bool found = FALSE;
        switch(instructions[w].type) {
        case I_WAIT_FOR_SENSOR:
            last_sensor_wait = w;
        case I_BRANCH: 
        case I_MERGE:
        case I_ENTER:
        case I_EXIT:
            found = TRUE;
            break;

        default:
            // Do Nothing
            break;
        }

        if(found) break;
    }
    int line = 4;

    int i = 0;
    Buffer term;
    bufferReset(ANONYMOUS, &term);
    TrackNode last_node = current_node;
    for(; i < num_instructions; ++i) {
        bufferPutStr(&term, "\033[");
        bufferPutInt(&term, line);
        bufferPutStr(&term, ";92H*"); // 77 is start
        putB(term_out_channel, &term);
        line++;

        TaskId tid;
        Instruction instruction;
        Receive(&tid, &instruction, sizeof(instruction)); 

        switch(instruction.type) {
        case I_START:
            setSpeed(train_out_channel, train_number, train_measurements->internal_speed, term_out_channel); 
            break;

        case I_STOP:
            setSpeed(train_out_channel, train_number, 0, term_out_channel); 
            break;

        case I_REVERSE:
            reverse(train_out_channel, train_number, term_out_channel);
            toggleOrientation(orientation);
            printDirection(term_out_channel, *orientation);
            break;

        case I_WAIT_FOR_SENSOR:
            if(i != last_sensor_wait) {
                int k = 0;
                AwaitSensor(instruction.node, &k);

                // Print Velocity
                printTimeError(term_out_channel, k - instruction.sensor_timeout); 
                // Print diff from expected and actual time
            }
            break;

        case I_DELAY:
            Delay(instruction.delay);
            break;

        case I_SET_SWITCH:
            updateSwitch(train_out_channel, instruction.sw, instruction.sw_position, switches_server);
            break;

        case I_BRANCH: 
        case I_MERGE:
        case I_ENTER:
        case I_EXIT:
        default:
            break;
        }

        switch(instruction.type) {
        case I_WAIT_FOR_SENSOR:
        case I_BRANCH: 
        case I_MERGE:
        case I_ENTER:
        case I_EXIT:
           if(instruction.node != last_node) {
                // Output that we're branching
                printPos(term_out_channel, instruction.node);
                last_node = instruction.node;
            }               
            break;
        default:
            break;
        }
    }
*/

    return NO_ERROR;
}

ErrorCode calibrateStoppingDistance(TaskId channel, const int trainNumber, const int speed, TaskId term_out_channel) {
    int t1Sensor = 39; //C8
    int stopSensor = 5; //A6
    int reverseSensor = 34; //C3

    //Move the train at speed
    setSpeed(channel, trainNumber, speed, term_out_channel);


    //Wait for Sensor
    int i = 0;
    for (i = 0; i < 3; i++) {
        //Train is ready to start calibration test
        setSpeed(channel, trainNumber, speed, term_out_channel);

        int time = 0;
        int real_time = 0;
        int actualSensor = 0;
        float distance = 0;
        AwaitSensor(t1Sensor, &time, &real_time, &actualSensor, &distance);
        unsigned int start = real_time;

        AwaitSensor(stopSensor, &time, &real_time, &actualSensor, &distance);

        //stop train
        setSpeed(channel, trainNumber, 0, term_out_channel);

        unsigned int end = real_time;

        unsigned int tdiff = end - start;
        float factor = (float)1/(float)100;

        unsigned int timecs = (int)((float)tdiff * factor);
        timecs = timecs;
        DEBUG("Time: %d, ticks: %d", timecs, tdiff);

        Delay(6 * SECOND_DELAY);

        reverseAndGo(channel, trainNumber, 8, term_out_channel);
        AwaitSensor(reverseSensor, &time, &real_time, &actualSensor, &distance);
        reverseAndGo(channel, trainNumber, 0, term_out_channel);
        
        Delay(4 * SECOND_DELAY);
    }
 
    return NO_ERROR;
}

//Goes around the loop and when it hits a sensor
ErrorCode calibrateStoppingTime(TaskId channel, const int trainNumber, const int speed, TaskId term_out_channel) {
    DEBUG("Starting Stopping Time Calibration");

    TaskId term_in_channel = WhoIs(TERM_GET_SERVER);
    if(!isValidTaskId(term_in_channel)) {
        ERROR("Failed to get TERM_GET_SERVER in calibrateStoppingTime: %d", term_in_channel);            
    }

    int stopSensor = 76; //E13

    //Move the train at speed
    setSpeed(channel, trainNumber, speed, term_out_channel);

    //Wait for Sensor
    int i = 0;
    for (i = 0; i < 3; i++) {
        //Train is ready to start calibration test
        setSpeed(channel, trainNumber, speed, term_out_channel);

        int time = 0;
        int real_time = 0;
        int actualSensor = 0;
        float distance = 0;
        AwaitSensor(stopSensor, &time, &real_time, &actualSensor, &distance);
        unsigned int start = real_time;

        //stop train
        setSpeed(channel, trainNumber, 0, term_out_channel);

        //Wait for any key from TERMINAL
        getc(term_in_channel);
        unsigned int end = Time();

        unsigned int tdiff = end - start;
        tdiff = tdiff;
        DEBUG("Stopping time ticks: %d", tdiff);

        Delay(5 * SECOND_DELAY);
    }
 
    return NO_ERROR;
}

