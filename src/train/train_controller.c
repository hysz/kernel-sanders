#include "task.h"
#include "kapi.h"
#include "debug.h"
#include "common.h"
#include "io.h"
#include "errno.h"
#include "buffer.h"
#include "names.h"
#include "nameserver_api.h"
#include "clockserver_api.h"
#include "command.h"
#include "path_finder.h"
#include "train.h"
#include "path_finder.h"
#include "track_helpers.h"
#include "path_finder_api.h"
#include "train_physics.h"
#include "path.h"
#include "track_dict.h"
#include "instruction.h"
#include "train_helpers_term.h"
#include "train_helpers_track.h"
#include "await_sensor_api.h"
#include "new_train.h"
#include "courier.h"

void Train()
{
    DEBUG("Starting Trains()");

    Command cmd;
    int ptid;
    Receive(&ptid, &cmd, sizeof(cmd));
    ErrorCode received = NO_ERROR;
    Reply(ptid, &received, sizeof(received));
    int train_number = cmd.args[0];
    TrackNode current_node = cmd.args[1];

    int train_index = cmd.train_index;

    const track_node* track;
    ErrorCode track_error = getTrack(&track);
    if(track_error) {
        ERROR("Train(): Failed to get track %d", track_error);
    }
    
    TrainMeasurements train_measurements;
    getTrainMeasurements(train_number, (TrainMeasurements*)&train_measurements);

    TaskId train_out_channel = WhoIs(TRAIN_PUT_SERVER);
    if(! isValidTaskId(train_out_channel) ) {
        ERROR("Trains failed to lookup train out channel: %d", train_out_channel);
        Exit();
    }

    TaskId term_out_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(term_out_channel) ) {
        ERROR("Trains failed to lookup term out channel: %d", term_out_channel);
        Exit();
    }

    // Start Train Driver
    TaskId train_driver = Create(TRAIN_DRIVER_PRIORITY, &NewTrain);
    if(! isValidTaskId(train_driver) ) {
        ERROR("TrainDriver Invalid TID=%d", (ErrorCode)train_driver);
        Exit();
    } 

    // Send Initialization Message
    TaskId train_courier = INVALID_TID;
    {
        Info message;
        message.train_number = train_number;
        message.train_index = train_index;

        ErrorCode error = Send(train_driver, &message, sizeof(message), &train_courier, sizeof(train_courier));
        if(error) {
            ERROR("TrainDriver Init Failed %d", error);
            Exit();
        }
    }
    if(! isValidTaskId(train_courier)) {
        ERROR("Invalid Surveyor TID Returned %d", (ErrorCode)train_courier);
        Exit();
    }

    TaskId switches_server = WhoIs(SWITCHES_COURIER);
    if(! isValidTaskId(switches_server)) {
        ERROR("Invalid switches server: %d", (ErrorCode)switches_server);
        Exit();
    }

    // Send <Find> Message to train
    // Currently this just sets the start node. No autofind.
    {
        TrainMsg request;
        request.type = T_FIND;
        request.node = current_node;

        ErrorCode error = Send(train_courier, &request, sizeof(request), 0, 0);
        if(error) {
            ERROR("Failed to send find request to driver: %d", error);
        }
    }

    int train_speed = 0;

    ERROR("CONTROLLER READY!");
    FOREVER {
        TaskId tid;
        Command cmd;
        Receive(&tid, &cmd, sizeof(cmd));

        ErrorCode error = NO_ERROR;
        Reply(tid, &error, sizeof(error));

        switch(cmd.type) {
        case SPEED:
            error = setSpeed(train_out_channel, train_number, cmd.args[1], term_out_channel);
            if(! error) {
                train_speed = cmd.args[1];
            }
            break;

        case REVERSE:
            error = reverseAndGo(train_out_channel, cmd.args[0], 10, term_out_channel); 
            break;

        case MOVE:
            {
                TrainMsg request;
                request.type = T_GO;
                request.destination = cmd.args[1];

                error = Send(train_courier, &request, sizeof(request), 0, 0);
                if(error) {
                    ERROR("Failed to send move request to driver: %d", error);
                } else {
                    current_node = cmd.args[1];
                }
            }
            break;

        case INIT:
            {
                TrainMsg request;
                request.type = T_FIND;
                request.node = cmd.args[1];

                error = Send(train_courier, &request, sizeof(request), 0, 0);
                if(error) {
                    ERROR("Failed to send find request to driver: %d", error);
                }
            }
            break;

        case ACCELERATION:
            remeasureA(cmd.args[1], &train_measurements);
            break; 

        case DECCELERATION:
            remeasureD(cmd.args[1], &train_measurements);
            break; 

        case VELOCITY:
            remeasureV(cmd.args[1], &train_measurements);
            break;

        case STOPPING_DISTANCE_CALIBRATION:
            {
                updateSwitch(train_out_channel, 18, 'S', switches_server);
                updateSwitch(train_out_channel, 3, 'S', switches_server);
                updateSwitch(train_out_channel, 5, 'S', switches_server);
                calibrateStoppingDistance(train_out_channel, cmd.args[0], cmd.args[1], term_out_channel);
                break;
            }

        case STOPPING_TIME_CALIBRATION:
            {
                updateSwitch(train_out_channel, 8, 'C', switches_server);
                updateSwitch(train_out_channel, 14, 'C', switches_server);
                updateSwitch(train_out_channel, 13, 'S', switches_server);
                calibrateStoppingTime(train_out_channel, cmd.args[0], cmd.args[1], term_out_channel);
                break;
            }

        default:
            error = UNRECOGNIZED_SPEED_COMMAND;
            break;
        }

        if(error) {
            ERROR("Failed to run train command: %d", error); 
        }
    } 

    DEBUG("Finished Trains()");
    Exit();
}
