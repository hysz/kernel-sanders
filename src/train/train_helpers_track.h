#pragma once
#include "task.h"
#include "kapi.h"
#include "debug.h"
#include "common.h"
#include "io.h"
#include "errno.h"
#include "buffer.h"
#include "names.h"
#include "nameserver_api.h"
#include "clockserver_api.h"
#include "command.h"
#include "path_finder.h"
#include "train.h"
#include "path_finder.h"
#include "track_helpers.h"
#include "path_finder_api.h"
#include "train_physics.h"
#include "path.h"
#include "instruction.h"

ErrorCode setSpeed(TaskId channel, int train, int speed, TaskId print_channel);
ErrorCode reverse(TaskId channel, int train, TaskId term_out_channel);
ErrorCode reverseAndGo(TaskId channel, int train, int speed, TaskId term_out_channel);
ErrorCode updateSwitch(TaskId channel, int sw, char direction, TaskId switches_server_tid);
ErrorCode move( const int train_number,
                Orientation *orientation,
                const TrainMeasurements *measurements,
                const track_node *track,
                const int current_node,
                const int destination_node,
                const TaskId train_out_channel,
                const TaskId term_out_channel,
                const TaskId switches_server);
ErrorCode calibrateStoppingDistance(TaskId channel, const int trainNumber, const int speed, TaskId term_out_channel);

ErrorCode calibrateStoppingTime(TaskId channel, const int trainNumber, const int speed, TaskId term_out_channel);
