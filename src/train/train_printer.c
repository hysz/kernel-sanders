#include "train_printer.h"
#include "common.h"
#include "kapi.h"
#include "task.h"
#include "debug.h"
#include "buffer.h"
#include "errno.h"
#include "instruction.h"
#include "track_node.h"
#include "track_dict.h"
#include "clockserver_api.h"
#include "new_train.h"
#include "nameserver_api.h"
#include "node_queue.h"

static ErrorCode printHeader(TaskId channel, int train, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[10;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "-----Train "); 
    bufferPutInt(&b, train);
    bufferPutStr(&b, "-----");

    bufferPutStr(&b, "\033[11;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Trip: --");

    bufferPutStr(&b, "\033[12;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Direction: Forward");

    bufferPutStr(&b, "\033[13;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Distance to Travel: --");

    bufferPutStr(&b, "\033[14;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Distance Travelled: --");

    bufferPutStr(&b, "\033[15;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "State: --");

    bufferPutStr(&b, "\033[16;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Velocity: --");

    bufferPutStr(&b, "\033[17;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Position: --");

    bufferPutStr(&b, "\033[18;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Prediction: --");

    bufferPutStr(&b, "\033[19;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Destination Offset: --");

    bufferPutStr(&b, "\033[20;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Stops: --");

    bufferPutStr(&b, "\033[21;");
    bufferPutInt(&b, x);
    bufferPutStr(&b, "H");
    bufferPutStr(&b, "Reserved: --");
    
    return putB(channel, &b); 
}

static ErrorCode printTrip(TaskId channel, int from, int to, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[11;");
    bufferPutInt(&b, (x == 0) ? 7 : x + 6);
    bufferPutStr(&b, "H");

    bufferPutStr(&b, trackNode_IndexToName(from));
    bufferPutStr(&b, " to ");
    bufferPutStr(&b, trackNode_IndexToName(to));
    while(b.size < 20) {
        bufferPut(&b, ' ');
    }
    
    return putB(channel, &b); 
}

static ErrorCode printOrientation(TaskId channel, Orientation orientation, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[12;");
    bufferPutInt(&b, (x == 0) ? 12 : x + 11);
    bufferPutStr(&b, "H");

    if(orientation == FORWARD) {
        bufferPutStr(&b, "Forward ");
    } else {
        bufferPutStr(&b, "Backward");
    } 
    
    return putB(channel, &b); 
}

static ErrorCode printDistanceToTravel(TaskId channel, float distance_to_travel, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[13;");
    bufferPutInt(&b, (x == 0) ? 21 : x + 20);
    bufferPutStr(&b, "H");
    bufferPutInt(&b, distance_to_travel);
    bufferPutStr(&b, " mm    ");

    return putB(channel, &b); 
}

static ErrorCode printDistanceTravelled(TaskId channel, float distance_travelled, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[14;");
    bufferPutInt(&b, (x == 0) ? 21 : x + 20);
    bufferPutStr(&b, "H");
    bufferPutInt(&b, distance_travelled);
    bufferPutStr(&b, " mm    ");

    return putB(channel, &b); 
}

static ErrorCode printVelocityState(TaskId channel, const Motion motion, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[15;");
    bufferPutInt(&b, (x == 0) ? 8 : x + 7);
    bufferPutStr(&b, "H");
    switch(motion) {
    case STOPPED:
        bufferPutStr(&b, "Resting     ");
        break;
    
    case CONSTANT:
        bufferPutStr(&b, "Constant    ");
        break;
    
    case DECELERATING:
        bufferPutStr(&b, "Decelerating");
        break;
    
    case ACCELERATING: 
        bufferPutStr(&b, "Accelerating");
        break;

    default:
        bufferPutStr(&b, "--          ");
        break;
    }

    return putB(channel, &b); 
}

static ErrorCode printVelocity(TaskId channel, float velocity, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[16;");
    bufferPutInt(&b, (x == 0) ? 11 : x + 10);
    bufferPutStr(&b, "H");
    bufferPutInt(&b, velocity);
    bufferPutStr(&b, " mm/s    ");

    return putB(channel, &b); 
}

static ErrorCode printPosition(TaskId channel, TrackNode node, int offset, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[17;");
    bufferPutInt(&b, (x == 0) ? 11 : x + 10);
    bufferPutStr(&b, "H");

    if(node >= 0) {
        bufferPutStr(&b, trackNode_IndexToName(node));
    } else {
        bufferPutStr(&b, "--");
    }
    if(offset >= 0) {
        bufferPutStr(&b, " +"); 
        bufferPutInt(&b, offset);
    } else {
        bufferPutStr(&b, " -"); 
        bufferPutInt(&b, -1 * offset);
    }
    bufferPutStr(&b, " mm    "); 
    
    return putB(channel, &b); 
}

static ErrorCode printPredictionDelta(TaskId channel, float offset, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[18;");
    bufferPutInt(&b, (x == 0) ? 13 : x + 12);
    bufferPutStr(&b, "H");

    if(offset >= 0) {
        bufferPutStr(&b, "+"); 
        bufferPutInt(&b, offset);
    } else {
        bufferPutStr(&b, "-"); 
        bufferPutInt(&b, -1 * offset);
    }
    bufferPutStr(&b, " mm   "); 
    
    return putB(channel, &b); 
}

static ErrorCode printDestinationOffset(TaskId channel, float offset, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[19;");
    bufferPutInt(&b, (x == 0) ? 21 : x + 20);
    bufferPutStr(&b, "H");

    if(offset >= 0) {
        bufferPutStr(&b, "+"); 
        bufferPutInt(&b, offset);
    } else {
        bufferPutStr(&b, "-"); 
        bufferPutInt(&b, -1 * offset);
    }
    bufferPutStr(&b, " mm   "); 
    
    return putB(channel, &b); 
}

static ErrorCode printStops(TaskId channel, TrackNodeQueue *stops, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[20;");
    bufferPutInt(&b, (x == 0) ? 8 : x + 7);
    bufferPutStr(&b, "H");

    int i = 0;
    for(; i < stops->len; ++i) {
        queueItem item = queuePopItem_TN(stops);
        queuePush_TN(stops, item.n, item.dist);

        if(item.type != Q_STOP) continue;

        bufferPutStr(&b, trackNode_IndexToName(item.n));
        if(i < stops->len - 1) bufferPutStr(&b, ", ");
    }
    while(b.size < 40) {
        bufferPut(&b, ' ');
    }
    
    return putB(channel, &b); 
}

static ErrorCode printNodes(TaskId channel, TrackNodeQueue *nodes, int x)
{
    Buffer b;
    bufferReset(ANONYMOUS, &b);

    bufferPutStr(&b, "\033[21;");
    bufferPutInt(&b, (x == 0) ? 11 : x + 10);
    bufferPutStr(&b, "H");

    int i = 0;
    for(; i < nodes->len; ++i) {
        queueItem item = queuePopItem_TN(nodes);
        queuePush_TN(nodes, item.n, item.dist);

        bufferPutStr(&b, trackNode_IndexToName(item.n));
        if(i < nodes->len - 1) bufferPutStr(&b, ", ");
    }

    while(b.size < 40) {
        bufferPut(&b, ' ');
    }
    
    return putB(channel, &b); 
}

void TrainPrinter()
{
    // Initialization Message
    PrinterInit init_message;
    TaskId tid;
    ErrorCode init_error = Receive(&tid, &init_message, sizeof(init_message));
    if(init_error) {
        ERROR("Recv Init Message %d", init_error);
        Exit();
    }   
    Reply(tid, 0, 0); 

    const Info* info = init_message.info;
    const State* state = init_message.state;
    int x = info->train_index * 40;
    TaskId term_out_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(term_out_channel)) {
        ERROR("Failed ot lookup term put server: %d", (ErrorCode)term_out_channel);
        Exit();
    }

    State last_state;
    last_state.motion = -1;
    last_state.orientation = -1;
    last_state.velocity = -1;
    last_state.last_sensor = -1;
    last_state.prediction_delta = -1;
    last_state.distance_travelled = -1;
    last_state.last_sensor_distance = -1;
    last_state.distance_to_travel = -1;
    last_state.destination_offset = 0;
    last_state.from = -1;
    last_state.to = -1;
    last_state.last_landmark = -1;

    queueReset_TN(&last_state.nodes);
    queueReset_TN(&last_state.stops);

    printHeader(term_out_channel, info->train_number, x);
    
    FOREVER {
        Delay(PRINT_DELAY); 

        if(last_state.from != state->from || last_state.to != state->to) {
            last_state.from = state->from;
            last_state.to = state->to;

            printTrip(term_out_channel, last_state.from, last_state.to, x);
        }
    
        if(last_state.orientation != state->orientation) {
            last_state.orientation = state->orientation; 
            printOrientation(term_out_channel, last_state.orientation, x); 
        }

        if(last_state.velocity != state->velocity) {
            last_state.velocity = state->velocity;
            printVelocity(term_out_channel, last_state.velocity, x);
        }

        if(last_state.motion != state->motion) {
            last_state.motion = state->motion;
            printVelocityState(term_out_channel, last_state.motion, x);
        }

        if(last_state.prediction_delta != state->prediction_delta) {
            last_state.prediction_delta = state->prediction_delta;
            printPredictionDelta(term_out_channel, last_state.prediction_delta, x);
        }

        if(last_state.distance_to_travel != state->distance_to_travel) {
            last_state.distance_to_travel = state->distance_to_travel;
            printDistanceToTravel(term_out_channel, last_state.distance_to_travel, x);
        }

        if(last_state.destination_offset != state->destination_offset) {
            last_state.destination_offset = state->destination_offset;
            printDestinationOffset(term_out_channel, last_state.destination_offset, x);
        }

        if(last_state.stops.len != state->stops.len) {
            cpyN(&last_state.stops, &state->stops, sizeof(state->stops));
            printStops(term_out_channel, &last_state.stops, x);
        } 

        if(last_state.nodes.len != state->nodes.len) {
            cpyN(&last_state.nodes, &state->nodes, sizeof(state->nodes));
            printNodes(term_out_channel, &last_state.nodes, x);
        } 

        Bool update_position = FALSE;
        if(last_state.last_landmark_dist != state->last_landmark_dist) {
            last_state.last_landmark_dist = state->last_landmark_dist;
            update_position = TRUE;
        }

        if(last_state.distance_travelled != state->distance_travelled) {
            last_state.distance_travelled = state->distance_travelled;
            printDistanceTravelled(term_out_channel, last_state.distance_travelled, x);
            update_position = TRUE;
        }

        if(last_state.last_landmark != state->last_landmark) {
            last_state.last_landmark = state->last_landmark;
            update_position = TRUE;
        }

        if(update_position) {
            printPosition(term_out_channel, last_state.last_landmark, last_state.distance_travelled - last_state.last_landmark_dist, x);
        }
    }
}
