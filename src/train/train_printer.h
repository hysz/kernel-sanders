#pragma once

#include "instruction.h"
#include "track_node.h"
#include "track_dict.h"
#include "new_train.h"

enum {PRINT_DELAY = 10};

struct PrinterInit {
    const Info *info;
    const State *state;
};
typedef struct PrinterInit PrinterInit;

void TrainPrinter();
