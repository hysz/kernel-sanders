#pragma once

#include "task.h"
#include "errno.h"
#include "path.h"
#include "path_finder.h"
#include "train_physics.h"
#include "instruction.h"

ErrorCode printSpeed(TaskId print_channel, int train, int speed);
ErrorCode printPath(TaskId print_channel, Path *path);
ErrorCode printOrientation(TaskId print_channel, int train, int speed);
ErrorCode printMeasurements(TaskId print_channel, const TrainMeasurements *m);
ErrorCode printPos(const TaskId print_channel, const TrackNode node);
ErrorCode printTimeError(const TaskId print_channel, const int diff);
ErrorCode printDirection(const TaskId print_channel, const Orientation orientation);
