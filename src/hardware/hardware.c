#include "hardware.h"

#include "interrupt.h"
#include "clock.h"
#include "term.h"
#include "hw_train.h"

void initializeHardware()
{
    initializeTimer();
    initializeTrain();
    initializeTerm();
}
