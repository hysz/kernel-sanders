#pragma once

struct Clock {
    short hs;
    unsigned int second;
    unsigned int minute;
};
typedef struct Clock Clock;

// Resets clock to 0:0:0
void resetClock(Clock *t);

// Increments clock by 1 centisecond
void incrementClock(Clock *t);

// Increments clock by 1 decisecond
void incrementClockDS(Clock *c);

// Gets number of clock ticks (total deciseconds)
int getTicks(Clock *c);

// Initializes clock's corresponding timer
void initializeTimer();

// Creates Clock struct
Clock createClock();
