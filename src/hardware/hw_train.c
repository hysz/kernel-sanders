#include "hw_train.h"
#include "ts7200.h"

void initializeTrain()
{
    // Enable UART1
    int *ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET);
    *ctrl |= UARTEN_MASK;
    *ctrl |= RIEN_MASK;
    *ctrl |= RTIEN_MASK;

    /*
        We want Baud rate of 2400
        Need to find divisor, given that we're running at a 508 Khz frequency.
        See calculation exampl here: http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0183g/I49493.html
        Baud Rate Divisor = (508 * 10^3) / (16 * 2400) = 13.229
                          = 00000000 00001101
     */

    // Set most significant byte of Baud Rate Divisor
    int *lcrm = (int *)( UART1_BASE + UART_LCRM_OFFSET);
    *lcrm = 0x0;

    int *lcrl = (int *)( UART1_BASE + UART_LCRL_OFFSET);
    *lcrl = 0xbf;
    // *lcrl = 0xd;

    /* Set according to Marklin Documentation (http://www.marklin.com/tech/digital1/components/commands.html)
     * Baud rate = 2400
     * Start Bits = 1 (if requested by computer)
     * Stop Bits = 2
     * Parity = None
     * Word size = 8 bits
     */

    /* Sanity test for lcrh: Binary should look like 1101000 */
    int* lcrh = (int *)( UART1_BASE + UART_LCRH_OFFSET); 
    *lcrh = 0;
    *lcrh &= ~BRK_MASK;  // Not specified in docs; so disable.
    *lcrh &= ~PEN_MASK;  // Disable parity
    *lcrh &= ~EPS_MASK;  // No parity check, so this shouldn't matter.
    *lcrh |= STP2_MASK;  // 2 stop bits
    //*lcrh &= ~FEN_MASK;  // Disable FIFO
    *lcrh |= FEN_MASK;  // Enable FIFO
    *lcrh |= WLEN_MASK;  // 8 bit words

    // Clear any errors from previous session
    int *errs = (int *)(UART1_BASE + UART_RSR_OFFSET);
    *errs = 0;
}

// THESE ARE USED DURING KERNEL INITIALIZATION - DONT USE ELSEWHERE
// BECAUSE INTERRUPTS WILL BE ENABLED AND THIS WILL CAUSE
// A BAD TIME..
int trainHasChar()
{
    int *flags = (int *)( UART1_BASE + UART_FLAG_OFFSET );
    return *flags & RXFF_MASK;
}

char getTrainChar()
{
    int *data = (int *)( UART1_BASE + UART_DATA_OFFSET );
    return *data;
}
