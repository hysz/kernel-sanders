#include "clock.h"
#include "ts7200.h"
#include "common.h"
#include "interrupt.h"
#include "debug.h"

void resetClock(Clock *c)
{
    c->hs = 0;
    c->second = 0;
    c->minute = 0;
}

void incrementClock(Clock *c)
{
    if(c->hs < 99) {
        c->hs += 1;
    } else { // tenth == 9, so tenth+1 == 10. Increment seconds.
        c->hs = 0;
        
        if(c->second < 59) {
            c->second += 1;
        } else { // second == 59, so second+1 == 60. Increment minutes.
            c->second = 0;
            c->minute++;
        }
    }
}

void incrementClockDS(Clock *c)
{
    if(c->hs < 90) {
        c->hs += 10;
    } else {
        c->hs = (c->hs + 10) % 100;
        
        if(c->second < 59) {
            c->second += 1;
        } else { // second == 59, so second+1 == 60. Increment minutes.
            c->second = 0;
            c->minute++;
        }
    }
}

int getTicks(Clock *c)
{
    int ticks;
    ticks = c->hs;
    ticks += c->second * 100; // 100 = num_ticks in a second
    ticks += c->minute * 60 * 100; // 60 * 100 = num_ticks in a minute 

    return ticks;
}

void initializeTimer()
{
    // Configure timer
    int* timer_crtl = (int*)(TIMER3_BASE + CRTL_OFFSET);
    // Reset values: disabled; free running (0xff.. down to 0x00); 2 kHz 
    *timer_crtl = 0;
    // Enable
    *timer_crtl |= ENABLE_MASK;

    // Use 508 Khz 
    *timer_crtl |= CLKSEL_MASK; 

    // Set Countdown from 508 (1000th of a second)
    unsigned int* timer_ldr = (unsigned int*)(TIMER3_BASE + LDR_OFFSET);
    *timer_ldr = 5080;
    *timer_crtl |= MODE_MASK; 
}

Clock createClock()
{
    // Create our clock
    Clock clock;
    resetClock(&clock);

    return clock;
}
