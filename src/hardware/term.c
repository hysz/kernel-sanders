#include "term.h"
#include "ts7200.h"

void initializeTerm()
{
    // Enable UART2
    int *ctrl = (int *)(UART2_BASE + UART_CTLR_OFFSET);
    *ctrl |= UARTEN_MASK;
    *ctrl |= RIEN_MASK;
    *ctrl |= RTIEN_MASK;

    // Disable FIFO
    int* lcrh = (int *)( UART2_BASE + UART_LCRH_OFFSET); 
    *lcrh |= FEN_MASK;
}

// THESE ARE USED DURING KERNEL INITIALIZATION - DONT USE ELSEWHERE
// BECAUSE INTERRUPTS WILL BE ENABLED AND THIS WILL CAUSE
// A BAD TIME..
int termHasChar()
{
    int *flags = (int *)( UART2_BASE + UART_FLAG_OFFSET );
    return *flags & RXFF_MASK;
}

char getTermChar()
{
    int *data = (int *)( UART2_BASE + UART_DATA_OFFSET );
    return *data;
}
