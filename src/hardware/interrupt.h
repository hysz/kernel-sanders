#pragma once
#include "task.h"

#include "errno.h"
#include "event.h"

#define NUM_OF_INTERRUPTS 20
#define MAX_INTERRUPTS 64

#define VIC1_BASE 0x800B0000
#define VIC2_BASE 0x800C0000

#define IRQ_STATUS_OFFSET 0x00
#define FIQ_STATUS_OFFSET 0x04
#define RAW_STATUS_OFFSET 0x08

#define INT_ENABLE_OFFSET 0x10
#define INT_ENABLE_CLEAR_OFFSET 0x14
#define INT_SELECT_OFFSET 0x0C

#define SOFT_INT_OFFSET 0x18
#define SOFT_INT_CLEAR_OFFSET 0x1C

//Interrupt Bit Numbers
#define TIMER3_INT_BIT 51
#define UART1_RCV_INT_BIT 23
#define UART1_TX_INT_BIT 24
#define UART1_OR_INT_BIT 52
#define UART2_RCV_INT_BIT 25
#define UART2_TX_INT_BIT 26
#define UART2_OR_INT_BIT 54

//Interrupt Clear Addresses
#define TIMER3_CLEAR_ADDR 0x8081008C
#define UART1_TX_CLEAR_ADDR 0x808c001c
#define UART2_TX_CLEAR_ADDR 0x808d001c

// Interrupt Vectors
#define SWI_VECTOR 0x08
#define HWI_VECTOR 0x18

ErrorCode enableInterrupt(int bitNum);

ErrorCode disableInterrupt(int bitNum);

ErrorCode clearInterrupt(int bitNum);

ErrorCode genSoftInterrupt(int bitNum);

ErrorCode clearSoftInterrupt(int bitNum);

ErrorCode getInterruptId(int* events, int* eventIdTable);

unsigned int getICUAddr(int bitNum);

// Installs Handlers & enable interrupts of interest
void initializeInterrupts();

void disableTrainTxInterrupts();

void enableTrainTxInterrupts();

void disableTermTxInterrupts();

void enableTermTxInterrupts();


void disableTrainRxInterrupts();

void enableTrainRxInterrupts();

void disableTermRxInterrupts();

void enableTermRxInterrupts();


void enableTrainModemInterrupt();
void disableTrainModemInterrupt();
