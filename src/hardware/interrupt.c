#include "interrupt.h"
#include "debug.h"
#include "task.h"
#include "context_switch.h"
#include "common.h"
#include "ts7200.h"

ErrorCode enableInterrupt(int bitNum) {
    if (bitNum < 0 || bitNum >= MAX_INTERRUPTS) {
        return INVALID_INTRPT_BIT_ERROR;
    }
    int* VICIntSelect = (int*) (getICUAddr(bitNum) + INT_SELECT_OFFSET);
    int* VICIntEnable = (int*) (getICUAddr(bitNum) + INT_ENABLE_OFFSET);
    if (bitNum >= 32) {
        bitNum -= 32;
    }
    *VICIntSelect &= ~(1 << bitNum);
    *VICIntEnable |= 1 << bitNum;

    return NO_ERROR;
}

ErrorCode disableInterrupt(int bitNum) {
    if (bitNum < 0 || bitNum >= MAX_INTERRUPTS) {
        return INVALID_INTRPT_BIT_ERROR;
    }
    int* VICIntEnable = (int*) (getICUAddr(bitNum) + INT_ENABLE_CLEAR_OFFSET);
    if (bitNum >= 32) {
        bitNum -= 32;
    }
    *VICIntEnable = 1 << bitNum;

    return NO_ERROR;
}

ErrorCode genSoftInterrupt(int bitNum) {
    if (bitNum < 0 || bitNum >= MAX_INTERRUPTS) {
        return INVALID_INTRPT_BIT_ERROR;
    }
    int* VICSoftInt = (int*) (getICUAddr(bitNum) + SOFT_INT_OFFSET);
    if (bitNum >= 32) {
        bitNum -= 32;
    }
    *VICSoftInt |= 1 << bitNum;

    return NO_ERROR;
}

ErrorCode clearSoftInterrupt(int bitNum) {
    if (bitNum < 0 || bitNum >= MAX_INTERRUPTS) {
        return INVALID_INTRPT_BIT_ERROR;
    }
    int* VICClearSoftInt = (int*) (getICUAddr(bitNum) + SOFT_INT_CLEAR_OFFSET);
    if (bitNum >= 32) {
        bitNum -= 32;
    }
    *VICClearSoftInt |= 1 << bitNum;

    return NO_ERROR;
}

//Disables the interrupt
ErrorCode clearInterrupt(int bitNum) {
    if (bitNum < 0 || bitNum >= MAX_INTERRUPTS) {
        return INVALID_INTRPT_BIT_ERROR;
    }

    int* VICIntClear = (int*) (getICUAddr(bitNum) + INT_ENABLE_CLEAR_OFFSET);
    if (bitNum >= 32) {
        bitNum -= 32;
    }
    *VICIntClear |= (1 << bitNum);

    return NO_ERROR;
}

ErrorCode getInterruptId(int* event, int* eventBitTable) {
    int* VICIntStatus;
    unsigned int bitMask;
    int i;
    for (i = 0; i < NUM_EVENTS; i++) {
        int bitNumOffset = 0;
        VICIntStatus = (int*) (getICUAddr(eventBitTable[i]) + IRQ_STATUS_OFFSET);
        if (eventBitTable[i] >= 32) {
            bitNumOffset = 32;
        } else {
            bitNumOffset = 0;
        }

        bitMask = 1 << (eventBitTable[i] - bitNumOffset);
        if ((*VICIntStatus & bitMask) > 0) {
            *event = i;
            break;
        }
    }

    if (i == NUM_EVENTS) {
        return NO_INTERRUPTS_TRIGGERED_ERROR;
    }

    return NO_ERROR;
}

unsigned int getICUAddr(int bitNum) {
    if (bitNum >= 32) {
        return VIC2_BASE;
    } else {
        return VIC1_BASE;
    }
}

static void installSwiHandler()
{
    unsigned handlerAddress = (unsigned)&SwiHandler + PROGRAM_OFFSET;

    //SWI handler
    unsigned location = ((handlerAddress - (unsigned)SWI_VECTOR - 0x8)>>2);
    if(location & 0xff000000) {
        KDEBUG("DONE FUDGED LOCATION");
    }   
    location |= 0xEA000000;
    unsigned* ptr = (unsigned *)(SWI_VECTOR);
    *ptr = location;
}

static void installHwiHandler()
{
    unsigned handlerAddress = (unsigned)&HwiHandler + PROGRAM_OFFSET;

    //hwi handler
    unsigned location = ((handlerAddress - (unsigned)HWI_VECTOR - 0x8)>>2);
    if(location & 0xff000000) {
        KDEBUG("DONE FUDGED hwi LOCATION");
    }
    location |= 0xEA000000;
    unsigned *ptr = (unsigned *)(HWI_VECTOR);
    *ptr = location;
}

static void enableTimerInterrupt()
{
    enableInterrupt(TIMER3_INT_BIT);
}

void initializeInterrupts()
{
     // Install handlers 
     installSwiHandler();
     installHwiHandler();

    /* Enable interrupts of interest */
    enableTimerInterrupt();
    enableInterrupt(UART1_OR_INT_BIT);
    enableInterrupt(UART2_OR_INT_BIT);
    disableTrainTxInterrupts();
    disableTermTxInterrupts();
}

void disableTrainTxInterrupts() {
    int *ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET);
    *ctrl &= ~TIEN_MASK;
    *ctrl &= ~MSIEN_MASK;
}

void enableTrainTxInterrupts() {
    int *ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET);
    *ctrl |= TIEN_MASK;
    *ctrl |= MSIEN_MASK;
}

void disableTermTxInterrupts() {
    int *ctrl = (int *)(UART2_BASE + UART_CTLR_OFFSET);
    *ctrl &= ~TIEN_MASK;
}

void enableTermTxInterrupts() {
    int *ctrl = (int *)(UART2_BASE + UART_CTLR_OFFSET);
    *ctrl |= TIEN_MASK;
}

void disableTrainRxInterrupts() {
    int *ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET);
    *ctrl &= ~RIEN_MASK;
    *ctrl &= ~RTIEN_MASK;
}

void enableTrainRxInterrupts() {
    int *ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET);
    *ctrl |= RIEN_MASK;
    *ctrl |= RTIEN_MASK;
}

void disableTermRxInterrupts() {
    int *ctrl = (int *)(UART2_BASE + UART_CTLR_OFFSET);
    *ctrl &= ~RIEN_MASK;
    *ctrl &= ~RTIEN_MASK;
}

void enableTermRxInterrupts() {
    int *ctrl = (int *)(UART2_BASE + UART_CTLR_OFFSET);
    *ctrl |= RIEN_MASK;
    *ctrl |= RTIEN_MASK;
}
