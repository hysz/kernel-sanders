#pragma once


enum { MAX_SWITCHES = 5, NUM_SENSORS = 40 };
#define SWITCH_DIRECTION_STRAIGHT 'S'
#define SWITCH_DIRECTION_CURVED   'C'

struct SwitchInfo {
    int  sw;
    char direction;
};
typedef struct SwitchInfo SwitchInfo;

struct TrackItem {
    int time; // time in centiseconds to travel between
    SwitchInfo switches[MAX_SWITCHES];
};
typedef struct TrackItem TrackItem;
