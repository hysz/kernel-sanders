#pragma once

#include "errno.h"
#include "track_node.h"
#include "track_data_new.h"

typedef enum {GET_TRACK} TrackRequestType;

struct TrackRequest {
    TrackRequestType type;
};
typedef struct TrackRequest TrackRequest;

struct TrackReply {
    ErrorCode error;
    track_node* track;
};
typedef struct TrackReply TrackReply;

void TrackServer();
