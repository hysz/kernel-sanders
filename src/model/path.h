#pragma once

#include "track_consts.h"
#include "track_dict.h"
#include "track_data_new.h"
#include "common.h"

struct Path {
    TrackNode nodes[TRACK_MAX];
    unsigned int num_nodes;
    unsigned int distance;
    Bool close;
    Bool open;
    char sw_position;
};
typedef struct Path Path;


