#include "sorted_node_queue.h"
#include "buffer.h"
#include "debug.h"

void queueReset_STN(SortedTrackNodeQueue* q)
{
    q->next = 0;
    q->len = 0;
}

ErrorCode queuePush_STN(SortedTrackNodeQueue* q, TrackNode n, int dist)
{
    if(q->len == SORTED_TRACK_NODE_QUEUE_SIZE) {
        return TRACK_NODE_QUEUE_FULL; 
    }

    // Extend Queue
    q->data[q->next].n = n; 
    q->data[q->next].dist = dist;

    q->next = (q->next + 1) % SORTED_TRACK_NODE_QUEUE_SIZE;
    if(q->len < SORTED_TRACK_NODE_QUEUE_SIZE) q->len++;

    // Reorganize Elements - O(n)
    int last_k = q->next - 1;;
    if(last_k < 0) last_k += SORTED_TRACK_NODE_QUEUE_SIZE;

    int i = 0;
    for(; i < q->len - 1; ++i) {
        int k = q->next - 2 - i;
        if(k < 0) k += SORTED_TRACK_NODE_QUEUE_SIZE; 

        if(dist > q->data[k].dist) break;

        q->data[last_k].n = q->data[k].n;
        q->data[last_k].dist = q->data[k].dist;
        
        q->data[k].n = n;
        q->data[k].dist = dist;

        last_k = k;
    }

    return NO_ERROR;
}

int queueEmpty_STN(SortedTrackNodeQueue* q)
{
    return (q->len == 0); 
}

TrackNode queuePop_STN(SortedTrackNodeQueue* q)
{
    if( queueEmpty_STN( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop from empty STN Queue");
        return 69;
    }

    int k = q->next - q->len;
    if(k < 0) k += SORTED_TRACK_NODE_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;
    return q->data[k].n;
} 

queueItem queuePopItem_STN(SortedTrackNodeQueue* q)
{
    if( queueEmpty_STN( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop item from empty STN Queue");
    }

    int k = q->next - q->len;
    if(k < 0) k += SORTED_TRACK_NODE_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;
    return q->data[k];
}
