#pragma once

#include "train_physics.h"
#include "path.h"
#include "track_dict.h"
#include "track_data_new.h"
#include "train.h"
#include "path_finder.h"
#include "track_helpers.h"
#include "path_finder_api.h"
#include "task.h"
#include "kapi.h"
#include "debug.h" 
#include "common.h"
#include "io.h"
#include "errno.h"
#include "buffer.h"
#include "names.h"

enum {MAX_STOPS = 20};
enum {MAX_INSTRUCTIONS = 60}; // Maximum number of instructions a train can have
typedef enum {FORWARD, BACKWARD} Orientation;

#define DIRECTION_CHANGE -1

typedef enum {I_START, I_STOP, I_REVERSE, I_WAIT_FOR_SENSOR, I_DELAY, I_SET_SWITCH, I_BRANCH, I_MERGE, I_ENTER, I_EXIT} InstructionType;

struct Instruction {
    InstructionType type;
    TrackNode node;     // node corresponds to
    D distance;         // Distance from last node instruction to this ndeo
    union {
        int merge;
        int branch;
        int sensor;
        double delay;
        int sw;
    };

    union {
        char sw_position;
        int sensor_timeout;
    };
};
typedef struct Instruction Instruction;

struct Switch {
    D dist;
    int sw;
    char pos;
    TrackNode node;
};
typedef struct Switch Switch;
enum {MAX_SWITCHES = 14};

ErrorCode printInstructions(TaskId print_channel,
                           int num_instructions,
                           Instruction instructions[MAX_INSTRUCTIONS],
                           const TrainMeasurements *measurements);

int compileInstructions(int velocity,
                        const Path* path,
                       const Orientation orientation,
                       const TrainMeasurements* measurements,
                       const track_node* track,
                       Instruction instructions[MAX_INSTRUCTIONS],
                       Physics *physics);

void toggleOrientation(Orientation *o);
