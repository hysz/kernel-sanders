#include "track_server.h"
#include "common.h"
#include "task.h"
#include "errno.h"
#include "kapi.h"
#include "debug.h"
#include "names.h"
#include "nameserver_api.h"

void TrackServer()
{
    // Handle Requests
    const int request_len = sizeof(TrackRequest);
    const int reply_len = sizeof(TrackReply);

    // Retrieve pointer to track from parent    
    track_node* track = 0;
    int get_tid;
    Receive(&get_tid, &track, sizeof(track));
    Reply(get_tid, 0, 0);

    //ERROR("TrackServer: 0x%x", track);

    // Register Server
    ErrorCode error = RegisterAs(TRACK_SERVER);
    if(error) {
        ERROR("Failed to register track server: %d", error);
    }

    FOREVER {
        // Get Request
        TaskId tid = INVALID_TID; 
        TrackRequest request;
        Receive(&tid, &request, request_len);

        // Process Request
        TrackReply reply;
        switch(request.type) {
        case GET_TRACK:
            reply.track = track;
            reply.error = NO_ERROR;
            break;

        default:
            reply.error = ERR_BAD_REQUEST_TYPE;
            break; 
        }
        if(reply.error) {
            ERROR("TrackServer: %d", reply.error);
        }

        Reply(tid, &reply, reply_len);
    }

    DEBUG("Track Server Exiting");
    Exit();
}
