#include "sensor_info.h"
#include "debug.h"
#include "track_dict.h"

void initializeSensorInfo(SensorInfo *sensor_info)
{
    sensor_info->train = 0;
    sensor_info->node = -1;
}

Bool isValidSensorInfo(const SensorInfo *sensor_info)
{
    return sensor_info->train > 0 && sensor_info->node >= 0;
}

void initializeSensorInfoMap(SensorInfoMap* sensor_info_map)
{
    int i = 0;
    for(; i < TRACK_MAX; ++i) {
        initializeSensorInfo(&sensor_info_map->sensors[i]);
    }
}

void assignSensor(SensorInfoMap* sensor_info_map, const SensorInfo *sensor_info)
{
    if(! isValidSensorInfo(sensor_info)) {
        ERROR("Tried to assign invalid sensor info: sensor=%s, train=%d",
                trackNode_IndexToName(sensor_info->node), sensor_info->train);
    }
    
    sensor_info_map->sensors[sensor_info->node].node = sensor_info->node;
    sensor_info_map->sensors[sensor_info->node].train = sensor_info->train;
}

void unassignSensor(SensorInfoMap* sensor_info_map, int node)
{
    if(node < 0 || node > TRACK_MAX) {
        ERROR("Tried to unassign invalid sensor=%d", node);
        return;
    }

    if(! isValidSensorInfo(&sensor_info_map->sensors[node])) {
        ERROR("Tried to unassign already unassigned sensor=%d", trackNode_IndexToName(node));
        return;
    }

    initializeSensorInfo(&sensor_info_map->sensors[node]);
}
