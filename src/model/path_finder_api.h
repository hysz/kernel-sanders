#pragma once

#include "errno.h"
#include "track_dict.h"
#include "track_consts.h"
#include "track_data_new.h"
#include "path.h"

ErrorCode getPath(TrackNode current_node, TrackNode destination_node, Path *path, Bool unreserved_only);
