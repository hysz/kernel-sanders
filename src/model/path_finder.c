#include "path_finder.h"
#include "track_data/track_node.h"
#include "track_data/track_data_new.h"
#include "track_dict.h"
#include "sorted_node_queue.h"

#include "kapi.h"
#include "nameserver_api.h"
#include "names.h"
#include "errno.h"
#include "common.h"
#include "debug.h"

#include "track_consts.h"
#include "track_helpers.h"
#include "path.h"

static void getPath(const TrackNode from, const TrackNode to, const track_node track[TRACK_MAX], Path *path, int unreserved_only)
{
    float dist[TRACK_MAX];
    float edge_dists[TRACK_MAX];
    int prev[TRACK_MAX];

    typedef enum {WHITE, GREY, BLACK} Color;
    Color colors[TRACK_MAX];

    // initialize
    int i = 0;
    for(; i < TRACK_MAX; ++i) {
        dist[i] = -1;
        edge_dists[i] = -1;
        prev[i] = -1;
        colors[i] = WHITE;
    }

    // initialize for dijkstra
    SortedTrackNodeQueue q;
    queueReset_STN(&q);
    queuePush_STN(&q, from, 0);
    dist[from] = 0;

    path->distance = 0;
    path->num_nodes = 0;

/*
    queuePush_STN(&q, 1, 1);
    queuePush_STN(&q, 0, 0);
    queuePush_STN(&q, 5, 5);
    queuePush_STN(&q, 6, 6);
    queuePush_STN(&q, 3, 3);
    queuePush_STN(&q, 4, 4);
    queuePush_STN(&q, 2, 2);
    queuePush_STN(&q, 7, 7);
    queuePush_STN(&q, 8, 8);
    queuePush_STN(&q, 9, 9);

    while( !queueEmpty_STN(&q) ) {
        ERROR(":: %d ::", queuePop_STN(&q)); 
    } 

    while(1) {}
*/

    // run dijkstra's algorithm to find the shortest path 
    TrackNode current = -1;
    while(! queueEmpty_STN(&q) ) {
        current = queuePop_STN(&q); 
        if(colors[current] != WHITE) continue;
        //ERROR(", %s", trackNode_IndexToName(current));

        if(current == to) break;
        
        int i = 0;
        for(; i < MAX_OUTGOING_EDGES; ++i) {
            const track_edge* edge = &(track[current].edge[i]);
            if(edge->dist == 0 || edge->broken) continue;
            TrackNode neighbour = (TrackNode)(edge->dest - &track[0]); 

            if( colors[neighbour] != WHITE) continue;
            if( dist[neighbour] >= 0 && dist[current] + edge->dist >= dist[neighbour] ) continue; 
            if( track[neighbour].reserved && track[neighbour].reserved != unreserved_only && dist[current] == 0) continue;
            
            edge_dists[neighbour] = edge->dist;
            dist[neighbour] = dist[current] + edge->dist;
            prev[neighbour] = current;

            ErrorCode push_error = queuePush_STN(&q, neighbour, dist[neighbour]);
            if(push_error) {
                ERROR("Dijkstra: Failed to push neighbour onto queue: %d", push_error);
            }
        }

        /*
            We want to incorporate synonyms as well.
            Con: We need to go ~30cm past merge/branch pairs before reversing.
            Strategy: Don't allow synonyms if:
            1. We're at a merge (meaning that we would reverse, set a swithc, then follow a branch)
            2. The chain of previous nodes has a merge less than 30cm back. 
         */
        Bool use_synonyms = TRUE;
        {
            int d = 0;
            int n = current;
            while(n > 0 && d < 300) {
                if(track[n].type == NODE_MERGE) {
                    use_synonyms = FALSE;
                    break;
                }

                d += edge_dists[n];
                n = prev[n];
            }
        }
        if(! use_synonyms) continue;

        for(i = 0; i < MAX_OUTGOING_EDGES; ++i) {
            const track_edge* edge = track[current].edge[i].reverse;
            if(! edge) continue;

            TrackNode synonym = (TrackNode)(edge->dest - &track[0]); 
            if(colors[synonym] != WHITE) continue;

            if( dist[synonym] >= 0 && dist[current] >= dist[synonym]) continue;            
            if( track[synonym].reserved && track[synonym].reserved != unreserved_only && dist[current] == 0) continue;

            edge_dists[synonym] = 0;
            dist[synonym] = dist[current];
            prev[synonym] = current;

            ErrorCode push_error = queuePush_STN(&q, synonym, dist[synonym]);
            if(push_error) {
                ERROR("Dijkstra: Failed to push neighbour onto queue: %d", push_error);
            }
        }

        colors[current] = BLACK;
    }
    if(current != to) {
        ERROR("\r\n#1 Failed to find path from %s to %s", trackNode_IndexToName(from), trackNode_IndexToName(to));
        return;
    }

    // Construct Path
    TrackNode s = to;
    path->nodes[0] = to;
    i = 1;
    // This version of the path is backwards
    while(s != from && i < TRACK_MAX) {
        //ERROR("-> %s", trackNode_IndexToName(s));
        path->nodes[i] = prev[s]; 
        s = prev[s];
        ++i;
    }
    if(i == TRACK_MAX) {
        ERROR("\r\n#2 Failed to find a path from %s to %s", trackNode_IndexToName(from), trackNode_IndexToName(to));
        return;
    }

    path->num_nodes = i;
    path->distance = dist[to];

    // Reverse Path
    int j = 0;
    int l = path->num_nodes - 1;
    for(; j <= l / 2; ++j) {
        TrackNode tmp = path->nodes[j];
        path->nodes[j] = path->nodes[l - j];
        path->nodes[l - j] = tmp;
    }

/*
    int k = 0;
    for(; k < i; ++k) {
        ERROR("\r\n-> %s", trackNode_IndexToName(path->nodes[k]));
    }
*/
}


void PathFinder()
{
    const int request_len = sizeof(TrackRequest); 
    const int reply_len = sizeof(Path); 

    RegisterAs(PATH_SERVER);

    const track_node *track;
    ErrorCode get_track_error = getTrack(&track);
    if(get_track_error) {
        ERROR("PathFinder(): Failed to getTrack (error=%d)", get_track_error);
        Exit();
    }
    DEBUG("Path Finder Track=0x%x", track);

    FOREVER {
        // Get Request
        TaskId tid = INVALID_TID;
        TrackRequest request;
        Receive(&tid, &request, request_len);

        // Process Request
        Path reply;
        switch(request.type) {
        case GET_PATH:
            // Get forward path
            getPath(request.from, request.to, track, &reply, request.unreserved_only);
            break;
        default:
            break; 
        }

        Reply(tid, &reply, reply_len);
    }
}
