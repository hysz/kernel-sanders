#pragma once

#include "track_dict.h" 
#include "errno.h"
#include "track_node.h"
#include "common.h"
#include "path.h"
#include "task.h"
#include "node_queue.h"
#include "tr_queue.h"

#define MAX_NODE_LOCK_COUNT 10
#define MAX_EDGE_LOCK_COUNT (2 * (MAX_NODE_LOCK_COUNT))

typedef enum {START_ROUTE, RESERVE, RELEASE, UPDATE_POSITION} TrackLockerRequest_Type;

struct TrackLockerInit {
    const track_node* track;
};
typedef struct TrackLockerInit TrackLockerInit;

struct TrackLockerRequest {
    TrackLockerRequest_Type type;
    union {
        TrackNode node;
        TrackNode from;
    };

    union {
        float distance;
        TrackNode to;
    };

    union {
        TaskId courier;
    };

    int train;
};
typedef struct TrackLockerRequest TrackLockerRequest;


struct TrackLockerReply {
    ErrorCode error;
};
typedef struct TrackLockerReply TrackLockerReply;

void TrackLocker();

struct Reserved {
    TrackNode from;
    TrackNode to;
    Bool empty;
};
typedef struct Reserved Reserved;

struct TState {
    TrainQueue reserve_waiters[TRACK_MAX];
    track_node* track;

    TrackNodeQueue node_queues[100];
    TaskId train_couriers[100];
    float reserved_dists[100];
    int last_assigned[100];
    int destinations[100];
    Bool is_waiting[100];
};
typedef struct TState TState;

struct TFriends {
    TaskId term_out_channel;
};
typedef struct TFriends TFriends;
