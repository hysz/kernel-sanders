#pragma once

#include "track_dict.h"
#include "track_data_new.h"
#include "errno.h"
#include "buffer.h"

#define SORTED_TRACK_NODE_QUEUE_SIZE TRACK_MAX

struct queueItem {
    TrackNode n;
    int dist;
};
typedef struct queueItem queueItem;

struct SortedTrackNodeQueue {
    queueItem data[SORTED_TRACK_NODE_QUEUE_SIZE];
    int next; 
    int len;
};
typedef struct SortedTrackNodeQueue SortedTrackNodeQueue;

void queueReset_STN(SortedTrackNodeQueue *q);
ErrorCode queuePush_STN(SortedTrackNodeQueue * q, TrackNode n, int dist);
int queueEmpty_STN(SortedTrackNodeQueue* q);
TrackNode queuePop_STN(SortedTrackNodeQueue* q);
queueItem queuePopItem_STN(SortedTrackNodeQueue* q);
