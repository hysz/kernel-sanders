#pragma once

#include "track_dict.h"
#include "track_data/track_data_new.h"
#include "errno.h"
#include "path.h"

typedef enum {GET_PATH, ROUTE_ME} TrackRequestType;

struct TrackRequest {
    TrackRequestType type;
    TrackNode from;
    TrackNode to; 
    int unreserved_only;
};
typedef struct TrackRequest TrackRequest;

struct TrackReply {
    ErrorCode error;
    Path *path;
};
typedef struct TrackReply TrackReply;
