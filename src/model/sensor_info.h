#pragma once

#include "common.h"
#include "track_data_new.h"

struct SensorInfo {
    int node;
    int train;
};
typedef struct SensorInfo SensorInfo;

void initializeSensorInfo(SensorInfo *sensor_info);
Bool isValidSensorInfo(const SensorInfo *sensor_info);

struct SensorInfoMap {
    SensorInfo sensors[TRACK_MAX];    
};
typedef struct SensorInfoMap SensorInfoMap;

void initializeSensorInfoMap(SensorInfoMap* sensor_info_map);
void assignSensor(SensorInfoMap* sensor_info_map, const SensorInfo *sensor_info);
void unassignSensor(SensorInfoMap* sensor_info_map, int node);
