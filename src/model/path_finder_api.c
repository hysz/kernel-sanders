#include "path_finder_api.h"
#include "names.h"
#include "kapi.h"
#include "nameserver_api.h"
#include "errno.h"
#include "task.h"
#include "debug.h"
#include "path_finder.h"

ErrorCode getPath(TrackNode current_node, TrackNode destination_node, Path *path, Bool unreserved_only)
{
    TaskId path_finder_tid = WhoIs(PATH_SERVER);
    if(! isValidTaskId(path_finder_tid) ) {
        ERROR("getPath failed to lookup path finder: %d", (ErrorCode)path_finder_tid);
        return (ErrorCode)path_finder_tid;
    }

    TrackRequest req;
    req.from = current_node;
    req.to =   destination_node;
    req.type = GET_PATH;
    req.unreserved_only = unreserved_only;

    ErrorCode send_error = Send(path_finder_tid, &req, sizeof(TrackRequest), path, sizeof(Path));
    if(send_error) {
        ERROR("getPath failed to get path: %d", send_error);
        return send_error;
    }

    if(path->nodes[0] != current_node || path->nodes[path->num_nodes - 1] != destination_node) {
        return GET_PATH_FAILED;
    } 

    return NO_ERROR;
}
