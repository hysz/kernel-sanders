#include "instruction.h"
#include "big_buffer.h"

void toggleOrientation(Orientation *o) 
{
    switch(*o) {
    case FORWARD:
        *o = BACKWARD;
        break;
    
    default:
        *o = FORWARD;
        break;
    }   
}


ErrorCode printInstructions(TaskId print_channel,
                           int num_instructions,
                           Instruction instructions[MAX_INSTRUCTIONS],
                           const TrainMeasurements *measurements)
{
    Buffer term;
    bufferReset(ANONYMOUS, &term);

    ERROR("\r\nNUM=%d", num_instructions);

    int i = 0;
    int line = 4;
    for(; i < num_instructions; ++i) {
        bufferPutStr(&term, "\033[");
        bufferPutInt(&term, line);
        bufferPutStr(&term, ";77H");

        switch(instructions[i].type) {
        case I_START:
            bufferPutStr(&term, "Speed <- ");
            if(measurements != 0) {
                bufferPutInt(&term, measurements->internal_speed);
            } else {
                bufferPut(&term, '#');
            }
            break;

        case I_STOP:
            bufferPutStr(&term, "Speed <- 0");
            break;

        case I_REVERSE:
            bufferPutStr(&term, "Reverse");
            break;

        case I_WAIT_FOR_SENSOR:
            bufferPutStr(&term, "Wait on ");
            bufferPutStr(&term, (char*)trackNode_IndexToName(instructions[i].node));
            break;

        case I_DELAY:
            bufferPutStr(&term, "Delay ");
            bufferPutInt(&term, instructions[i].delay);
            break;

        case I_SET_SWITCH:
            bufferPutStr(&term, "Sw ");
            bufferPutStr(&term, (char*)trackNode_IndexToName(instructions[i].node));
            bufferPutStr(&term, " <- ");
            bufferPut(&term, instructions[i].sw_position);
            break;

        case I_BRANCH: 
            // Output that we're branching
            bufferPutStr(&term, "Branch ");
            bufferPutStr(&term, (char*)trackNode_IndexToName(instructions[i].node));
            break;

        case I_MERGE:
            // Output that we're merging 
            bufferPutStr(&term, "Merge ");
            bufferPutStr(&term, (char*)trackNode_IndexToName(instructions[i].node));
            break;

        case I_ENTER:
            // Notify Enter
            bufferPutStr(&term, "Enter ");
            bufferPutStr(&term, (char*)trackNode_IndexToName(instructions[i].node)); 
            break;

        case I_EXIT:
            // Notify Exit
            bufferPutStr(&term, "Exit ");
            bufferPutStr(&term, (char*)trackNode_IndexToName(instructions[i].node));
            break;
        }

        bufferPutStr(&term, "\033[K");
        ++line;

        if(line % 10 == 0) {
            ErrorCode e = putB(print_channel, &term); 
            if(e) {
                ERROR("\r\nERROR: %d", e);
            }
        }
    }
    ErrorCode e = putB(print_channel, &term); 
    if(e) {
        ERROR("\r\nERROR: %d", e);
    }

    ERROR("\r\nBSize=%d", term.size);

    while(i++ < MAX_INSTRUCTIONS) {
        bufferPutStr(&term, "\033[");
        bufferPutInt(&term, line);
        bufferPutStr(&term, ";77H\033[K");
        ++line;
    }
    putB(print_channel, (Buffer*)&term); 

    return NO_ERROR;
}

// Returns number of stops
static void preprocess(
                           const Path* path,
                           const Orientation orientation,
                           const TrainMeasurements* train_measurements,
                           const track_node* track,
                           D stops[MAX_STOPS],
                          int *num_stops,
                           Switch switches[MAX_SWITCHES],
                          int *num_switches,
                          D edge_dists[])
{
    // Find distances where we need to stop
    int i = 1;
    int j = 0;
    int h = 0;
    D distance = 0;

    Orientation current_orientation = orientation;

    Bool hit_branch = FALSE;
    int branch_num = 0;
    TrackNode branch_node = 0;
    D synonym_offset = 0;
    for(; i < path->num_nodes; ++i) {
        const TrackNode node = path->nodes[i];
        const track_node* node_data = &track[node];

        // Check if node is synonym -> require a stop + reverse
        if(isSynonym(track, node, path->nodes[i-1])) {
            // Don't need to stop on first or last synonyms
            if(i <= 1 || i == path->num_nodes - 1) continue;
/*

            if(current_orientation == FORWARD) {
                distance += 1.5 * train_measurements->back_length;
            } else {
                distance += 1.5 * train_measurements->back_length + train_measurements->front_length;
            }
*/

            synonym_offset = 2.5 * (train_measurements->back_length + train_measurements->front_length);
            edge_dists[i-1] += synonym_offset;
            distance += synonym_offset;

            stops[j] = distance;  
            ++j;

            toggleOrientation(&current_orientation);
            
            // -1 indicates a change in direciton. A stop shouldn't be issued
            // if there is a direction change first!
            stops[j] = DIRECTION_CHANGE;
            ++j;

            switches[h].dist = DIRECTION_CHANGE;
            ++h;

        } else {
            // Find edge that was taken
            const Edge edge = getEdgeTaken(track, node, path->nodes[i-1]);
            if(! edge.edge) {
                ERROR("preprocess - No Edge Taken!");
            }
            edge_dists[i] = edge.edge->dist + synonym_offset;
            distance += edge_dists[i];
            synonym_offset = 0;
        }

        // It's possible that this branch is a synonym
        // So we'll want to do this processing *after* the 
        // above synonym check.
        // This is so that there will be a *DIRECTION_CHANGE* node
        // prior to this switch  
        if(hit_branch) {
            const Edge edge = getEdgeTaken(track, node, path->nodes[i-1]);
            if(! edge.edge) {
                ERROR("preprocess: Failed to get edge taken");
            }
            switches[h].dist = distance;
            switches[h].sw = branch_num;
            switches[h].node = branch_node;
            switches[h].pos = (edge.curved) ? 'C' : 'S';
            ++h;
        }

        if(node_data->type == NODE_BRANCH) {
            branch_num = node_data->num;
            branch_node = node;
            hit_branch = TRUE;
        } else {
            hit_branch = FALSE;
        }
    }
    if(path->close) stops[j] = distance; // also stop at destination
/*
    if(stops[j] != path->distance) {
        ERROR("preprocess(): destination stop not at path distance");
    } 
*/
    ++j;

    int p = 0;
        ERROR("\r\n\r\n");
    for(; p < j; ++p) {
        ERROR("\r\n%d", (int)stops[p]);
    }
    //while(1){}


    *num_stops = j;
    *num_switches = h; 
}

static int flipSwitches( Switch switches[MAX_SWITCHES],
                         int num_switches,
                         int *h,
                         int z,
                         Instruction instructions[MAX_INSTRUCTIONS])
{
        int a = *h;
        int num_instructions_added = 0;
        for(; a < num_switches; ++a ) {
            if(switches[a].dist == DIRECTION_CHANGE) {
                break;
            }

            instructions[z].type = I_SET_SWITCH;
            instructions[z].sw = switches[a].sw;
            instructions[z].sw_position = switches[a].pos;
            instructions[z].node = switches[a].node;
            ++z;

            num_instructions_added++;
        } 

        if(num_instructions_added > 0) {
            instructions[z].type = I_DELAY;
            instructions[z].delay = num_instructions_added * 30;
            instructions[z].node = switches[a].node;

            ++z;
        }


    *h = a;
    return z;
}

int compileInstructions(const int velocity,
                        const Path* path,
                        const Orientation orientation,
                        const TrainMeasurements* measurements,
                        const track_node* track,
                        Instruction instructions[MAX_INSTRUCTIONS],
                        Physics *physics)
{
    D stops[MAX_STOPS];
    Switch switches[MAX_SWITCHES];
    int num_stops = 0;
    int num_switches = 0;
    D edge_dists[path->num_nodes];
    preprocess(path, orientation, measurements, track, stops, &num_stops, switches, &num_switches, edge_dists);

    // Generate Instructions
    int i = 1;  // ignore first node -- we're already there!
    int j = 0;  // iterates stops 
    int h = 0; // iterates switches
    int z = 0; // instruction index
    D distance = 0;
    Bool set_init_speed = path->open ? FALSE : TRUE;

/*
    physics->j = 0;
    physics->v = velocity;
    physics->a = (velocity < measurements->velocity) ? measurements->acceleration : 0;
    physics->d = 0;//measurements->decceleration;
    physics->max_v = measurements->velocity;
    physics->max_a = measurements->acceleration;
    physics->max_d = measurements->decceleration;
*/

    for(; i < path->num_nodes; ++i) {
        if(z >= MAX_INSTRUCTIONS) {
            return ERR_TOO_MANY_INSTRUCTIONS;
        } 

        const TrackNode node = path->nodes[i];

        // Reverse if it synonym; start if train stopped
        if( isSynonym(track, node, path->nodes[i-1]) ) {
            // We expect to change direcitons on a synonym!
            if(j < num_stops && stops[j] == DIRECTION_CHANGE) {
               ++j; // there won't be a stop if the train has remained stationary and just reversed itself 
                //else continue;
            } else if(i > 1 && i < path->num_nodes - 1) {
                ERROR("Stops Expected Direction Change!");
            }

            if(h < num_switches && switches[h].dist == DIRECTION_CHANGE) {
                ++h;
            } else if(i > 1 && i < path->num_nodes - 1) {
                ERROR("Switches Expected Direction Change!");
            }
            z = flipSwitches( switches, num_switches, &h, z, instructions);

            instructions[z].type = I_DELAY;
            instructions[z].delay = 100;
            instructions[z].node = node;
            ++z;
            instructions[z].type = I_REVERSE;
            instructions[z].node = node;
            ++z;

            if(i < path->num_nodes - 1) { // only change speed if this isn't our last node 
                instructions[z].type = I_DELAY;
                instructions[z].delay = 15;
                instructions[z].node = node;
                ++z;

                instructions[z].type = I_START;
                instructions[z].node = node;
                ++z;

            }

            physics->a = measurements->acceleration;
            physics->v = 0;

            set_init_speed = TRUE;

            // After flipping switches, no more processing required 
            continue;

        } else if(! set_init_speed) {
            z = flipSwitches( switches, num_switches, &h, z, instructions);

            instructions[z].type = I_START;
            instructions[z].node = node;
            ++z;

            set_init_speed = TRUE;
        }

        // Find edge that was taken
        Edge edge = getEdgeTaken(track, node, path->nodes[i-1]);
        if(! edge.edge) {
            ERROR("Failed to compile instructions: Could not find edge taken.");
            return ERR_COULD_NOT_FIND_EDGE;
        }
        //const D edge_distance = edge.edge->dist;
        const D edge_distance = edge_dists[i];

        //ERROR("\r\na=%d; v=%d", (int)physics->a, (int)physics->v);
        // Handle Physics
        TimeInfo time_info;
        time_info.t_to_node = -1;       
        time_info.t_to_stop = -1;
        if(j < num_stops && stops[j] != DIRECTION_CHANGE) { 
            time_info = translatePhysics(edge_distance, stops[j] - distance, physics); 
        } else {
            time_info.t_to_node = translatePhysics_1(edge_distance, physics); 
        }

        // Stops
        if(time_info.t_to_stop >= 0) {
            // Handle Stop!
            instructions[z].type = I_DELAY;
            instructions[z].delay = 100 * time_info.t_to_stop;
            instructions[z].node = node;
            ++z;
        
            instructions[z].type = I_STOP;
            instructions[z].node = node;
            ++z;

            ++j;
        }
        distance += edge_distance; 

        // Wait until reach node
        if(track[node].type == NODE_SENSOR) {
            instructions[z].type = I_WAIT_FOR_SENSOR;  
            instructions[z].sensor = track[node].num;
            instructions[z].sensor_timeout = 100 * time_info.t_to_node;
            instructions[z].node = node;
            ++z;
        } else {
            instructions[z].type = I_DELAY;
            instructions[z].node = node;
            instructions[z].delay = 100 * time_info.t_to_node;
            ++z;
        
            switch(track[node].type) {
            case NODE_MERGE:
                instructions[z].type = I_MERGE;
                instructions[z].merge = track[node].num;
                instructions[z].node = node;
                ++z;
                break;

            case NODE_BRANCH:
                instructions[z].type = I_BRANCH;
                instructions[z].branch = track[node].num; 
                instructions[z].node = node;
                ++z;
                break;

            case NODE_ENTER:
                instructions[z].type = I_ENTER;
                instructions[z].branch = track[node].num; 
                instructions[z].node = node;
                ++z;
        
            case NODE_EXIT:
                instructions[z].type = I_EXIT;
                instructions[z].branch = track[node].num; 
                instructions[z].node = node;
                ++z;
                break;

            default:
                // Already handled
                break;
            }
        }
    }    

    return z;
}

