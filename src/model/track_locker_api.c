#include "track_locker_api.h"
#include "path.h"
#include "task.h"
#include "errno.h"
#include "kapi.h"
#include "track_locker.h"
#include "common.h"

/*
ErrorCode startRoute(const int train, const Path *p, const TaskId track_locker)
{
    TrackLockerRequest request;
    request.type = START_ROUTE;
    cpyN(&request.path, p, sizeof(Path));
    request.train = train; 

    return Send(track_locker, &request, sizeof(request), 0, 0);
}
*/

ErrorCode reserveNode(const int train, const TrackNode node, const TaskId my_courier, const TaskId track_locker)
{
    TrackLockerRequest request;
    request.type = RESERVE;
    request.node = node;
    request.courier = my_courier;
    request.train = train; 

    return Send(track_locker, &request, sizeof(request), 0, 0);
}

ErrorCode releaseNode(const int train, const TrackNode node, const float distance, const TaskId track_locker)
{
    TrackLockerRequest request;
    request.type = RELEASE;
    request.node = node;
    request.distance = distance;
    request.train = train; 

    return Send(track_locker, &request, sizeof(request), 0, 0);
}

ErrorCode updatePosition(const int train, const float distance, const TaskId track_locker)
{
    TrackLockerRequest request;
    request.type = UPDATE_POSITION;
    request.distance = distance;
    request.train = train; 

    return Send(track_locker, &request, sizeof(request), 0, 0);
}
