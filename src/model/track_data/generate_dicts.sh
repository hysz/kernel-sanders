#!/bin/bash

{
    echo "#pragma once"

    cat <<EOF
#if ! defined(TRACK_A) && ! defined(TRACK_B)
    #error "Must define a track: TRACK_A xor TRACK_B"
#endif

#if defined(TRACK_A) && defined(TRACK_B)
    #error "Must define a single track: TRACK_A xor TRACK_B"
#endif

    int trackNode_NameToIndex(char* str);
    const char* trackNode_IndexToName(int index);
EOF

    echo "typedef enum {"
    echo "      #if defined(TRACK_A)"
    cat track_data_new.c  | grep "name" | sed -r 's/track\[([0-9]+)\]\.name = "([[:alnum:]]+)".*/ \2 = \1,/' | head -144
    echo "      #elif defined(TRACK_B)"
    cat track_data_new.c  | grep "name" | sed -r 's/track\[([0-9]+)\]\.name = "([[:alnum:]]+)".*/ \2 = \1,/' | tail -140
    echo "      #endif"
    echo "   NUM_TRACK_NODES"
    echo "} TrackNode;"
} > track_dict.h

{
    echo '#include "track_dict.h"'
    echo '#include "common.h"'
    echo '#include "debug.h"'
    echo '#include "errno.h"'

    echo "int trackNode_NameToIndex(char* str) {"
    echo "      #if defined(TRACK_A)"
    cat track_data_new.c  | grep "name"  | sed -r 's/track\[([0-9]+)\]\.name = "([[:alnum:]]+)".*/ if(strcmp(str, "\2")) return \1;/' | head -144
    echo "      #elif defined(TRACK_B)"
    cat track_data_new.c  | grep "name"  | sed -r 's/track\[([0-9]+)\]\.name = "([[:alnum:]]+)".*/ if(strcmp(str, "\2")) return \1;/' | tail -140
    echo "      #endif"
    echo "      return UNRECOGNIZED_TRACK_INDEX;"
    echo "}"

    echo ""

    echo "const char* trackNode_IndexToName(int index) {"
    echo "switch(index) {"
    echo "      #if defined(TRACK_A)"
    cat track_data_new.c  | grep "name"  | sed -r 's/track\[([0-9]+)\]\.name = "([[:alnum:]]+)".*/ case \1: return "\2";/' | head -144
    echo "      #elif defined(TRACK_B)"
    cat track_data_new.c  | grep "name"  | sed -r 's/track\[([0-9]+)\]\.name = "([[:alnum:]]+)".*/ case \1: return "\2";/' | tail -140
    echo "      #endif"
    echo "  }"
    echo "   ERROR(\"UNRECOGNIZED INDEX: %d\", index);"
    echo "   return \"\";"
    echo "}"
} > track_dict.c
