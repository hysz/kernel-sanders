#pragma once

#include "path.h"
#include "task.h"
#include "errno.h"

// ErrorCode startRoute(const int train, const Path *p, const TaskId track_locker);
ErrorCode releaseNode(const int train, const TrackNode node, const float distance, const TaskId track_locker);
ErrorCode reserveNode(const int train, const TrackNode node, const TaskId my_courier, const TaskId track_locker);
ErrorCode updatePosition(const int train, const float distance, const TaskId track_locker);
