#pragma once

#include "track_dict.h"
#include "track_data_new.h"
#include "errno.h"
#include "buffer.h"

#define TRACK_NODE_QUEUE_SIZE TRACK_MAX

typedef enum {Q_NONE, Q_SWITCH, Q_STOP} QueueItemType;

struct queueItem {
    QueueItemType type;
    int n;
    union {
        int dist;
        char sw_position;
    };
};
typedef struct queueItem queueItem;

struct TrackNodeQueue {
    queueItem data[TRACK_NODE_QUEUE_SIZE];
    int next; 
    int len;
};
typedef struct TrackNodeQueue TrackNodeQueue;

void queueReset_TN(TrackNodeQueue *q);
ErrorCode queuePushSwitch_TN(TrackNodeQueue * q, TrackNode n, char sw_position);
ErrorCode queuePushStop_TN(TrackNodeQueue * q, TrackNode n, int dist);
ErrorCode queuePush_TN(TrackNodeQueue * q, TrackNode n, int dist);
int queueEmpty_TN(TrackNodeQueue* q);
TrackNode queuePop_TN(TrackNodeQueue* q);
queueItem queuePopItem_TN(TrackNodeQueue* q);
queueItem queuePeekItem_TN(TrackNodeQueue* q);
