#pragma once

#include "common.h"
#include "track_dict.h"
#include "track_data_new.h"
#include "errno.h"
#include "path.h"

struct Edge {
    const track_edge *edge;
    Bool curved; 
};
typedef struct Edge Edge;

int getNodeIndex(const track_node *track, const track_node* node);
Bool compareNodesByAddr(const track_node *track, const track_node* node_1, const track_node* node_2);
Bool isSynonym(const track_node* track, const TrackNode current, const TrackNode neighbour);
TrackNode getSynonym(const track_node* track, const TrackNode current);
Edge getEdgeTaken(const track_node* track, const TrackNode to, const TrackNode from);

// Wrapper for track server
ErrorCode getTrack(const track_node**);

void initTrack(track_node* track);

int getCommonAncestor(Path* A, Path* B);
