#include "track_locker.h"
#include "track_node.h"
#include "common.h"
#include "debug.h"
#include "errno.h"
#include "track_helpers.h"
#include "kapi.h"
#include "task.h"
#include "names.h"
#include "nameserver_api.h"
#include "buffer.h"
#include "io.h"
#include "node_queue.h"
#include "courier.h"
#include "train_physics.h"
#include "tr_queue.h"
#include "new_train.h"
#include "sensor_info.h"

// returns index into node_queues array
static void reserveMax(int train, TState *state, const TFriends *friends);
static Bool release(const int train, const TrackNode node, const float dist, TState *state, const TFriends *friends, Bool reserve_after_release);

static Bool reroute(const int train, const TrackNode from, const TrackNode to, TState *state, const TFriends *friends)
{
    ERROR("Reroute: Getting path!");
    Path path;
    getPath(from, to, &path, train);
    if(path.num_nodes == 0) {
        ERROR("No path from %s -> %s for train %d", trackNode_IndexToName(from), trackNode_IndexToName(to), train);
        return FALSE;
    }

    ERROR("Reroute: Emptying Existing Queue!");
    // Empty previous queue items
    TrackNodeQueue* q = &state->node_queues[train];
/*
    while(! queueEmpty_TN(q)) {
        queueItem item = queuePopItem_TN(q);
        if(state->track[item.n].reserved == train) {
            ERROR("Reroute Releasing %s", trackNode_IndexToName(item.n));
            release(train, item.n, item.dist, state, friends, FALSE);
        }
    }
    if(state->reserved_dists[train] != 0) {
        ERROR("Reroute: Expected reserved dist to be zero for train %d (dist=%d)", train, state->reserved_dists[train]);
    }
*/

    TrainMsg msg;
    msg.type = T_RESET;
    msg.from = from;
    msg.to = to;
    Send(state->train_couriers[train], &msg, sizeof(msg), 0, 0);

    ERROR("Reroute: Adding path to Queue!");
    // Add path to queue
    int i = 0;
    for(; i < path.num_nodes; ++i) {
        queueItem item;
        item.n = path.nodes[i];
        if(i == 0 || isSynonym(state->track, path.nodes[i], path.nodes[i-1])) {
            item.dist = 0;
        } else {
            Edge e = getEdgeTaken(state->track, item.n, path.nodes[i-1]);
            if(! e.edge) {
                ERROR("Failed to get edge taken (%s -> %s)", trackNode_IndexToName(path.nodes[i-1]), trackNode_IndexToName(item.n));
                continue;
            }

            item.dist = e.edge->dist;
        }
        queuePush_TN(q, item.n, item.dist);
    }

    ERROR("Reroute: Reserving!");
    // Reserve what I need
    state->destinations[train] = to;
    reserveMax(train, state, friends);

    return TRUE;
}

static Bool helper_isTrainBlockedOnNode(int train, TrackNode node, TState *state, const TFriends *friends)
{
    Bool blocked = FALSE;
    int i = 0;
    for(; i < state->reserve_waiters[node].len; ++i) {
        int waiting_train = queuePop_TR(&state->reserve_waiters[node]);
        queuePush_TR(&state->reserve_waiters[node], waiting_train); 

        if(waiting_train == train) {
            // We have a deadlock!
            blocked = TRUE;

            /*****
            Don't Break! We need to finish the loop so that the queue will 
            be back in the order it started!
            *****/
        }
    }

    return blocked;
}

static Bool isTrainBlockedOnNode(int train, TrackNode node, TState *state, const TFriends *friends)
{
    return helper_isTrainBlockedOnNode(train, node, state, friends) ||
            helper_isTrainBlockedOnNode(train, getSynonym(state->track, node), state, friends);    
}

static void forceReserve(int train, TrackNode node, TState *state, const TFriends *friends)
{
    state->track[node].reserved = train;
    state->track[node].fake_reserved = FALSE;

    TrackNode synonym = getSynonym(state->track, node);
    if(! state->track[synonym].reserved) {
        state->track[synonym].reserved = train;
        state->track[synonym].fake_reserved = TRUE;
    } else if(state->track[synonym].reserved != train) {
        ERROR("Train %d tried to reserve synonym owned by someone else: %d has %s",
            train, state->track[synonym].reserved, trackNode_IndexToName(synonym)); 
    }
    
    state->last_assigned[train] = node;

    // Notify Sensor Server
       


    // Notify Train's Courier
    TrainMsg msg;
    msg.type = T_CONTINUE;
    msg.node = node;
    Send(state->train_couriers[train], &msg, sizeof(msg), 0, 0);
}

static Bool reserve(int train, TrackNode node, TState *state, const TFriends *friends)
{
    if(! state->track[node].reserved || state->track[node].reserved == train) {
        forceReserve(train, node, state, friends);
        return TRUE;
    }

    int train_with_node = state->track[node].reserved;

    // Check to see if we have a cycle
    int last_assigned = state->last_assigned[train];
    if(last_assigned > 0) {
        ERROR("Checking last assigned");
        if(isTrainBlockedOnNode(train_with_node, last_assigned, state, friends)) {
            ERROR("Deadlock detected between %d (wants %s) and %d (wants %s)! Let's see if we can find an alternate route for %d",
                    train, trackNode_IndexToName(node), train_with_node, trackNode_IndexToName(last_assigned), train);

            // 1. See if we can get an alternate path for <train>
            if(! reroute(train, last_assigned, state->destinations[train], state, friends)) {
                ERROR("Sorry, no alternate route for %d. Let's see if we can find one for %d",
                        train, train_with_node);

                // 2.a Reroute the train currently holding the node
                if(! reroute(train_with_node, node, state->destinations[train_with_node], state, friends)) {
                    ERROR("Wow, we really suck! Trains %d and %d are deadlocked forever!!! Sorry Sujey :'(", train, train_with_node);
                }

                // 2.b Reserve <node> for <train>, since <train_with_node> has been rerouted 
                forceReserve(train, node, state, friends);
            }
            return FALSE; // False tells reserveMax to stop reserving. If we've just rerouted, we don't want it to continue reserving old path!
        }
    }

    if(! state->is_waiting[train]) {
        ERROR("Train %d will wait for %s until %d is finished", train, trackNode_IndexToName(node), train_with_node);
        // When the node is released, <train> will get it :) 
        queuePush_TR(&state->reserve_waiters[node], train); 
        queuePush_TR(&state->reserve_waiters[getSynonym(state->track, node)], train); 
        state->is_waiting[train] = TRUE;
    } else {
        ERROR("Train %d will NOT wait for %s as it is already waiting", train, trackNode_IndexToName(node), train_with_node);
    }
    
    return FALSE;
}

static void reserveMax(int train, TState *state, const TFriends *friends)
{
    float min_dist = 1.5 * getStoppingDistanceFromSpeed(train, 10);
    int index = train;
    while(! state->is_waiting[index] && state->reserved_dists[index] < min_dist && ! queueEmpty_TN(&state->node_queues[train])) {
        queueItem item = queuePopItem_TN(&state->node_queues[train]); 
        if(reserve(train, item.n, state, friends)) {
            state->reserved_dists[index] += item.dist;
        } else {
            break;
        }
    }
}

static void updateDistance(const int train, const int node, const float dist, TState *state, const TFriends *friends)
{
    state->reserved_dists[train] -= dist;
    if(state->reserved_dists[train] < 0) {
        ERROR("Zeroing negative reserved dist for train %d (unreserved %s, dist became %d)",
                train, trackNode_IndexToName(node), (int)state->reserved_dists[train]);
        state->reserved_dists[train] = 0;
    } else if(state->reserved_dists[train] == 0) {
        ERROR("Congrats, reserved dist hit 0 for train %d (unreserving %s)", train, trackNode_IndexToName(node));
    }
    
    // ERROR("Dist %d (removed %d; now at %d) (%d=%s)", train, (int)dist, (int)state->reserved_dists[train], node, trackNode_IndexToName(node));
}

static Bool release(const int train, const TrackNode node, const float dist, TState *state, const TFriends *friends, Bool reserve_after_release)
{
    if(! state->track[node].reserved) {
        ERROR("Tried to release unreserved track: train=%d, node=%s", train, trackNode_IndexToName(node));
        return FALSE;
    }
    if(state->track[node].reserved != train) {
        ERROR("Tried to release someone else's track: train=%d, node=%s", train, trackNode_IndexToName(node));
        return FALSE;
    }
    if(state->track[getSynonym(state->track, node)].reserved != train) {
        ERROR("Tried to release someone else's synonym: train=%d, node=%s", train, trackNode_IndexToName(getSynonym(state->track, node)));
        return FALSE;
    }
    
    TrackNode synonym = getSynonym(state->track, node);
    if(! state->track[synonym].fake_reserved) {
        // If I've reserved synonym, then I shouldn't release this node. BUT, I should mark it as <fake_reserved>
        state->track[node].fake_reserved = TRUE; 
        ERROR("Not unreserving %s as it is not fake reserved", trackNode_IndexToName(synonym));
    }
        
    // Reserve what I need
    updateDistance(train, node, dist, state, friends); 
    if(reserve_after_release) reserveMax(train, state, friends);

    if(! state->track[synonym].fake_reserved) return TRUE;

    // I can release this node + its synonym
    state->track[node].reserved = FALSE;
    state->track[synonym].reserved = FALSE;
    state->track[synonym].fake_reserved = FALSE;

    // ERROR("<<RELEASING>> train=%d, reserved dist=%d, input dist=%d, node=%s", train, (int)reserved_dists[train)], (int)dist, trackNode_IndexToName(node);

    // Is anyone waiting to reserve this node?
    if(queueEmpty_TR(&state->reserve_waiters[node])) return TRUE;

    // Reserve track for waiter
    int waiting_train = queuePop_TR(&state->reserve_waiters[node]); 
    int sanity_waiting_train = queuePop_TR(&state->reserve_waiters[getSynonym(state->track, node)]);
    if(waiting_train != sanity_waiting_train) {
        ERROR("Waiting Train (%d) != Train Waiting on Synonym (%d)", waiting_train, sanity_waiting_train);
        return FALSE;
    } 

    if(reserve(waiting_train, node, state, friends)) {
        state->is_waiting[waiting_train] = FALSE;

        // this reserve should succeed since we just released the node :)
        // continue reserveing <waiting_train>'s nodes in its path's queue
        reserveMax(waiting_train, state, friends);
    }
    return TRUE;
} 

void TrackLocker()
{
    // Initialize
    TFriends friends;
    friends.term_out_channel = WhoIs(TERM_PUT_SERVER);
    if(! isValidTaskId(friends.term_out_channel) ) { 
        ERROR("Trains failed to lookup term out channel: %d", friends.term_out_channel);
        Exit();
    } 
    // Spawn Courier
    TaskId my_courier = Create(MyPriority(), &Courier);
    if(! isValidTaskId(my_courier) ) { 
        ERROR("TrackLocker Courier Invalid TID=%d", (ErrorCode)my_courier);
        Exit();
    }   

    // Send Initialization Message
    {   
        CourierInitMsg message;
        message.forward = MyTid();
        message.message_size = sizeof(TrackLockerRequest);
        message.name = TRACK_LOCKER;
        message.reply_size = 0;
        message.msg_peek = (volatile char**)0;

        ErrorCode error = Send(my_courier, &message, sizeof(message), 0, 0); 
        if(error) {
            ERROR("TrainInstructor Init Failed %d", error);
            Exit();
        }   
    }
    
    TState state;

    const track_node* const_track = 0;
    ErrorCode get_track_error = getTrack(&const_track);
    if(get_track_error) {
        ERROR("TrackLocker: Failed to init");
        Exit();
    }
    state.track = (track_node*)const_track;

    // Set all edges to unreserved
    {
        int i = 0;
        for(; i < TRACK_MAX; ++i) {
            state.track[i].reserved = FALSE;
            state.track[i].fake_reserved = FALSE;
            queueReset_TR(&state.reserve_waiters[i]);
        }
    }
    // Initialize remaining state
    {
        int i = 0;
        for(; i < 100; ++i) {
            queueReset_TN(&state.node_queues[i]);
            state.train_couriers[i] = INVALID_TID;
            state.reserved_dists[i] = 0;
            state.last_assigned[i] = -1;
            state.destinations[i] = -1;
            state.is_waiting[i] = FALSE;
        }
    }

    // Handle Requests
    const int request_len = sizeof(TrackLockerRequest); 
    ERROR("Track Locker Ready!");
    FOREVER {
        // Get Request
        TrackLockerRequest request;
        Send(my_courier, 0,0, &request, request_len);

        switch(request.type) {
        case RESERVE:
            state.train_couriers[request.train] = request.courier;
            reserve(request.train, request.node, &state, &friends);
            break; 

        case START_ROUTE:
            reroute(request.train,
                    request.from,
                    request.to,
                    &state,
                    &friends
                   );
            break;

        case RELEASE:
            release(request.train,
                    request.node,
                    request.distance,
                    &state,
                    &friends,
                    TRUE
                    );
            break;

        case UPDATE_POSITION:
            updateDistance(request.train, -1, request.distance, &state, &friends); 
            reserveMax(request.train,
                        &state,
                        &friends
                      );
            break;

        default:
            ERROR("Unrecognized request type: %d", request.type);
            break;
        }
    }

    ERROR("TrackLocker exiting!!");
    Exit();
}

