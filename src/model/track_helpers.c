#include "track_helpers.h"
#include "common.h"
#include "track_dict.h"
#include "track_data_new.h"
#include "track_consts.h"
#include "names.h"
#include "nameserver_api.h"
#include "track_server.h"
#include "kapi.h"
#include "debug.h"
#include "clockserver_api.h"

int getNodeIndex(const track_node *track, const track_node* node)
{
    return (TrackNode)(node - &track[0]);
}

Bool compareNodesByAddr(const track_node *track, const track_node* node_1, const track_node* node_2)
{
    return ( getNodeIndex(track, node_1) == getNodeIndex(track, node_2) );
}

Bool isSynonym(const track_node* track, const TrackNode current, const TrackNode neighbour)
{
    int i = 0;
    for(i = 0; i < MAX_OUTGOING_EDGES; ++i) { 
        const track_edge* edge = track[current].edge[i].reverse;
        if(! edge) continue;
        if( compareNodesByAddr(track, edge->dest, &track[neighbour]) ) return TRUE;
    }   

    return FALSE;
}

TrackNode getSynonym(const track_node* track, const TrackNode current)
{
    int i = 0;
    for(i = 0; i < MAX_OUTGOING_EDGES; ++i) { 
        const track_edge* edge = track[current].edge[i].reverse;
        if(! edge) continue;
        return (TrackNode)(edge->dest - track);
    }   

    ERROR("Failed to get synonym for %s", trackNode_IndexToName(current));

    return -1;
}

Edge getEdgeTaken(const track_node* track, const TrackNode to, const TrackNode from)
{
    Edge edge;
    edge.edge = 0;
    edge.curved = FALSE;
    
    // Find edge that was taken
    int k = 0;
    for(; k < 2; ++k) {
        const track_edge *e = &track[from].edge[k];
        if(compareNodesByAddr(track, e->dest, &track[to])) {
            edge.edge = e;
            edge.curved = (k == DIR_CURVED); 
        }
    }

    return edge;
}

ErrorCode getTrack(const track_node** track)
{
    TaskId track_server_tid = WhoIs(TRACK_SERVER);
    while(! isValidTaskId(track_server_tid) ) {
        ERROR("Failed to lookup track server: %d. Trying again...", (ErrorCode)track_server_tid);
        track_server_tid = WhoIs(TRACK_SERVER);
    }

    TrackRequest req;
    req.type = GET_TRACK;
        
    TrackReply reply;
    Send(track_server_tid, &req, sizeof(req), &reply, sizeof(reply)); 
    if(reply.error) {
        return reply.error;
    }   

    *track = reply.track;
    return NO_ERROR;
}

void initTrack(track_node* track)
{
    #if defined(TRACK_A) && defined(TRACK_B)
        ERROR("Both Tracks Defined!");
    #elif defined(TRACK_A)
        init_tracka(track);
    #elif defined(TRACK_B)
        init_trackb(track);
    #endif    
}

int getCommonAncestor(Path* A, Path* B) {
    Path* smallerPath;
    Path* longerPath;
    if (A->num_nodes > B->num_nodes) {
        smallerPath = B;
        longerPath = A; 
    } else {
        smallerPath = A;
        longerPath = B;
    }
    DEBUG("Path A LENGTH: %d", A->num_nodes);
    DEBUG("SmallestPath Length: %d", smallerPath->num_nodes);
    DEBUG("LongestPath length: %d", longerPath->num_nodes);

    int i;

    for(i = longerPath->num_nodes - 1; i >= smallerPath->num_nodes; i--) {
        //DEBUG("CA: Comparing small[%d]:%d: to long[%d]:%d:", smallerPath->num_nodes - 1, smallerPath->nodes[smallerPath->num_nodes - 1], i, longerPath->nodes[i]);
        if (smallerPath->nodes[smallerPath->num_nodes - 1] == longerPath->nodes[smallerPath->num_nodes + i]) {
            return smallerPath->num_nodes - 1;
        } 
    }

    for (i = smallerPath->num_nodes - 1; i >= 0; i--) {
        //DEBUG("CA: Comparing small[%d]:%d: to long[%d]:%d:", i, smallerPath->nodes[i], i, longerPath->nodes[i]);
        if (smallerPath->nodes[i] == longerPath->nodes[i]) {
            return i;
        }
    }

    return NO_COMMON_ANCESTOR_ERR;
}

