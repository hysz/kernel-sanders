#pragma once

#include "errno.h"

#define TRAIN_QUEUE_SIZE 5

struct TrainQueue {
    int data[TRAIN_QUEUE_SIZE];
    int next; 
    int len;
};
typedef struct TrainQueue TrainQueue;

void queueReset_TR(TrainQueue *q);
ErrorCode queuePush_TR(TrainQueue * q, int train);
int queueEmpty_TR(TrainQueue* q);
int queuePop_TR(TrainQueue* q);
