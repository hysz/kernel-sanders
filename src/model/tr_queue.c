#include "tr_queue.h"
#include "buffer.h"
#include "debug.h"

void queueReset_TR(TrainQueue* q)
{
    q->next = 0;
    q->len = 0;
}

ErrorCode queuePush_TR(TrainQueue* q, int train)
{
    if(q->len == TRAIN_QUEUE_SIZE) {
        return TRAIN_QUEUE_FULL; 
    }

    // Extend Queue
    q->data[q->next] = train;

    q->next = (q->next + 1) % TRAIN_QUEUE_SIZE;
    if(q->len < TRAIN_QUEUE_SIZE) q->len++;

    return NO_ERROR;
}

int queueEmpty_TR(TrainQueue* q)
{
    return (q->len == 0); 
}

int queuePop_TR(TrainQueue* q)
{
    if( queueEmpty_TR( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop item from empty TR Queue");
    }

    int k = q->next - q->len;
    if(k < 0) k += TRAIN_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;
    return q->data[k];
}
