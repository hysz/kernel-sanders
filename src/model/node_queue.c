#include "node_queue.h"
#include "buffer.h"
#include "debug.h"

void queueReset_TN(TrackNodeQueue* q)
{
    q->next = 0;
    q->len = 0;
}

ErrorCode queuePushStop_TN(TrackNodeQueue* q, TrackNode n, int dist)
{
    if(q->len == TRACK_NODE_QUEUE_SIZE) {
        return TRACK_NODE_QUEUE_FULL; 
    }

    // Extend Queue
    q->data[q->next].type = Q_STOP; 
    q->data[q->next].n = n; 
    q->data[q->next].dist = dist;

    q->next = (q->next + 1) % TRACK_NODE_QUEUE_SIZE;
    if(q->len < TRACK_NODE_QUEUE_SIZE) q->len++;

    return NO_ERROR;
}

ErrorCode queuePushSwitch_TN(TrackNodeQueue* q, TrackNode n, char sw_position)
{
    if(q->len == TRACK_NODE_QUEUE_SIZE) {
        return TRACK_NODE_QUEUE_FULL; 
    }

    // Extend Queue
    q->data[q->next].type = Q_SWITCH; 
    q->data[q->next].n = n; 
    q->data[q->next].sw_position = sw_position;

    q->next = (q->next + 1) % TRACK_NODE_QUEUE_SIZE;
    if(q->len < TRACK_NODE_QUEUE_SIZE) q->len++;

    return NO_ERROR;
}

ErrorCode queuePush_TN(TrackNodeQueue* q, TrackNode n, int dist)
{
    if(q->len == TRACK_NODE_QUEUE_SIZE) {
        return TRACK_NODE_QUEUE_FULL; 
    }

    // Extend Queue
    q->data[q->next].type = Q_NONE; 
    q->data[q->next].n = n; 
    q->data[q->next].dist = dist;

    q->next = (q->next + 1) % TRACK_NODE_QUEUE_SIZE;
    if(q->len < TRACK_NODE_QUEUE_SIZE) q->len++;

    return NO_ERROR;
}

int queueEmpty_TN(TrackNodeQueue* q)
{
    return (q->len == 0); 
}

TrackNode queuePop_TN(TrackNodeQueue* q)
{
    if( queueEmpty_TN( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop from empty TN Queue");
        return 69;
    }

    int k = q->next - q->len;
    if(k < 0) k += TRACK_NODE_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;
    return q->data[k].n;
} 

queueItem queuePopItem_TN(TrackNodeQueue* q)
{
    if( queueEmpty_TN( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop item from empty TN Queue");
    }

    int k = q->next - q->len;
    if(k < 0) k += TRACK_NODE_QUEUE_SIZE;

    q->len--;
    if(q->len < 0) q->len = 0;
    return q->data[k];
}

queueItem queuePeekItem_TN(TrackNodeQueue* q)
{
    if( queueEmpty_TN( q ) ) {
        // You should check if empty vefore calling
        ERROR("Tried to pop item from empty TN Queue");
    }

    int k = q->next - q->len;
    if(k < 0) k += TRACK_NODE_QUEUE_SIZE;
    return q->data[k];
}
