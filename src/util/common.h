#pragma once

#define FOREVER while(1)
#define TRUE 1
#define FALSE 0

typedef short Bool;

#define PROGRAM_OFFSET 2195456 /* 0x00218000 */

#define ASS __asm__ __volatile__

// src must be null-terminated
void cpy(char *dest, char *src);

// src does not have to be null-terminated
// Returns number of characters copied from src to dest
int cpyN(void *dest, const void *src, int n);

void iToHexA(char *dest, int n);
void iToDecA(char *dest, int n);

int strcmp(const char* s1, const char* s2);

// Print clock cycles 
void PCC();
