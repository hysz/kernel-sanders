#include "common.h"
#include "ts7200.h"
#include "debug.h"

// src must be null-terminated
void cpy(char *dest, char *src)
{
    // operator= returns the value that was set
    // So when the value is '\0', it will return all zeros
    // thus the loop will exit
    while( (*dest++ = *src++) );
}

// src does not have to be null-terminated
int cpyN(void *dest, const void *src, int n)
{
    int i = n;
    
    if(n % sizeof(int) == 0) {
        for(; n / sizeof(int) > 0; n -= sizeof(int)) {
            *(int *)dest = *(int *)src;
            dest += sizeof(int);
            src += sizeof(int);
        }
    }

    for(; n > 0; n -= 1) {
        *(char *)dest++ = *(char *)src++;
    }

    return i;
}

void iToHexA(char *dest, int n)
{
    char* h = dest + 8;
    
    for(; n > 0; n /= 16) {
        int v = n % 16; 
    
        if(v <= 9) *h-- = v + '0';
        else       *h-- = v - 10 + 'A'; 
    }   

    while(h > dest) {
        *h-- = '0';
    }   
}

void iToDecA(char *dest, int n)
{
    char* h = dest + 1;
    if(n < 10) *h-- = ' ';

    for(; n > 0; n /= 10) *h-- = (n % 10) + '0';
}

void PCC()
{
    //unsigned int time = *(int*)(TIMER3_BASE + VAL_OFFSET) * 1000/508;
    //DEBUG("%d", time);
}

int strcmp(const char* s1, const char* s2)
{
    int i = 0;
    while(s1[i] != '\0' && s2[i] != '\0') {
        if(s1[i] != s2[i]) return FALSE;
        i++;
    }
    // verify strings end at same char
    if(s1[i] != s2[i]) return FALSE;
    return TRUE;
}
