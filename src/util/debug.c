#include "debug.h"
#include "names.h"
#include "nameserver_api.h"
#include "io.h"
#include "bwio.h"
#include "buffer.h"

static char c2x( char ch ) {
	if ( (ch <= 9) ) return '0' + ch;
	return 'a' + ch - 10;
}

static int bufputx( Buffer *buf, char c ) {
	char chh, chl;

	chh = c2x( c / 16 );
	chl = c2x( c % 16 );
	bufferPut( buf, chh );
	return bufferPut( buf, chl );
}

int bufputr( Buffer *buf, unsigned int reg ) {
	int byte;
	char *ch = (char *) &reg;

	for( byte = 3; byte >= 0; byte-- ) bufputx( buf, ch[byte] );
	return bufferPut( buf, ' ' );
}

int bufputstr( Buffer *buf, char *str ) {
	while( *str ) {
		if( bufferPut( buf, *str ) < 0 ) return -1;
		str++;
	}
	return 0;
}

void bufputw( Buffer *buf, int n, char fc, char *bf ) {
	char ch;
	char *p = bf;

	while( *p++ && n > 0 ) n--;
	while( n-- > 0 ) bufferPut( buf, fc );
	while( ( ch = *bf++ ) ) bufferPut( buf, ch );
}

static int bwa2d( char ch ) {
	if( ch >= '0' && ch <= '9' ) return ch - '0';
	if( ch >= 'a' && ch <= 'f' ) return ch - 'a' + 10;
	if( ch >= 'A' && ch <= 'F' ) return ch - 'A' + 10;
	return -1;
}

static char bwa2i( char ch, char **src, int base, int *nump ) {
	int num, digit;
	char *p;

	p = *src; num = 0;
	while( ( digit = bwa2d( ch ) ) >= 0 ) {
		if ( digit > base ) break;
		num = num*base + digit;
		ch = *p++;
	}
	*src = p; *nump = num;
	return ch;
}

static void bwui2a( unsigned int num, unsigned int base, char *bf ) {
	int n = 0;
	int dgt;
	unsigned int d = 1;
	
	while( (num / d) >= base ) d *= base;
	while( d != 0 ) {
		dgt = num / d;
		num %= d;
		d /= base;
		if( n || dgt > 0 || d == 0 ) {
			*bf++ = dgt + ( dgt < 10 ? '0' : 'a' - 10 );
			++n;
		}
	}
	*bf = 0;
}

static void bwi2a( int num, char *bf ) {
	if( num < 0 ) {
		num = -num;
		*bf++ = '-';
	}
	bwui2a( num, 10, bf );
}

static void format( Buffer *buf, char *fmt, va_list va ) {
	char bf[12];
	char ch, lz;
	int w;
	
	while ( ( ch = *(fmt++) ) ) {
		if ( ch != '%' )
			bufferPut( buf, ch );
		else {
			lz = 0; w = 0;
			ch = *(fmt++);
			switch ( ch ) {
			case '0':
				lz = 1; ch = *(fmt++);
				break;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				ch = bwa2i( ch, &fmt, 10, &w );
				break;
			}
			switch( ch ) {
			case 0: return;
			case 'c':
				bufferPut( buf, va_arg( va, char ) );
				break;
			case 's':
				bufputw( buf, w, 0, va_arg( va, char* ) );
				break;
			case 'u':
				bwui2a( va_arg( va, unsigned int ), 10, bf );
				bufputw( buf, w, lz, bf );
				break;
			case 'd':
				bwi2a( va_arg( va, int ), bf );
				bufputw( buf, w, lz, bf );
				break;
			case 'x':
				bwui2a( va_arg( va, unsigned int ), 16, bf );
				bufputw( buf, w, lz, bf );
				break;
			case '%':
				bufferPut( buf, ch );
				break;
			}
		}
	}
}


void createDebugText(Buffer* buf, int kernelMode, int type, char* file, int lineNum, char* fmt, va_list* va) {
    int* numDebugLines = (int*) 0x1fdcfc4;
    int debugLine = (*numDebugLines % DEBUG_MAX_LINES) + DEBUG_LINE_NUM;
    bufferPutStr(buf, "\033[");
    bufferPutInt(buf, debugLine);
    bufferPutStr(buf, ";0H\033[2K");
    if (kernelMode == 1) {
        bufferPut(buf, 'K');
    }
    if (type == ERROR_TYPE) {
        bufferPutStr(buf, "E: ");
    } else {
        bufferPutStr(buf, "W: ");
    }

    format( buf, fmt, *va );

    bufferPutStr(buf, "~~");
    bufferPutStr(buf, file);
    bufferPutStr(buf, ":");
    bufferPutInt(buf, lineNum);
    bufferPutStr(buf, " [");
    bufferPutInt(buf, *numDebugLines);
    bufferPutStr(buf, "]");

    (*numDebugLines)++;
}

void bwdebug(int kernelMode, int type, char* file, int lineNum, char* fmt, ...) {
    Buffer buf;
    bufferReset(ERROR_LOG, &buf);
    va_list va;
    va_start(va, fmt);
    createDebugText(&buf, kernelMode, type, file, lineNum, fmt,  &va);
    va_end(va);

    while(!bufferEmpty(&buf)) {
        bwputc(1, bufferPop(&buf));    
    } 
}

void debug(int type, char* file, int lineNum, char* fmt, ...) {
    TaskId output_channel = WhoIs(TERM_PUT_SERVER);    

    Buffer buf;
    bufferReset(ERROR_LOG, &buf);
    va_list va;
    va_start(va, fmt);
    createDebugText(&buf, 0, type, file, lineNum, fmt,  &va);
    va_end(va);

    putB(output_channel, &buf);
}

