#pragma once
#include "task.h"
#include "task_set.h"
#include "message.h"

#define ERROR_TYPE 1
#define DEBUG_TYPE 0

#define DEBUG_LINE_NUM 22
#define DEBUG_MAX_LINES 20

#define DEBUG_CHANNEL COM2

#ifdef DEBUG_MODE
    #define DEBUG(...) debug(0, __FILE__, __LINE__, __VA_ARGS__)
    #define KDEBUG(...) bwdebug(1, 0, __FILE__, __LINE__, __VA_ARGS__)
    #define IODEBUG(...) bwdebug(0, 0, __FILE__, __LINE__, __VA_ARGS__)
#else
    #define DEBUG(...)
    #define KDEBUG(...)
    #define IODEBUG(...)
#endif


#ifdef ERROR_MODE
    #define ERROR(...) debug(1, __FILE__, __LINE__, __VA_ARGS__)
    #define KERROR(...) bwdebug(1, 1, __FILE__, __LINE__, __VA_ARGS__)
    #define IOERROR(...) bwdebug(0, 1, __FILE__, __LINE__, __VA_ARGS__)
#else
    #define ERROR(...)
    #define KERROR(...)
    #define IOERROR(...)
#endif


void bwdebug(int kernelMode, int type, char* file, int lineNum, char* fmt, ...);

void debug(int type, char* file, int lineNum, char* fmt, ...);

void initDebug();
