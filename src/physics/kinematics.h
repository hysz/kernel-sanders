#pragma once

typedef float J; // Jerk
typedef float A; // Acceleration
typedef float V; // Velocity
typedef float D; // Distance
typedef float T; // Time

V getV_1(A a, D d, V init_v, V max_v);
D getD_1(V v, V v_at_d, A a);
T getT_1(V v, V v_at_t, A a);
A getA_1(D distance, V constant_v, T time_at_variable_t);

