#include "train_physics.h"
#include "kinematics.h"
#include "debug.h"
#include "math.h"

void getTrainMeasurements(int train_number, TrainMeasurements* m)
{
    m->jerk = 0;
    m->acceleration = 0;
    m->decceleration = 0;
    m->velocity = 0;
    m->front_length = 21;
    m->back_length = 140;
    m->internal_speed = 0;
    // m->curveRatio = 0;

    switch(train_number) {
    case 45:
        m->internal_speed = 7;
        m->velocity = 300;
        m->decceleration = 93;
        m->acceleration = 83;
        break;

    case 12:
        m->internal_speed = 8;
        m->velocity = 364;
        m->decceleration = 164;
        m->acceleration = 164;
        break;
    
    case 24:
        m->internal_speed = 8;
        m->velocity = 192;
        m->decceleration = 82;
        m->acceleration = 82;
        break;

    }

/*
    remeasureA(m->acceleration, m);
    remeasureD(m->decceleration, m);
*/
    //ERROR("\r\n\r\n\r\nDA=%d; DD=%d\r\n", (int)m->dist_to_accel, (int)m->dist_to_deccel); 
}


float getStoppingDistanceFromSpeed(int trNum, int trSpeed) {
    switch(trNum) {
        case 12:
            switch(trSpeed) {
                case 10:
                    return 665;
                case 9:
                    return 533;
                case 8:
                    return 404;
            }

        case 24:
            switch(trSpeed) {
                case 10:
                    return 414;
                case 9:
                    return 340;
                case 8:
                    return 226;
            }

        case 45:
            switch(trSpeed) {
                case 10:
                    return 631;

            }

        case 58:
            switch(trSpeed) {
                case 10:
                    return 490;

            }

        case 62:
            switch(trSpeed) {
                case 10:
                    return 681;
                case 9:
                    return 617;
                case 8:
                    return 538;
            }
    }
    return -1;
}


float getMaxVelocityFromSpeed(int trNum, int trSpeed) {
    switch(trNum) {
        case 12:
            switch(trSpeed) {
                case 10:
                    return 520;
                case 9:
                    return 404;
                case 8:
                    return 364;
            }

        case 24:
            switch(trSpeed) {
                case 10:
                    return 303;
                case 9:
                    return 242;
                case 8:
                    return 192;
            }
        case 45:
            switch(trSpeed) {
                case 10:
                    return 358;

            }

        case 58:
            switch(trSpeed) {
                case 10:
                    return 303;

            }

        case 62:
            switch(trSpeed) {
                case 10:
                    return 455;
                case 9:
                    return 428;
                case 8:
                    return 364;
            }
    }
    return -1;
}

float getMaxVelocity(int train_number, float stoppingDistance)
{
    float A = 0, B = 0, C = 0, D = 0;
    int v = 0;
    switch(train_number) {
        case 12:
            A = 0.000001214;
            B = -0.001594;
            C = 1.298;
            D = -0.2268;
            break;

        case 24:
            A = 0.0000002943;
            B = -0.000789;
            C = 0.958;
            D = 9.9;
            break;

        case 45:
            A = 0.0000000278;
            B = -0.000306;
            C = 0.7466;
            D = 10.32;
            break;

        case 58:
            A = 1.3293;
            B = -0.0017;
            C = 1.148;
            D = 6.533;
            break;

        case 62:
            A = -0.0000001026;
            B = 0.000163;
            C = 0.593;
            D = 14.81;
            break;
    }

    v = calcPolynomial(stoppingDistance, A, B, C, D);
    return v; 
}

float getStoppingDistance(int train_number, float velocity)
{
    float A = 0, B = 0, C = 0, D = 0;
    float sd = 0;
    switch(train_number) {
        case 12:
            A = -0.000002017;
            B = 0.0024;
            C = 0.5545;
            D = 5.799;
            break;

        case 24:
            A = 0.000004475;
            B = -0.000106;
            C = 1.1108;
            D = -11.57;
            break;

        case 45:
            A = 0.000001232;
            B = 0.000795;
            C = 1.288;
            D = -12.174;
            break;

        case 58:
            A = -0.000004085;
            B = 0.004939;
            C = 0.4078;
            D = 3.1405;
            break;

        case 62:
            A = -0.00000004615;
            B = -0.000164;
            C = 1.598;
            D = -20.302;
            break;
    }

    sd = calcPolynomial(velocity, A, B, C, D);
    return sd; 
}


float getStoppingTimeFromDistance(int trNum, float stoppingDistance){

}

float getStoppingTime(int train_number, float stoppingDistance)
{
    float A = 0, B = 0, C = 0, D = 0;
    int st = 0;
    switch(train_number) {
        case 12:
            break;

        case 24:
            A = 0.0000000221;
            B = -0.00001839;
            C = 0.00825;
            D = 0.427;
            break;

        case 45:
            break;

        case 58:
            A = 0.0000000343;
            B = -0.0000299;
            C = 0.00994;
            D = 0.564;
            break;

        case 62:
            break;
    }

    st = calcPolynomial(stoppingDistance, A, B, C, D);
    return st; 
}
float calcPolynomial(int x, float A, float B, float C, float D) {
    float y = A;

    y = y * x + B;
    y = y * x + C;
    y = y * x + D;

    return y;
}

float getVelocityDuringDeceleration(float t, float v0, int trainNumber)
{
    float stopping_distance = getStoppingDistance(trainNumber, v0);
    float u = v0 * t / (2 * stopping_distance); 

    float term1 = 2 * v0 * power(u, 3); 
    float term2 = -3 * v0 * power(u, 2);
    float term3 = v0;

    return term1 + term2 + term3;
}

float getDistanceDuringDeceleration(float t, float v0, int trainNumber)
{
    float stopping_distance = getStoppingDistance(trainNumber, v0);
    float u = v0 * t / (2 * stopping_distance); 

    float term1 = stopping_distance * power(u, 4);
    float term2 = -2 * stopping_distance * power(u, 3);
    float term3 = 2 * stopping_distance * u;

    return term1 + term2 + term3;
}

//time is in seconds
//Uses formula from jerk.pdf
float getDistanceDuringAcceleration(float t, float v0, int trainNumber, int speed) {
/*
    int vMax = getTrainVelocityFromSpeed(trainNumber, 10);
    float d = getStoppingDistance(trainNumber, vMax);

    float dRatio = (vMax * t) / (2.0 * d);

    float term1 = d * power(dRatio, 4.0);

    float term2 = -2.0 * d * power(dRatio, 3.0);

    float term3 = 2.0 * d * dRatio;    

    return term1 + term2 + term3;
*/

    float max_velocity = getMaxVelocityFromSpeed(trainNumber, speed); 
    float velocity_delta = max_velocity - v0;

    float stopping_distance = getStoppingDistanceFromSpeed(trainNumber, speed);
    float time_to_max_velocity = (stopping_distance * 2) / velocity_delta; // manipulate x(t) to solve this 

    // Formula From Jerk.pdf
    float u = t / time_to_max_velocity; 

    float term1 = -0.5 * time_to_max_velocity * velocity_delta * power(u, 4); 
    float term2 = time_to_max_velocity * velocity_delta * power(u, 3);
    float term3 = time_to_max_velocity * v0 * u;
    // term3 == 0 and term4 == 0    
    
    return term1 + term2 + term3;
}

float getVelocityDuringAcceleration(float t, float v0, int trainNumber, int speed) {
/*
    int vMax = getTrainVelocityFromSpeed(trainNumber, speed);
    float d = getStoppingDistance(trainNumber, vMax);

    float dRatio = (vMax * t) / (2.0 * d);
    
    float term1 = 2.0 * vMax * power(dRatio, 3.0);
    
    float term2 = -3.0 * vMax * power(dRatio, 2.0);

    float term3 = vMax;

    int v = vMax - (term1 + term2 + term3);

    if (v <= 0) {
        return vMax;
    } 

    return v;
*/

    float max_velocity = getMaxVelocityFromSpeed(trainNumber, speed); 
    float velocity_delta = max_velocity - v0;

    float stopping_distance = getStoppingDistanceFromSpeed(trainNumber, speed);
    float time_to_max_velocity = (float)(stopping_distance * 2) / velocity_delta; // manipulate x(t) to solve this 

    // Formula From Jerk.pdf
    float u = t / time_to_max_velocity; 

    float term1 = -2.0 * velocity_delta * power(u, 3); 
    float term2 = 3.0 * velocity_delta * power(u, 2); 
    float term3 = v0;
    // term3 == 0
    
    return term1 + term2 + term3;
}

float power(float n, float exp) {
    float val = n;
    int i = 0;
    for (i = 0; i < exp - 1; i++) {
        val *= n;
    }

    return val;
}

float Abs(float n) {
    return n >= 0.0 ? n : n * -1;
}

void remeasureA(A a, TrainMeasurements *m)
{
    m->acceleration = a;
    m->time_to_accel = (m->velocity / m->acceleration); 
    m->dist_to_accel = (m->velocity * m->velocity) / (2 * m->acceleration);
}

void remeasureD(D d, TrainMeasurements *m)
{
    m->decceleration = d;
    m->time_to_deccel = (m->velocity / m->decceleration);
    m->dist_to_deccel = (m->velocity * m->velocity) / (2 * m->decceleration);
}

void remeasureV(V v, TrainMeasurements *m)
{
    m->velocity = v;
    remeasureA(m->acceleration, m);
    remeasureD(m->decceleration, m);
}

T translatePhysics_1(const D distance, Physics *physics)
{
    T time_to_max = getT_1(physics->v, physics->max_v, physics->a); 
    T dist_to_max = getD_1(physics->v, physics->max_v, physics->a);
    T time_to_node = (distance - dist_to_max) / physics->max_v;

    // Update velocity
    if(physics->a != 0 && physics->v < physics->max_v) {
        physics->v = getV_1(physics->a, distance, physics->v, physics->max_v);     
    }

    // Update acceleration
    if(physics->v == physics->max_v) {
        physics->a = 0;
    }

    return time_to_max + time_to_node;
}

void translatePhysics_2(const A a, Physics *physics)
{
    physics->a = a;   
}

T whenToStop(D distance, const Physics *p)
{
    Physics physics = *p;
    D max_stop_distance = getD_1(0, physics.max_v, physics.max_d);  /* x -1 because <d> is negative */
    D distance_to_max   = getD_1(physics.v, physics.max_v, physics.a);
    
    if(distance_to_max + max_stop_distance <= distance) {
        T time_to_max = translatePhysics_1(distance_to_max, &physics);
        T max_to_issue_stop = (distance - max_stop_distance - distance_to_max) / physics.max_v;

        ERROR("CASE1");//: %d %d %d", (int)(100 * time_to_max), (int)(100 * max_to_issue_stop), (int)(distance - max_stop_distance - distance_to_max));
        return time_to_max + max_to_issue_stop; 
    }

    // Find intersection of current acceleration line and the decceleration line    
    /*
        Strategy :Find time when velocities will be equal.
        Ie, current_veloity + acceleration * t = -1 * decceleration * t
        -> t = ( -1 * v )  / (acceleration + decceleration)
     */ 
      ERROR("CASE2");// v=%d, a=%d, d=%d", (int)physics.v, (int)physics.a, (int)physics.max_d);

    D intersection = ( 2 * p->max_d * distance - Square(p->v) ) / (2 * p->a + 2 * p->max_d);
    V v_at_intersection = getV_1(p->a, intersection, p->v, p->max_v); 

    return getT_1(p->v, v_at_intersection, p->a); 
}

TimeInfo translatePhysics(const D edge_d, const D next_stop_d, Physics *physics)
{
    TimeInfo time_info;
    time_info.t_to_stop = -1;
    time_info.t_to_node = -1; 
 
    Physics tmp = *physics;
    time_info.t_to_node = translatePhysics_1(edge_d, physics);

    T when_to_stop = whenToStop(next_stop_d, &tmp);
    // ERROR("\r\nWTS=%d; DTS=%d; TTN=%d", (int)(100 * when_to_stop), (int)next_stop_d, (int)(100 * time_info.t_to_node));
    if(when_to_stop < time_info.t_to_node) {
        time_info.t_to_stop = when_to_stop;
        time_info.t_to_node -= time_info.t_to_stop;
    }

    return time_info;
}
