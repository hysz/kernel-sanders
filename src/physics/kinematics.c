#include "kinematics.h"
#include "math.h"

V getV_1(A a, D d, V init_v, V max_v)
{
    V v = Sqrt( Square(init_v) + 2 * ( a * d ));
    if(v > max_v) return max_v;

    return v;
}

D getD_1(V v, V v_at_d, A a)
{
    if(v == v_at_d) return 0;
    return ( Square(v_at_d) - Square(v) ) / (2 * a);    
}

T getT_1(V v, V v_at_t, A a)
{
    if(v_at_t == v) return 0;
    return ( v_at_t - v ) / a;
}

A getA_1(D distance, V constant_v, T time_at_variable_t)
{
    return Square(constant_v) / (2 * ( constant_v * (time_at_variable_t) - distance) );
}
