#pragma once

#include "kinematics.h" 

struct TrainMeasurements {
    V internal_speed; 
    
    J jerk;
    A acceleration;
    A decceleration;
    V velocity;

    D front_length;
    D back_length;

    T time_to_accel;
    D dist_to_accel;

    T time_to_deccel;
    D dist_to_deccel;
};
typedef struct TrainMeasurements TrainMeasurements;

void getTrainMeasurements(int train_number, TrainMeasurements* m);
void remeasureA(A a, TrainMeasurements *m);
void remeasureD(D d, TrainMeasurements *m);
void remeasureV(V v, TrainMeasurements *m);

struct Physics {
    J j;
    A a;
    A d;
    V v;

    V max_v;
    A max_a;
    A max_d;
};
typedef struct Physics Physics;

struct TimeInfo {
    T t_to_stop;
    T t_to_node;
};
typedef struct TimeInfo TimeInfo;

// Returns time to travel distance
T translatePhysics_1(D distance, Physics *physics);
void translatePhysics_2(A acceleration, Physics *physics);
T whenToStop(D distance, const Physics *p);
TimeInfo translatePhysics(const D edge_d, const D next_stop_d, Physics *physics);

float getMaxVelocity(int train_number, float stoppingDistance);

float getMaxVelocityFromSpeed(int trNum, int trSpeed);

float getStoppingDistance( int train_number, float velocity);

float getStoppingTime(int trNum, float stoppingDistance);

float calcPolynomial(int x, float A, float B, float C, float D);

float getDistanceDuringDeceleration(float t, float v0, int trainNumber);

float getVelocityDuringDeceleration(float t, float v0, int trainNumber);

float getDistanceDuringAcceleration(float t, float v0, int train, int speed);

float getVelocityDuringAcceleration(float t, float v0, int trian, int speed);

float getStoppingDistanceFromSpeed(int trNum, int trSpeed);

float power(float n, float exp);

float Abs(float n);
