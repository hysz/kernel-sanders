#include "clockserver_api.h"
#include "clockserver.h"
#include "task.h"
#include "debug.h"
#include "nameserver_api.h"
#include "names.h"
#include "kapi.h"

int Time()
{
    TaskId clockserver = WhoIs(CLOCK_SERVER); 
    if(! isValidTaskId( clockserver ) ) {
        ERROR("Time() failed to lookup clockserver: %d", clockserver);
        return -1;
    }

    ClockServerRequest req;
    req.ticks = 0;
    req.type = TIME; 
    ClockServerReply reply;
    Send(clockserver, &req, sizeof(req), &reply, sizeof(reply));
    if(reply.error) {
        ERROR("Time() failed to get time from Clock Server: %d", reply.error);
        return -1;
    }

    return reply.time;
}

int Delay(int ticks)
{
    if(ticks == 0) return 0;

    TaskId clockserver = WhoIs(CLOCK_SERVER); 
    if(! isValidTaskId( clockserver ) ) {
        ERROR("Delay() failed to lookup clockserver: %d", clockserver);
        return -1;
    }

    ClockServerRequest req;
    req.type = DELAY; 
    req.ticks = ticks;
    ClockServerReply reply;
    Send(clockserver, &req, sizeof(req), &reply, sizeof(reply));
    if(reply.error) {
        ERROR("Delay() failed to get time from Clock Server: %d", reply.error);
        return -1;
    }

    return reply.time;
}

int DelayUntil( int ticks )
{
    int time = Time();
    if(time < 0) {
        ERROR("DelayUntil() failed to get current time: %d", time);
        return -1;
    }

    int delay = ticks - time;
    int error = Delay(delay);
    if(error) {
        ERROR("DelayUntil() failed to delay for %d ticks: %d", delay, error);
        return -1;
    }

    return 0; 
}
