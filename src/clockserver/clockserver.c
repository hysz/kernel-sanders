#include "clockserver.h"
#include "common.h"
#include "task.h"
#include "message.h"
#include "clock.h"
#include "debug.h"
#include "kapi.h"
#include "event.h"
#include "names.h"
#include "nameserver_api.h"

#define CLOCK_NOTIFIER_MSG 69
static void ClockNotifier()
{
    const TaskId clockserver_tid = MyParentTid(); 

    DEBUG("Clock Notifier Parent = %d; My Tid=%d", clockserver_tid, MyTid());

    // Run    
    ClockServerRequest request;
    request.type = NOTIFY;
    request.ticks = CLOCK_NOTIFIER_MSG;
    FOREVER {
        //DEBUG("\n\rClock Notifier Awaiting Event");
        ErrorCode err = AwaitEvent(TIMER_EVENT, 0, 0);
        //DEBUG("\n\rClock Notifier Got Event");
        if (err) {
            ERROR("\n\rAwaitEvent returned error: %d", err);
        }

        ClockServerReply reply;
        Send(clockserver_tid, &request, sizeof(request), &reply, sizeof(reply));
        if(reply.error) {
            ERROR("\n\rClock Notifier Got Error From Clock Server: %d", reply.error); 
        }   
    };  
}

static void notify(ClockServerReply *reply, TaskId notifier_tid, TaskId client_tid, ClockServerRequest *request, Clock *clock, ClientSet* clients)
{
    // Update Clock & Get Current Ticks
    incrementClock(clock);
    int ticks = getTicks(clock);
    // printClock(clock);

    // Awaken clients if time >= notify_time
    Client *active_queue = 0;
    Client *last = 0;
    Client *client = 0;
    Client *next = 0;
    for(client = clients->first; client != 0; client = next) {
         //DEBUG("Check Notify: %d; is %d <= %d", client->tid, client->notify_time, ticks); 
        if(client == client->next) {
            ERROR("Infinite Loop Detected in <Notify>");
        }

        if(client->notify_time > ticks) {
            next = client->next;
            if(active_queue == 0) {
                client->next = 0;
                active_queue = client;
                last = client;
            } else {
                client->next = 0;
                last->next = client;
                last = client;
            }
                
            continue;
        }

        // Reply to client
        ClockServerReply r;
        r.error = (client->notify_time == ticks) ? 0 : ERR_LATE_NOTIFY; // ticks > notify_time => late
        r.time = ticks;
        Reply(client->tid, &r, sizeof(r)); 

        // Reclaim queue space
        client->tid = INVALID_TID;
        next = client->next;
    }
    clients->first = active_queue;
    clients->last = last;

    // Generate Reply to Notifier
    reply->error = NO_ERROR;
    if(client_tid != notifier_tid) {
        reply->error = ERR_NOTIFY_MSG_DID_NOT_COME_FROM_NOTIFIER;
    } else if(request->ticks != CLOCK_NOTIFIER_MSG) {
        reply->error = ERR_UNEXPECTED_NOTIFIER_MSG;
    }

}

static void delay(ClockServerReply *reply, TaskId client_tid, ClockServerRequest *request, Clock *clock, ClientSet* clients)
{
    if(! isValidTaskId(client_tid) ) {
        ERROR("Delay got invalid Task ID");
        reply->error = ERR_INVALID_TID; 
        return;
    }

    // Validate request input
    if(request->ticks < 0) {
        reply->error = ERR_NEGATIVE_DELAY_REQUESTED;
        return;
    } else if(request->ticks == 0) {
        reply->error = ERR_NO_DELAY_REQUESTED;
        return;
    }

    // Try to allocate - O(MAX_CLIENTS)
    // Average Case - MAX_CLIENTS/2
    int i = 0;
    for(; i < MAX_CLIENTS; ++i) {
        if(! isValidTaskId( clients->data[i].tid ) ) {
            break;
        }
    }
    if(i == MAX_CLIENTS) {
        ERROR("Clock Server has run out of queue space");
        reply->error = ERR_DELAY_QUEUE_FULL; 
        return;
    }

    // Fill in client content
    Client *client = &clients->data[i];
    client->tid = client_tid;
    client->notify_time = getTicks(clock) + request->ticks;
    client->next = 0;
    //DEBUG("Notify Time (%d) = %d + %d = %d", client_tid, getTicks(clock), request->ticks, client->notify_time);

    // Add Client to processing queue
    if(! clients->first) {
        clients->first = client;
        clients->last = client;
    } else if(! clients->last) {
        ERROR("Expected <last> to have a value");
        reply->error = ERR_QUEUE_CORRUPTED;
    } else {
        clients->last->next = client;
        clients->last = client;
    }
}

static void time(ClockServerReply *reply, ClockServerRequest *request, Clock *clock)
{
    reply->time = getTicks(clock);
    reply->error = reply->time >= request->ticks ? NO_ERROR : ERR_TIME_GONE_BACKWARDS_DETECTED;
}

static void initializeClientSet(ClientSet *clients)
{
    // Initialize data 
    int i = 0;
    for(; i < MAX_CLIENTS; ++i) {
        clients->data[i].tid = INVALID_TID;
        clients->data[i].notify_time = 0;
        clients->data[i].next = 0;
    }

    clients->first = 0;
    clients->last = 0;
}

void ClockServerTask()
{
    DEBUG("Starting Clock Server; tid=%d", MyTid());
    // Create Notifier
    TaskId notifier_tid = Create(CLOCK_NOTIFIER_PRIORITY, &ClockNotifier); 

    // Initialize Clock Server
    Clock clock; 
    resetClock(&clock);
    RegisterAs(CLOCK_SERVER);
    //initializeTimer();
    
    ClientSet clients;
    initializeClientSet(&clients);

    // Handle Requests
    const int request_len = sizeof(ClockServerRequest); 
    const int reply_len = sizeof(ClockServerReply);
    FOREVER {
        // Get Request
        TaskId tid = INVALID_TID;
        ClockServerRequest request;
        Receive(&tid, &request, request_len);

        // Process Request
        ClockServerReply reply; 
        switch(request.type) {
        case NOTIFY:
            // DEBUG("Tick");
            notify(&reply, notifier_tid, tid, &request, &clock, &clients);
            // printClock(&clock);
            // Send Reply
            Reply(tid, &reply, reply_len);
            break;

        case DELAY:
            delay(&reply, tid, &request, &clock, &clients);
            break;
    
        case TIME:
            time(&reply, &request, &clock);
            // Send Reply
            Reply(tid, &reply, reply_len);
            break;

        default:
            reply.error = ERR_BAD_REQUEST_TYPE;
            // Send Reply
            Reply(tid, &reply, reply_len);
            break; 
        }   
    }   
}
