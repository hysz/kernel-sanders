#pragma once

#define CENTISECOND_DELAY 1
#define DECISECOND_DELAY (10 * (CENTISECOND_DELAY))
#define SECOND_DELAY (10 * (DECISECOND_DELAY))
#define MINUTE_DELAY (60 * (SECOND_DELAY))

int Time();
int Delay( int ticks );
int DelayUntil( int ticks );
