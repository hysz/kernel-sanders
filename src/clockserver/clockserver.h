#pragma once

#include "errno.h"
#include "task.h"

typedef enum {DELAY, TIME, NOTIFY} ClockServerRequest_Type;

struct ClockServerRequest {
    ClockServerRequest_Type type;
    int ticks;  
};
typedef struct ClockServerRequest ClockServerRequest;

struct ClockServerReply {
    ErrorCode error;
    int time;  
};
typedef struct ClockServerReply ClockServerReply;

struct Client {
    TaskId tid;
    int notify_time;
    struct Client *next;
};
typedef struct Client Client;

#define MAX_CLIENTS 20
struct ClientSet {
    Client data[MAX_CLIENTS];
    Client *first;
    Client *last;
};
typedef struct ClientSet ClientSet;

void ClockServerTask();
