#!/bin/bash
output="$(make work 2>&1 > /dev/null)"
if [ -n "$output" ]; then
    echo "$output"
    echo "$(tput bold)BUILD FAILED!$(tput sgr0)"
    exit 1
fi

echo "$(tput bold)BUILD SUCCEEDED!$(tput sgr0)"
