#
# Makefile for busy-wait IO tests
#
XCC	 = gcc
AS	= as
LD	  = ld
CFLAGS  = -c -fPIC -Wall -I. -I./io/include -mcpu=arm920t -msoft-float -DTRACK_A  -DERROR_MODE # -DDEBUG_MODEA

# -g: include hooks for gdb
# -c: only compile
# -mcpu=arm920t: generate code for the 920t architecture
# -fpic: emit position-independent code
# -Wall: report all warnings

ASFLAGS	= -mcpu=arm920t -mapcs-32
# -mapcs: always generate a complete stack frame

LDFLAGS = -init main -Map build/a1.map -N  -T orex.ld -L/u/wbcowan/gnuarm-4.0.2/lib/gcc/arm-elf/4.0.2 -Lio/lib

S_FILES := $(shell find src -type f -name "*.s")
# C_FILES :=  src/main.c $(shell find src -mindepth 2 -type f -name "*.c" | grep -v "test") src/test/k2_measure_tests.c
C_FILES :=  src/main.c $(shell find src -mindepth 2 -type f -name "*.c" | grep -v "test") src/test/io_test.c
# C_FILES :=  src/main.c $(shell find src -mindepth 2 -type f -name "*.c" | grep -v "test") src/test/termout_test.c
#C_FILES :=  src/main.c $(shell find src -mindepth 2 -type f -name "*.c" | grep -v "test") src/test/termin_test.c
# C_FILES := src/main.c $(shell find src -mindepth 2 -type f -name "*.c" | grep -v "test") src/test/clockserver_test.c
SUBDIRS := $(shell find src -maxdepth 2 -type d)

all: a1.elf

init:
	mkdir build/ -p

assembly: init
	for f in $(C_FILES); do \
		$(XCC) -O2 -S $(CFLAGS) $(SUBDIRS:%=-I%) -o build/"$$(basename $$f | sed 's/\.c//g' )".s $$f ; \
	done 
objects: init
	for f in $(C_FILES); do \
		$(AS) $(ASFLAGS) -o build/$$(basename $$f | sed 's/\.c//g' ).o build/$$(basename $$f | sed 's/\.c//g' ).s; \
	done; \
	for f in $(S_FILES); do \
		$(AS) $(ASFLAGS) -o build/$$(basename $$f | sed 's/\.s//g' ).o $$f; \
	done 

a1.elf: init assembly objects
	$(LD) $(LDFLAGS) -o build/$@ $(C_FILES:%.c=build/$$(basename %.o)) $(S_FILES:%.s=build/$$(basename %.o)) -L./io/lib -lbwio -lgcc; \
	cp build/$@ /u/cs452/tftp/ARM/quack.elf; \
	chmod +r /u/cs452/tftp/ARM/quack.elf;

clean:
	-rm -f build/*

work: clean all 

